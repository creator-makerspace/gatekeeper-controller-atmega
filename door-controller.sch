<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="13" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="12" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="recom-intl">
<description>&lt;b&gt;RECOM International&lt;/b&gt; - v1.00 (05/05/2011)&lt;p&gt;
&lt;p&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;
USE AT YOUR OWN RISK!&lt;p&gt;
&lt;author&gt;Copyright (C) 2010, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;&lt;/author&gt;</description>
<packages>
<package name="R-78XX">
<description>R-78xx-0.5 Series&lt;p&gt;RECOM Intl.</description>
<wire x1="-6" y1="5.75" x2="6" y2="5.75" width="0.2032" layer="21"/>
<wire x1="6" y1="5.75" x2="6" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="6" y1="-2.75" x2="-6" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-6" y1="-2.75" x2="-6" y2="5.75" width="0.2032" layer="21"/>
<circle x="-4.766" y="-1.492" radius="0.449" width="0" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.9304" shape="square"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<text x="-6.985" y="-2.54" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2.54" y="2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="R-78XX">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-5.08" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<text x="-2.7623" y="2.8575" size="1.27" layer="95">DC/DC</text>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-78XX" prefix="U">
<description>&lt;b&gt;DC-DC CONVERTER&lt;/b&gt;&lt;p&gt; 
R-78xx-0.5 Series</description>
<gates>
<gate name="G$1" symbol="R-78XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R-78XX">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="cyviz">
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="SUP-3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch-dil">
<description>&lt;b&gt;DIL Switches and Code Switches&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DS-01">
<description>&lt;b&gt;DIL/CODE SWITCH&lt;/b&gt;&lt;p&gt;
Mors</description>
<wire x1="-2.032" y1="-5.08" x2="2.032" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="2.032" y1="5.08" x2="-2.032" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="5.08" x2="-2.032" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.905" x2="-2.032" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.905" x2="-2.032" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-5.08" x2="2.032" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.905" x2="-0.762" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.905" x2="0.762" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.905" x2="-0.762" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.905" x2="-1.397" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-1.905" x2="-1.397" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.905" x2="-1.397" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-0.254" y="-3.429" size="0.9906" layer="51" ratio="14">1</text>
<text x="-2.032" y="-6.731" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.032" y="5.461" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.762" y="2.413" size="0.9906" layer="51" ratio="14">ON</text>
<rectangle x1="-0.762" y1="-1.905" x2="0.762" y2="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIP01">
<wire x1="-1.905" y1="5.08" x2="-1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="5.08" x2="1.905" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.905" y1="5.08" x2="1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-5.08" x2="1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="0.508" y1="-2.54" x2="-0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-2.54" x2="-0.508" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="4.445" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-0.127" y="-4.064" size="0.9906" layer="94" ratio="14">1</text>
<text x="-0.889" y="3.175" size="0.9906" layer="94" ratio="14">ON</text>
<rectangle x1="-0.254" y1="-2.286" x2="0.254" y2="0" layer="94"/>
<pin name="2" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DS01" prefix="S" uservalue="yes">
<description>&lt;b&gt;DIL/CODE SWITCH&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIP01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DS-01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA03-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.175" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-3.81" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="4.064" y="0.635" size="1.27" layer="21" ratio="10">6</text>
<text x="-1.27" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA03-2">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA03-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA03-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X03">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X3">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X3_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-PTH">
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.24" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="3.97" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-5MM-3">
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-SMD">
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-2.26" y="0.2" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.26" y="-1.07" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="1X03-1MM-RA">
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-3.155" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-2.955" y="-3.395" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X03_SMD_RA_MALE">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD 3-Pin Male Right-Angle Header w/ Alignment posts&lt;/h3&gt;

Matches 4UCONN part # 11026&lt;br&gt;
&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt;http://www.4uconnector.com/online/object/4udrawing/11026.pdf&lt;/a&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
</package>
<package name="JST-3-PTH-VERT">
<description>This 3-pin connector mates with the JST cable sold on SparkFun.</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
</packages>
<symbols>
<symbol name="M03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M03" prefix="J" uservalue="yes">
<description>&lt;b&gt;Header 3&lt;/b&gt;
Standard 3-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="J$1" symbol="M03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X03">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="STOREFRONT_ID" value="PRT-08433" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12591" constant="no"/>
<attribute name="VALUE" value="3-PIN SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="relay">
<description>&lt;b&gt;Relays&lt;/b&gt;&lt;p&gt;
&lt;ul&gt;
&lt;li&gt;Eichhoff
&lt;li&gt;Finder
&lt;li&gt;Fujitsu
&lt;li&gt;HAMLIN
&lt;li&gt;OMRON
&lt;li&gt;Matsushita
&lt;li&gt;NAiS
&lt;li&gt;Siemens
&lt;li&gt;Schrack
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="G5SB">
<description>&lt;b&gt;Compact Single-pole Relay for Switching 5 A&lt;/b&gt;&lt;p&gt;
Source: http://www.omron.com/ecb/products/pry/121/g5sb.html</description>
<wire x1="-11.25" y1="4.9" x2="8.7" y2="4.9" width="0.2032" layer="21"/>
<wire x1="8.7" y1="4.9" x2="8.7" y2="-4.9" width="0.2032" layer="21"/>
<wire x1="8.7" y1="-4.9" x2="-11.25" y2="-4.9" width="0.2032" layer="21"/>
<wire x1="-11.25" y1="-4.9" x2="-11.25" y2="4.9" width="0.2032" layer="21"/>
<pad name="1" x="7.62" y="3.81" drill="1.3" diameter="1.9304" rot="R180"/>
<pad name="2" x="-2.54" y="3.81" drill="1.3" diameter="2.1844" rot="R180"/>
<pad name="3" x="-10.16" y="3.81" drill="1.3" diameter="2.1844" rot="R180"/>
<pad name="4" x="-7.62" y="-3.81" drill="1.3" diameter="2.1844" rot="R180"/>
<pad name="5" x="7.62" y="-3.81" drill="1.3" diameter="1.9304" rot="R180"/>
<text x="-6.35" y="0" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.975" y1="-2.525" x2="8.325" y2="2.55" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="K">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.778" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="U">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;PART</text>
<pin name="O" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="S" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="P" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="G5SB" prefix="K">
<description>&lt;b&gt;Compact Single-pole Relay for Switching 5 A&lt;/b&gt;&lt;p&gt;
Source: http://www.omron.com/ecb/products/pry/121/g5sb.html</description>
<gates>
<gate name="1" symbol="K" x="-10.16" y="0" addlevel="must"/>
<gate name="U" symbol="U" x="12.7" y="-2.54" addlevel="must"/>
</gates>
<devices>
<device name="" package="G5SB">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="5"/>
<connect gate="U" pin="O" pad="4"/>
<connect gate="U" pin="P" pad="2"/>
<connect gate="U" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="creator">
<description>Generated from &lt;b&gt;door-reader.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.05" dx="0.6" dy="1.3" layer="1" roundness="30"/>
<smd name="2" x="0.95" y="-1.05" dx="0.6" dy="1.3" layer="1" roundness="30"/>
<smd name="1" x="-0.95" y="-1.05" dx="0.6" dy="1.3" layer="1" roundness="30"/>
<text x="-1.143" y="0.127" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.143" y="-0.381" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="21"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="21"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="21"/>
<polygon width="0.2" layer="39">
<vertex x="-1.4" y="-0.65"/>
<vertex x="-1.4" y="0.65"/>
<vertex x="1.4" y="0.65"/>
<vertex x="1.4" y="-0.65"/>
</polygon>
</package>
<package name="SMD-IPC_C0805">
<description>&lt;b&gt;CAPACITOR EIA0805&lt;/b&gt;</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="21"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="21"/>
<rectangle x1="-1.0922" y1="-0.724" x2="-0.342" y2="0.7262" layer="21"/>
<rectangle x1="0.3555" y1="-0.724" x2="1.1057" y2="0.7262" layer="21"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1002" y2="0.4002" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<text x="-1.016" y="0.762" size="0.254" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.016" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-1.5" y="0.7"/>
<vertex x="1.4" y="0.7"/>
<vertex x="1.4" y="-0.7"/>
<vertex x="-1.5" y="-0.7"/>
</polygon>
</package>
<package name="SMD-IPC_C0201">
<description>&lt;b&gt;CAPACITOR EIA0201&lt;/b&gt;</description>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
<smd name="1" x="-0.33" y="0" dx="0.45" dy="0.46" layer="1"/>
<smd name="2" x="0.33" y="0" dx="0.45" dy="0.46" layer="1"/>
<text x="-0.6" y="0.3" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.6" y="-0.6" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-0.4" y="0.2"/>
<vertex x="0.4" y="0.2"/>
<vertex x="0.4" y="-0.2"/>
<vertex x="-0.4" y="-0.2"/>
</polygon>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.4" y="0.3" size="0.127" layer="25">&gt;NAME</text>
<text x="-0.4" y="-0.4" size="0.127" layer="27">&gt;VALUE</text>
</package>
<package name="C0201_HAPRO">
<description>C0201 after hapro's suggestion</description>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.03" layer="21"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.03" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.03" layer="21"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.03" layer="21"/>
<smd name="1" x="-0.25" y="0" dx="0.3" dy="0.4" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.3" dy="0.4" layer="1"/>
<text x="-0.4" y="0.3" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.4" y="-0.5" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.15" layer="39">
<vertex x="-0.4" y="0.2"/>
<vertex x="0.4" y="0.2"/>
<vertex x="0.4" y="-0.2"/>
<vertex x="-0.4" y="-0.2"/>
</polygon>
</package>
<package name="SMD-IPC_C0306">
<description>TDK C Series Commercial Grade Low ESL Reverse Geometry&lt;p&gt;
http://product.tdk.com/capacitor/mlcc/en/documents/mlcc_commercial_lwreverse_en.pdf</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="21"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.2539" y2="0.2952" layer="21"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5589" y2="0.2952" layer="21"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.2" y2="0.3001" layer="35"/>
<smd name="1" x="-0.3" y="0" dx="0.4" dy="1.6" layer="1" roundness="50"/>
<smd name="2" x="0.3" y="0" dx="0.4" dy="1.6" layer="1" roundness="50"/>
<text x="-0.508" y="0.354" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.608" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.15" layer="39">
<vertex x="-0.5" y="0.8"/>
<vertex x="0.5" y="0.8"/>
<vertex x="0.5" y="-0.8"/>
<vertex x="-0.5" y="-0.8"/>
</polygon>
</package>
<package name="SMD-IPC_C0603">
<description>&lt;b&gt;CAPACITOR EIA0603&lt;/b&gt;</description>
<wire x1="-0.356" y1="0.357" x2="0.356" y2="0.357" width="0.1016" layer="21"/>
<wire x1="-0.356" y1="-0.344" x2="0.356" y2="-0.344" width="0.1016" layer="21"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.338" y2="0.4802" layer="21"/>
<rectangle x1="0.3301" y1="-0.4699" x2="0.8303" y2="0.4802" layer="21"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.2" y2="0.3001" layer="35"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.85" layer="1" roundness="50"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.85" layer="1" roundness="50"/>
<text x="-0.7938" y="0.4013" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.7938" y="-0.7938" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.1" layer="39">
<vertex x="-0.8" y="-0.375"/>
<vertex x="0.8" y="-0.375" curve="90"/>
<vertex x="0.95" y="-0.225"/>
<vertex x="0.95" y="0.225" curve="90"/>
<vertex x="0.8" y="0.375"/>
<vertex x="-0.8" y="0.375" curve="90"/>
<vertex x="-0.95" y="0.225"/>
<vertex x="-0.95" y="-0.225" curve="90"/>
</polygon>
</package>
<package name="SMD-IPC_C1206">
<description>&lt;b&gt;CAPACITOR EIA1206&lt;/b&gt;</description>
<wire x1="-0.9652" y1="0.7446" x2="0.9652" y2="0.7446" width="0.1016" layer="21"/>
<wire x1="-0.9652" y1="-0.7446" x2="0.9652" y2="-0.7446" width="0.1016" layer="21"/>
<rectangle x1="-1.7018" y1="-0.8045" x2="-0.9516" y2="0.8045" layer="21"/>
<rectangle x1="0.9516" y1="-0.8045" x2="1.7018" y2="0.8045" layer="21"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.2" y2="0.4002" layer="35"/>
<smd name="+" x="1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="50"/>
<smd name="-" x="-1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="50"/>
<text x="-0.762" y="0.254" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.635" size="0.3048" layer="27">&gt;VALUE</text>
<text x="0.4572" y="-0.3048" size="0.6096" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-1.9" y="0.9"/>
<vertex x="1.9" y="0.9"/>
<vertex x="1.9" y="-0.9"/>
<vertex x="-1.9" y="-0.9"/>
</polygon>
</package>
<package name="SMD-IPC_C3225">
<description>&lt;b&gt;CAPACITOR EIA3225&lt;/b&gt;</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="21"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="21"/>
<rectangle x1="-1.7018" y1="-1.2955" x2="-0.9516" y2="1.3045" layer="21"/>
<rectangle x1="0.9516" y1="-1.3045" x2="1.7018" y2="1.2955" layer="21"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.2" y2="0.5002" layer="35"/>
<smd name="+" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="50"/>
<smd name="-" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="50"/>
<text x="-0.8" y="0.6" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1" size="0.254" layer="27">&gt;VALUE</text>
<text x="1.905" y="-0.635" size="1.27" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-2" y="1.4"/>
<vertex x="2" y="1.4"/>
<vertex x="2" y="-1.4"/>
<vertex x="-2" y="-1.4"/>
</polygon>
</package>
<package name="SMD-IPC_CAPC1005X55N">
<description>&lt;b&gt;CAPACITOR EIA0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="21"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.2539" y2="0.2952" layer="21"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5589" y2="0.2952" layer="21"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.2" y2="0.3001" layer="35"/>
<smd name="1" x="-0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="50"/>
<smd name="2" x="0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="50"/>
<text x="-0.508" y="0.354" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.608" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.15" layer="39">
<vertex x="-0.76" y="0.31"/>
<vertex x="0.76" y="0.31"/>
<vertex x="0.76" y="-0.31"/>
<vertex x="-0.76" y="-0.31"/>
</polygon>
</package>
<package name="SMD-IPC_R0805">
<description>&lt;b&gt;RESISTOR EIA0805&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="21"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0565" y2="0.7016" layer="21"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4167" y2="0.7016" layer="21"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.2" y2="0.5002" layer="35"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<text x="-0.9525" y="0.7938" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.9525" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-1.3" y="0.8"/>
<vertex x="1.3" y="0.8"/>
<vertex x="1.3" y="-0.8"/>
<vertex x="-1.3" y="-0.8"/>
</polygon>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.5" y="0.3" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.5" y="-0.6" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.15" layer="39">
<vertex x="-0.395" y="0.215"/>
<vertex x="0.395" y="0.215"/>
<vertex x="0.395" y="-0.215"/>
<vertex x="-0.395" y="-0.215"/>
</polygon>
</package>
<package name="SMD-IPC_R0402">
<description>&lt;b&gt;RESISTOR EIA0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.149" x2="0.245" y2="0.149" width="0.1524" layer="21"/>
<wire x1="0.245" y1="-0.149" x2="-0.245" y2="-0.149" width="0.1524" layer="21"/>
<rectangle x1="-0.479" y1="-0.3048" x2="-0.1789" y2="0.2952" layer="21"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5589" y2="0.2952" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.53" dy="0.63" layer="1"/>
<smd name="2" x="0.45" y="0" dx="0.53" dy="0.63" layer="1"/>
<text x="-0.5" y="0.4" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.5" y="-0.6" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.15" layer="39">
<vertex x="-0.715" y="0.315"/>
<vertex x="0.715" y="0.315"/>
<vertex x="0.715" y="-0.315"/>
<vertex x="-0.715" y="-0.315"/>
</polygon>
</package>
<package name="SMD-IPC_R0603">
<description>&lt;b&gt;RESISTOR EIA0603&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="21"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="21"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8383" y2="0.4319" layer="21"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4317" y2="0.4319" layer="21"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.2" y2="0.4002" layer="35"/>
<smd name="1" x="-0.7" y="0" dx="0.75" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.7" y="0" dx="0.75" dy="0.9" layer="1" roundness="50"/>
<text x="-0.7938" y="0.4763" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.7938" y="-0.7938" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.1" layer="39">
<vertex x="0.95" y="-0.5"/>
<vertex x="-0.95" y="-0.5" curve="-90"/>
<vertex x="-1.1" y="-0.35"/>
<vertex x="-1.1" y="0.35" curve="-90"/>
<vertex x="-0.95" y="0.5"/>
<vertex x="0.95" y="0.5" curve="-90"/>
<vertex x="1.1" y="0.35"/>
<vertex x="1.1" y="-0.35" curve="-90"/>
</polygon>
</package>
<package name="SMD-IPC_R0805W">
<description>&lt;b&gt;RESISTOR EIA0805 for wave soldering&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0565" y2="0.7016" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4167" y2="0.7016" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.2" y2="0.5002" layer="35"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1" roundness="50"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1" roundness="50"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-1.5" y="0.7"/>
<vertex x="1.5" y="0.7"/>
<vertex x="1.5" y="-0.7"/>
<vertex x="-1.5" y="-0.7"/>
</polygon>
</package>
<package name="SMD-IPC_R1206">
<description>&lt;b&gt;RESISTOR EIA1206&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="21"/>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9524" y2="0.8764" layer="21"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6892" y2="0.8764" layer="21"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3001" y2="0.7001" layer="35"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1" roundness="50"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1" roundness="50"/>
<text x="-2.159" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.159" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-2" y="0.9"/>
<vertex x="2" y="0.9"/>
<vertex x="2" y="-0.9"/>
<vertex x="-2" y="-0.9"/>
</polygon>
</package>
<package name="SMD-IPC_R1206W">
<description>&lt;b&gt;RESISTOR EIA1206 for wave soldering&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9008" y2="0.8739" layer="51"/>
<rectangle x1="0.8889" y1="-0.8763" x2="1.6391" y2="0.8739" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3001" y2="0.7001" layer="35"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1" roundness="50"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1" roundness="50"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-2.1" y="0.9"/>
<vertex x="2.1" y="0.9"/>
<vertex x="2.1" y="-0.9"/>
<vertex x="-2.1" y="-0.9"/>
</polygon>
</package>
<package name="BSP1212-03H03-06_LF">
<description>Footprint for the Bestar BSP1212-03H03-06 LF Piezo electric buzzer</description>
<smd name="1" x="-5.6" y="0" dx="1.2" dy="4" layer="1"/>
<smd name="2" x="5.6" y="0" dx="1.2" dy="4" layer="1"/>
<wire x1="-6" y1="6" x2="-3.2" y2="6" width="0.127" layer="21"/>
<wire x1="-3.2" y1="6" x2="3.2" y2="6" width="0.127" layer="21"/>
<wire x1="3.2" y1="6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-3.2" y1="6" x2="-3.2" y2="3.9" width="0.127" layer="21"/>
<wire x1="-3.2" y1="3.9" x2="3.2" y2="3.9" width="0.127" layer="21"/>
<wire x1="3.2" y1="3.9" x2="3.2" y2="6" width="0.127" layer="21"/>
<text x="0" y="1" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-6.5" y1="-6.5" x2="6.5" y2="6.5" layer="39"/>
</package>
<package name="SMD-IPC_C1310">
<description>&lt;b&gt;CAPACITOR EIA1310&lt;/b&gt;</description>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<rectangle x1="-0.6604" y1="-0.6224" x2="-0.2803" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6224" x2="0.6595" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1002" y2="0.3001" layer="35"/>
<smd name="+" x="0.7" y="0" dx="1" dy="1.3" layer="1" roundness="50"/>
<smd name="-" x="-0.7" y="0" dx="1" dy="1.3" layer="1" roundness="50"/>
<text x="-1.4" y="1.1" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.4" y="-1.7" size="0.6096" layer="27">&gt;VALUE</text>
<text x="0.635" y="-0.2032" size="0.4064" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-1.1" y="0.7"/>
<vertex x="1.1" y="0.7"/>
<vertex x="1.1" y="-0.7"/>
<vertex x="-1.1" y="-0.7"/>
</polygon>
</package>
<package name="SMD-IPC_C1608">
<description>&lt;b&gt;CAPACITOR EIA1608&lt;/b&gt;</description>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.338" y2="0.4802" layer="51"/>
<rectangle x1="0.3301" y1="-0.4699" x2="0.8303" y2="0.4802" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.2" y2="0.3001" layer="35"/>
<smd name="+" x="0.85" y="0" dx="1.1" dy="1" layer="1" roundness="50"/>
<smd name="-" x="-0.85" y="0" dx="1.1" dy="1" layer="1" roundness="50"/>
<text x="-1.1" y="0.6" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.1" y="-1" size="0.4064" layer="27">&gt;VALUE</text>
<text x="0.7874" y="-0.3048" size="0.6096" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-1.1" y="0.5"/>
<vertex x="1.2" y="0.5"/>
<vertex x="1.2" y="-0.5"/>
<vertex x="-1.1" y="-0.5"/>
</polygon>
</package>
<package name="SMD-IPC_C1825">
<description>&lt;b&gt;CAPACITOR EIA1825&lt;/b&gt;</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.4629" y2="3.3473" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3979" y2="3.3473" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7001" y2="0.7001" layer="35"/>
<smd name="+" x="1.95" y="0" dx="1.9" dy="6.8" layer="1" roundness="50"/>
<smd name="-" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1" roundness="50"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.54" y="-0.635" size="1.27" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-2.6" y="3.4"/>
<vertex x="2.7" y="3.4"/>
<vertex x="2.7" y="-3.4"/>
<vertex x="-2.6" y="-3.4"/>
</polygon>
</package>
<package name="SMD-IPC_C2012">
<description>&lt;b&gt;CAPACITOR EIA2012&lt;/b&gt;</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<rectangle x1="-1.0922" y1="-0.724" x2="-0.342" y2="0.7262" layer="51"/>
<rectangle x1="0.3555" y1="-0.724" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1002" y2="0.4002" layer="35"/>
<smd name="+" x="0.85" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<smd name="-" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1" roundness="50"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="1.1" y="-0.4" size="0.8128" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-1.3" y="0.8"/>
<vertex x="1.3" y="0.8"/>
<vertex x="1.3" y="-0.8"/>
<vertex x="-1.3" y="-0.8"/>
</polygon>
</package>
<package name="SMD-IPC_C3216">
<description>&lt;b&gt;CAPACITOR EIA3216&lt;/b&gt;</description>
<wire x1="-0.965" y1="0.787" x2="1.565" y2="0.787" width="0.1016" layer="21"/>
<wire x1="-0.965" y1="-0.787" x2="1.565" y2="-0.787" width="0.1016" layer="21"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9516" y2="0.8492" layer="21"/>
<rectangle x1="1.5516" y1="-0.8491" x2="1.7018" y2="0.851" layer="21"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3001" y2="0.5002" layer="35"/>
<smd name="N" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="50"/>
<smd name="P" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="50"/>
<text x="-0.9" y="0.3" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.6" size="0.254" layer="27">&gt;VALUE</text>
<text x="0.5" y="-0.6" size="1.27" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-2.1" y="0.9"/>
<vertex x="2.1" y="0.9"/>
<vertex x="2.1" y="-0.9"/>
<vertex x="-2.1" y="-0.9"/>
</polygon>
</package>
<package name="SMD-IPC_CT3528">
<description>&lt;b&gt;TANTALUM CAPACITOR EIA3528&lt;/b&gt;</description>
<wire x1="-1.637" y1="-1.383" x2="-1.637" y2="1.383" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="1.016" x2="-1.778" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.637" y1="1.383" x2="-1.637" y2="1.383" width="0.2032" layer="21"/>
<wire x1="1.637" y1="-1.383" x2="-1.637" y2="-1.383" width="0.2032" layer="21"/>
<wire x1="1.778" y1="1.016" x2="1.778" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.637" y1="-1.383" x2="1.637" y2="1.383" width="0.2032" layer="21"/>
<wire x1="-0.68" y1="0" x2="-1.31" y2="0" width="0.2032" layer="21"/>
<wire x1="-1" y1="0.3" x2="-1" y2="-0.33" width="0.2032" layer="21"/>
<rectangle x1="-0.3" y1="-1" x2="0.3001" y2="1.0001" layer="35"/>
<smd name="+" x="-1.5" y="0" dx="2" dy="2.2" layer="1" roundness="50"/>
<smd name="-" x="1.5" y="0" dx="2" dy="2.2" layer="1" roundness="50"/>
<text x="-1.4288" y="0.7938" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.4288" y="-1.1113" size="0.4064" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-2.3" y="1.5"/>
<vertex x="2.4" y="1.5"/>
<vertex x="2.4" y="-1.5"/>
<vertex x="-2.3" y="-1.5"/>
</polygon>
</package>
<package name="SMD-IPC_C4532">
<description>&lt;b&gt;CAPACITOR EIA4532&lt;/b&gt;</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4375" y2="1.6491" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3979" y2="1.6491" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4002" y2="0.7001" layer="35"/>
<smd name="+" x="1.95" y="0" dx="1.9" dy="3.4" layer="1" roundness="50"/>
<smd name="-" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1" roundness="50"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.54" y="-0.635" size="1.27" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-2.6" y="1.7"/>
<vertex x="2.6" y="1.7"/>
<vertex x="2.6" y="-1.7"/>
<vertex x="-2.6" y="-1.7"/>
</polygon>
</package>
<package name="SMD-IPC_C4564">
<description>&lt;b&gt;CAPACITOR EIA4564&lt;/b&gt;</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.4629" y2="3.3473" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3979" y2="3.3473" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5002" y2="1.0001" layer="35"/>
<smd name="+" x="1.95" y="0" dx="1.9" dy="6.8" layer="1" roundness="50"/>
<smd name="-" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1" roundness="50"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.54" y="-0.635" size="1.27" layer="21">+</text>
<polygon width="0.127" layer="39">
<vertex x="-2.7" y="-3.4"/>
<vertex x="2.8" y="-3.4"/>
<vertex x="2.8" y="3.4"/>
<vertex x="-2.7" y="3.4"/>
</polygon>
</package>
<package name="SMD-IPC_CT6032">
<description>&lt;b&gt;TANTALUM CAPACITOR EIA6032&lt;/b&gt;</description>
<wire x1="-2.932" y1="-1.637" x2="-2.932" y2="1.637" width="0.2032" layer="21"/>
<wire x1="-3.073" y1="1.067" x2="-3.073" y2="-1.067" width="0.1524" layer="21"/>
<wire x1="2.957" y1="1.637" x2="-2.932" y2="1.637" width="0.2032" layer="21"/>
<wire x1="2.957" y1="-1.637" x2="-2.932" y2="-1.637" width="0.2032" layer="21"/>
<wire x1="3.073" y1="1.067" x2="3.073" y2="-1.067" width="0.1524" layer="21"/>
<wire x1="2.957" y1="-1.637" x2="2.957" y2="1.637" width="0.2032" layer="21"/>
<wire x1="-1.18" y1="0" x2="-2.45" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.63" x2="-1.8" y2="-0.64" width="0.2032" layer="21"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5002" y2="1.0001" layer="35"/>
<smd name="+" x="-2.5" y="0" dx="2.6" dy="2.2" layer="1" roundness="50"/>
<smd name="-" x="2.5" y="0" dx="2.6" dy="2.2" layer="1" roundness="50"/>
<text x="-2.6988" y="0.9525" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.6988" y="-1.4288" size="0.4064" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-3.4" y="1.9"/>
<vertex x="3.4" y="1.9"/>
<vertex x="3.4" y="-1.9"/>
<vertex x="-3.4" y="-1.9"/>
</polygon>
</package>
<package name="SMD-IPC_CT7343">
<description>&lt;b&gt;TANTALUM CAPACITOR EIA7343&lt;/b&gt;</description>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-3.734" y1="1.143" x2="-3.734" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.8" y1="2.3" x2="-3.8" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.8" y1="-2.3" x2="-3.8" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="3.734" y1="1.143" x2="3.734" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.8" y1="-2.3" x2="3.8" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-2.04" y1="0" x2="-3.09" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.55" y1="0.52" x2="-2.55" y2="-0.52" width="0.2032" layer="21"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5002" y2="1.0001" layer="35"/>
<smd name="+" x="-3.1" y="0" dx="2.45" dy="2.45" layer="1" roundness="50"/>
<smd name="-" x="3.1" y="0" dx="2.45" dy="2.45" layer="1" roundness="50"/>
<text x="-3.556" y="1.016" size="1.016" layer="25">&gt;NAME</text>
<text x="-3.556" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="39">
<vertex x="-4.1" y="2.4"/>
<vertex x="4.1" y="2.4"/>
<vertex x="4.1" y="-2.4"/>
<vertex x="-4.1" y="-2.4"/>
</polygon>
</package>
<package name="PXE-F">
<description>The F61 and F80 package of Nippon Chemi-con PXE series&lt;p&gt;
http://www.chemi-con.co.jp/e/catalog/pdf/al-e/al-sepa-e/002-cp/al-pxe-e-111201.pdf</description>
<circle x="0" y="0" radius="3.15" width="0.127" layer="21"/>
<wire x1="-3.45" y1="3.45" x2="2.4" y2="3.45" width="0.127" layer="21"/>
<wire x1="2.4" y1="3.45" x2="3.45" y2="2.4" width="0.127" layer="21"/>
<wire x1="3.45" y1="2.4" x2="3.45" y2="-2.4" width="0.127" layer="21"/>
<wire x1="3.45" y1="-2.4" x2="2.4" y2="-3.45" width="0.127" layer="21"/>
<wire x1="2.4" y1="-3.45" x2="-3.45" y2="-3.45" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-3.45" x2="-3.45" y2="3.45" width="0.127" layer="21"/>
<wire x1="3.5" y1="0" x2="4.7" y2="0" width="0.127" layer="21"/>
<wire x1="4.1" y1="0.6" x2="4.1" y2="-0.6" width="0.127" layer="21"/>
<smd name="+" x="2.5" y="0" dx="3" dy="1.5" layer="1" roundness="10"/>
<smd name="-" x="-2.5" y="0" dx="3" dy="1.5" layer="1" roundness="10"/>
<text x="-1.222" y="0.968" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.222" y="-0.198" size="0.6096" layer="27">&gt;VALUE</text>
<polygon width="0.2" layer="39">
<vertex x="-3.5" y="3.5"/>
<vertex x="3.5" y="3.5"/>
<vertex x="3.5" y="-3.5"/>
<vertex x="-3.5" y="-3.5"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-2" y="2.4"/>
<vertex x="-2" y="-2.4" curve="-44.999389"/>
<vertex x="-3.15" y="0" curve="-44.999389"/>
</polygon>
</package>
<package name="PXE-H">
<description>The H70 and H80 package of Nippon Chemi-con PXE series&lt;p&gt;
http://www.chemi-con.co.jp/e/catalog/pdf/al-e/al-sepa-e/002-cp/al-pxe-e-111201.pdf</description>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-4.15" y1="4.15" x2="2.4" y2="4.15" width="0.127" layer="21"/>
<wire x1="2.4" y1="4.15" x2="4.15" y2="2.4" width="0.127" layer="21"/>
<wire x1="4.15" y1="2.4" x2="4.15" y2="-2.4" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2.4" x2="2.4" y2="-4.15" width="0.127" layer="21"/>
<wire x1="2.4" y1="-4.15" x2="-4.15" y2="-4.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-4.15" x2="-4.15" y2="4.15" width="0.127" layer="21"/>
<wire x1="2.5" y1="0" x2="3.7" y2="0" width="0.127" layer="21"/>
<wire x1="3.1" y1="0.6" x2="3.1" y2="-0.6" width="0.127" layer="21"/>
<smd name="+" x="3.4" y="0" dx="3.3" dy="1.5" layer="1" roundness="10"/>
<smd name="-" x="-3.4" y="0" dx="3.3" dy="1.5" layer="1" roundness="10"/>
<text x="-1.222" y="0.968" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.222" y="-0.198" size="0.6096" layer="27">&gt;VALUE</text>
<polygon width="0.2" layer="39">
<vertex x="-4.1" y="4.1"/>
<vertex x="4.1" y="4.1"/>
<vertex x="4.1" y="-4.1"/>
<vertex x="-4.1" y="-4.1"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-2" y="3.4"/>
<vertex x="-2" y="-3.4" curve="-54.999563"/>
<vertex x="-3.95" y="0" curve="-54.999563"/>
</polygon>
</package>
<package name="SOT323-SC70_R">
<description>&lt;b&gt;Reflow soldering footprint SOT323 (SC-70)&lt;/b&gt;&lt;p&gt;

Drawn from &lt;a href=http://www.nxp.com/documents/data_sheet/BC847_SER.pdf&gt;ON BC847 series datasheet 20 August 2012&lt;/a&gt;</description>
<wire x1="1.1" y1="0.625" x2="1.1" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="1.1" y1="-0.625" x2="0.85" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="0.85" y1="-0.625" x2="0.85" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="0.85" y1="-1.1" x2="0.45" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="0.45" y1="-0.625" x2="-0.45" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="-0.45" y1="-0.625" x2="-0.45" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-0.45" y1="-1.1" x2="-0.85" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-0.85" y1="-1.1" x2="-0.85" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="-0.85" y1="-0.625" x2="-1.1" y2="-0.625" width="0.1524" layer="21"/>
<wire x1="-1.1" y1="-0.625" x2="-1.1" y2="0.625" width="0.1524" layer="21"/>
<wire x1="-1.1" y1="0.625" x2="-0.2" y2="0.625" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.625" x2="-0.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="1.1" x2="0.2" y2="1.1" width="0.1524" layer="21"/>
<wire x1="0.2" y1="1.1" x2="0.2" y2="0.625" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.625" x2="1.1" y2="0.625" width="0.1524" layer="21"/>
<smd name="1" x="-0.65" y="-0.925" dx="0.6" dy="0.55" layer="1" roundness="30"/>
<smd name="2" x="0.65" y="-0.925" dx="0.6" dy="0.55" layer="1" roundness="30"/>
<smd name="3" x="0" y="0.925" dx="0.6" dy="0.55" layer="1" roundness="30"/>
<text x="-0.9" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.4" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.2" layer="39">
<vertex x="-1.175" y="-1.325"/>
<vertex x="-1.175" y="0.75"/>
<vertex x="-0.5" y="0.75"/>
<vertex x="-0.5" y="1.325"/>
<vertex x="0.5" y="1.325"/>
<vertex x="0.5" y="0.75"/>
<vertex x="1.175" y="0.75"/>
<vertex x="1.175" y="-1.325"/>
</polygon>
</package>
<package name="QFP80P900X900X120-32N">
<wire x1="-2.0066" y1="-6.1976" x2="-2.0066" y2="-5.1816" width="0.1524" layer="21"/>
<wire x1="-1.1938" y1="5.1816" x2="-1.1938" y2="6.223" width="0.1524" layer="21"/>
<wire x1="5.1816" y1="-0.381" x2="6.1976" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-3.0734" y1="2.667" x2="-2.667" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="-3.5052" x2="-3.0226" y2="-3.5052" width="0" layer="51"/>
<wire x1="-3.0226" y1="-3.5052" x2="-2.5654" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-3.5052" x2="3.5052" y2="-3.0226" width="0" layer="51"/>
<wire x1="3.5052" y1="-3.0226" x2="3.5052" y2="-2.5654" width="0" layer="51"/>
<wire x1="3.5052" y1="3.5052" x2="3.0226" y2="3.5052" width="0" layer="51"/>
<wire x1="3.0226" y1="3.5052" x2="2.5654" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="3.5052" x2="-3.5052" y2="3.0226" width="0" layer="51"/>
<wire x1="-3.5052" y1="3.0226" x2="-3.5052" y2="2.5654" width="0" layer="51"/>
<wire x1="3.0226" y1="3.5052" x2="3.0226" y2="4.4958" width="0" layer="51"/>
<wire x1="3.0226" y1="4.4958" x2="2.5654" y2="4.4958" width="0" layer="51"/>
<wire x1="2.5654" y1="4.4958" x2="2.5654" y2="3.5052" width="0" layer="51"/>
<wire x1="1.778" y1="3.5052" x2="2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="2.2352" y1="3.5052" x2="2.5654" y2="3.5052" width="0" layer="51"/>
<wire x1="2.2352" y1="3.5052" x2="2.2352" y2="4.4958" width="0" layer="51"/>
<wire x1="2.2352" y1="4.4958" x2="1.778" y2="4.4958" width="0" layer="51"/>
<wire x1="1.778" y1="4.4958" x2="1.778" y2="3.5052" width="0" layer="51"/>
<wire x1="0.9652" y1="3.5052" x2="1.4224" y2="3.5052" width="0" layer="51"/>
<wire x1="1.4224" y1="3.5052" x2="1.778" y2="3.5052" width="0" layer="51"/>
<wire x1="1.4224" y1="3.5052" x2="1.4224" y2="4.4958" width="0" layer="51"/>
<wire x1="1.4224" y1="4.4958" x2="0.9652" y2="4.4958" width="0" layer="51"/>
<wire x1="0.9652" y1="4.4958" x2="0.9652" y2="3.5052" width="0" layer="51"/>
<wire x1="0.1778" y1="3.5052" x2="0.635" y2="3.5052" width="0" layer="51"/>
<wire x1="0.635" y1="3.5052" x2="0.9652" y2="3.5052" width="0" layer="51"/>
<wire x1="0.635" y1="3.5052" x2="0.635" y2="4.4958" width="0" layer="51"/>
<wire x1="0.635" y1="4.4958" x2="0.1778" y2="4.4958" width="0" layer="51"/>
<wire x1="0.1778" y1="4.4958" x2="0.1778" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.635" y1="3.5052" x2="-0.1778" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.1778" y1="3.5052" x2="0.1778" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.1778" y1="3.5052" x2="-0.1778" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.1778" y1="4.4958" x2="-0.635" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.635" y1="4.4958" x2="-0.635" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.4224" y1="3.5052" x2="-0.9652" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.9652" y1="3.5052" x2="-0.635" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.9652" y1="3.5052" x2="-0.9652" y2="4.4958" width="0" layer="51"/>
<wire x1="-0.9652" y1="4.4958" x2="-1.4224" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.4224" y1="4.4958" x2="-1.4224" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.2352" y1="3.5052" x2="-1.778" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.778" y1="3.5052" x2="-1.4224" y2="3.5052" width="0" layer="51"/>
<wire x1="-1.778" y1="3.5052" x2="-1.778" y2="4.4958" width="0" layer="51"/>
<wire x1="-1.778" y1="4.4958" x2="-2.2352" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.2352" y1="4.4958" x2="-2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="3.5052" x2="-3.0226" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.0226" y1="3.5052" x2="-2.5654" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.5654" y1="3.5052" x2="-2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="-2.5654" y1="3.5052" x2="-2.5654" y2="4.4958" width="0" layer="51"/>
<wire x1="-2.5654" y1="4.4958" x2="-3.0226" y2="4.4958" width="0" layer="51"/>
<wire x1="-3.0226" y1="4.4958" x2="-3.0226" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="3.0226" x2="-4.4958" y2="3.0226" width="0" layer="51"/>
<wire x1="-4.4958" y1="3.0226" x2="-4.4958" y2="2.5654" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.5654" x2="-3.5052" y2="2.5654" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.778" x2="-3.5052" y2="2.2352" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.2352" x2="-3.5052" y2="2.5654" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.2352" x2="-4.4958" y2="2.2352" width="0" layer="51"/>
<wire x1="-4.4958" y1="2.2352" x2="-4.4958" y2="1.778" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.778" x2="-3.5052" y2="1.778" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.9652" x2="-3.5052" y2="1.4224" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.4224" x2="-3.5052" y2="1.778" width="0" layer="51"/>
<wire x1="-3.5052" y1="1.4224" x2="-4.4958" y2="1.4224" width="0" layer="51"/>
<wire x1="-4.4958" y1="1.4224" x2="-4.4958" y2="0.9652" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.9652" x2="-3.5052" y2="0.9652" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.1778" x2="-3.5052" y2="0.635" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.635" x2="-3.5052" y2="0.9652" width="0" layer="51"/>
<wire x1="-3.5052" y1="0.635" x2="-4.4958" y2="0.635" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.635" x2="-4.4958" y2="0.1778" width="0" layer="51"/>
<wire x1="-4.4958" y1="0.1778" x2="-3.5052" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.635" x2="-3.5052" y2="-0.1778" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.1778" x2="-3.5052" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.1778" x2="-4.4958" y2="-0.1778" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.1778" x2="-4.4958" y2="-0.635" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.635" x2="-3.5052" y2="-0.635" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.4224" x2="-3.5052" y2="-0.9652" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.9652" x2="-3.5052" y2="-0.635" width="0" layer="51"/>
<wire x1="-3.5052" y1="-0.9652" x2="-4.4958" y2="-0.9652" width="0" layer="51"/>
<wire x1="-4.4958" y1="-0.9652" x2="-4.4958" y2="-1.4224" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.4224" x2="-3.5052" y2="-1.4224" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.2352" x2="-3.5052" y2="-1.778" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.778" x2="-3.5052" y2="-1.4224" width="0" layer="51"/>
<wire x1="-3.5052" y1="-1.778" x2="-4.4958" y2="-1.778" width="0" layer="51"/>
<wire x1="-4.4958" y1="-1.778" x2="-4.4958" y2="-2.2352" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.2352" x2="-3.5052" y2="-2.2352" width="0" layer="51"/>
<wire x1="-3.5052" y1="-3.5052" x2="-3.5052" y2="-3.0226" width="0" layer="51"/>
<wire x1="-3.5052" y1="-3.0226" x2="-3.5052" y2="-2.5654" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.5654" x2="-3.5052" y2="-2.2352" width="0" layer="51"/>
<wire x1="-3.5052" y1="-2.5654" x2="-4.4958" y2="-2.5654" width="0" layer="51"/>
<wire x1="-4.4958" y1="-2.5654" x2="-4.4958" y2="-3.0226" width="0" layer="51"/>
<wire x1="-4.4958" y1="-3.0226" x2="-3.5052" y2="-3.0226" width="0" layer="51"/>
<wire x1="-3.0226" y1="-3.5052" x2="-3.0226" y2="-4.4958" width="0" layer="51"/>
<wire x1="-3.0226" y1="-4.4958" x2="-2.5654" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.5654" y1="-4.4958" x2="-2.5654" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.778" y1="-3.5052" x2="-2.2352" y2="-3.5052" width="0" layer="51"/>
<wire x1="-2.2352" y1="-3.5052" x2="-2.5654" y2="-3.5052" width="0" layer="51"/>
<wire x1="-2.2352" y1="-3.5052" x2="-2.2352" y2="-4.4958" width="0" layer="51"/>
<wire x1="-2.2352" y1="-4.4958" x2="-1.778" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.778" y1="-4.4958" x2="-1.778" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.9652" y1="-3.5052" x2="-1.4224" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.4224" y1="-3.5052" x2="-1.778" y2="-3.5052" width="0" layer="51"/>
<wire x1="-1.4224" y1="-3.5052" x2="-1.4224" y2="-4.4958" width="0" layer="51"/>
<wire x1="-1.4224" y1="-4.4958" x2="-0.9652" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.9652" y1="-4.4958" x2="-0.9652" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.1778" y1="-3.5052" x2="-0.635" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.635" y1="-3.5052" x2="-0.9652" y2="-3.5052" width="0" layer="51"/>
<wire x1="-0.635" y1="-3.5052" x2="-0.635" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.635" y1="-4.4958" x2="-0.1778" y2="-4.4958" width="0" layer="51"/>
<wire x1="-0.1778" y1="-4.4958" x2="-0.1778" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.635" y1="-3.5052" x2="0.1778" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.1778" y1="-3.5052" x2="-0.1778" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.1778" y1="-3.5052" x2="0.1778" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.1778" y1="-4.4958" x2="0.635" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.635" y1="-4.4958" x2="0.635" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.4224" y1="-3.5052" x2="0.9652" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.9652" y1="-3.5052" x2="0.635" y2="-3.5052" width="0" layer="51"/>
<wire x1="0.9652" y1="-3.5052" x2="0.9652" y2="-4.4958" width="0" layer="51"/>
<wire x1="0.9652" y1="-4.4958" x2="1.4224" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.4224" y1="-4.4958" x2="1.4224" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.2352" y1="-3.5052" x2="1.778" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.778" y1="-3.5052" x2="1.4224" y2="-3.5052" width="0" layer="51"/>
<wire x1="1.778" y1="-3.5052" x2="1.778" y2="-4.4958" width="0" layer="51"/>
<wire x1="1.778" y1="-4.4958" x2="2.2352" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.2352" y1="-4.4958" x2="2.2352" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-3.5052" x2="3.0226" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.0226" y1="-3.5052" x2="2.5654" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.5654" y1="-3.5052" x2="2.2352" y2="-3.5052" width="0" layer="51"/>
<wire x1="2.5654" y1="-3.5052" x2="2.5654" y2="-4.4958" width="0" layer="51"/>
<wire x1="2.5654" y1="-4.4958" x2="3.0226" y2="-4.4958" width="0" layer="51"/>
<wire x1="3.0226" y1="-4.4958" x2="3.0226" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-3.0226" x2="4.4958" y2="-3.0226" width="0" layer="51"/>
<wire x1="4.4958" y1="-3.0226" x2="4.4958" y2="-2.5654" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.5654" x2="3.5052" y2="-2.5654" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.778" x2="3.5052" y2="-2.2352" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.2352" x2="3.5052" y2="-2.5654" width="0" layer="51"/>
<wire x1="3.5052" y1="-2.2352" x2="4.4958" y2="-2.2352" width="0" layer="51"/>
<wire x1="4.4958" y1="-2.2352" x2="4.4958" y2="-1.778" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.778" x2="3.5052" y2="-1.778" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.9652" x2="3.5052" y2="-1.4224" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.4224" x2="3.5052" y2="-1.778" width="0" layer="51"/>
<wire x1="3.5052" y1="-1.4224" x2="4.4958" y2="-1.4224" width="0" layer="51"/>
<wire x1="4.4958" y1="-1.4224" x2="4.4958" y2="-0.9652" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.9652" x2="3.5052" y2="-0.9652" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.1778" x2="3.5052" y2="-0.635" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.635" x2="3.5052" y2="-0.9652" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.635" x2="4.4958" y2="-0.635" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.635" x2="4.4958" y2="-0.1778" width="0" layer="51"/>
<wire x1="4.4958" y1="-0.1778" x2="3.5052" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.5052" y1="0.635" x2="3.5052" y2="0.1778" width="0" layer="51"/>
<wire x1="3.5052" y1="0.1778" x2="3.5052" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.5052" y1="0.1778" x2="4.4958" y2="0.1778" width="0" layer="51"/>
<wire x1="4.4958" y1="0.1778" x2="4.4958" y2="0.635" width="0" layer="51"/>
<wire x1="4.4958" y1="0.635" x2="3.5052" y2="0.635" width="0" layer="51"/>
<wire x1="3.5052" y1="1.4224" x2="3.5052" y2="0.9652" width="0" layer="51"/>
<wire x1="3.5052" y1="0.9652" x2="3.5052" y2="0.635" width="0" layer="51"/>
<wire x1="3.5052" y1="0.9652" x2="4.4958" y2="0.9652" width="0" layer="51"/>
<wire x1="4.4958" y1="0.9652" x2="4.4958" y2="1.4224" width="0" layer="51"/>
<wire x1="4.4958" y1="1.4224" x2="3.5052" y2="1.4224" width="0" layer="51"/>
<wire x1="3.5052" y1="2.2352" x2="3.5052" y2="1.778" width="0" layer="51"/>
<wire x1="3.5052" y1="1.778" x2="3.5052" y2="1.4224" width="0" layer="51"/>
<wire x1="3.5052" y1="1.778" x2="4.4958" y2="1.778" width="0" layer="51"/>
<wire x1="4.4958" y1="1.778" x2="4.4958" y2="2.2352" width="0" layer="51"/>
<wire x1="4.4958" y1="2.2352" x2="3.5052" y2="2.2352" width="0" layer="51"/>
<wire x1="3.5052" y1="3.5052" x2="3.5052" y2="3.0226" width="0" layer="51"/>
<wire x1="3.5052" y1="3.0226" x2="3.5052" y2="2.5654" width="0" layer="51"/>
<wire x1="3.5052" y1="2.5654" x2="3.5052" y2="2.2352" width="0" layer="51"/>
<wire x1="3.5052" y1="2.5654" x2="4.4958" y2="2.5654" width="0" layer="51"/>
<wire x1="4.4958" y1="2.5654" x2="4.4958" y2="3.0226" width="0" layer="51"/>
<wire x1="4.4958" y1="3.0226" x2="3.5052" y2="3.0226" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.2352" x2="-2.2352" y2="3.5052" width="0" layer="51"/>
<wire x1="-0.1778" y1="-3.5052" x2="0.1778" y2="-3.5052" width="0" layer="51"/>
<wire x1="3.5052" y1="-0.9652" x2="3.5052" y2="0.9652" width="0" layer="51"/>
<wire x1="1.778" y1="3.5052" x2="-1.778" y2="3.5052" width="0" layer="51"/>
<wire x1="-3.5052" y1="2.5654" x2="-3.5052" y2="-2.5654" width="0" layer="51"/>
<smd name="1" x="-4.1148" y="2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-4.1148" y="2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-4.1148" y="1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-4.1148" y="0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-4.1148" y="-0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-4.1148" y="-1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-4.1148" y="-2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-4.1148" y="-2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-2.794" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="10" x="-2.0066" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="11" x="-1.1938" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="12" x="-0.4064" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="13" x="0.4064" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="14" x="1.1938" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="15" x="2.0066" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="16" x="2.794" y="-4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="17" x="4.1148" y="-2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="18" x="4.1148" y="-2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="19" x="4.1148" y="-1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="20" x="4.1148" y="-0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="21" x="4.1148" y="0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="22" x="4.1148" y="1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="23" x="4.1148" y="2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="24" x="4.1148" y="2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="25" x="2.794" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="26" x="2.0066" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="27" x="1.1938" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="28" x="0.4064" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="29" x="-0.4064" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="30" x="-1.1938" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="31" x="-2.0066" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="32" x="-2.794" y="4.1148" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<text x="-6.0706" y="2.9464" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-6.0706" y="2.9464" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-6.0706" y="2.9464" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-6.0706" y="2.9464" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-5.4864" y="-7.9756" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="7.9756" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="QFP32">
<description>&lt;b&gt;General QFP32 package&lt;/b&gt;</description>
<wire x1="3.45" y1="3.45" x2="-3.45" y2="3.45" width="0.127" layer="21"/>
<wire x1="-3.45" y1="3.45" x2="-3.45" y2="-3.45" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-3.45" x2="3.45" y2="-3.45" width="0.127" layer="21"/>
<wire x1="3.45" y1="-3.45" x2="3.45" y2="3.45" width="0.127" layer="21"/>
<circle x="-3" y="3" radius="0.2" width="0.127" layer="21"/>
<smd name="21" x="4.2" y="0.4" dx="1.5" dy="0.5" layer="1"/>
<smd name="4" x="-4.2" y="0.4" dx="1.5" dy="0.5" layer="1"/>
<smd name="22" x="4.2" y="1.2" dx="1.5" dy="0.5" layer="1"/>
<smd name="23" x="4.2" y="2" dx="1.5" dy="0.5" layer="1"/>
<smd name="24" x="4.2" y="2.8" dx="1.5" dy="0.5" layer="1"/>
<smd name="20" x="4.2" y="-0.4" dx="1.5" dy="0.5" layer="1"/>
<smd name="19" x="4.2" y="-1.2" dx="1.5" dy="0.5" layer="1"/>
<smd name="18" x="4.2" y="-2" dx="1.5" dy="0.5" layer="1"/>
<smd name="7" x="-4.2" y="-2" dx="1.5" dy="0.5" layer="1"/>
<smd name="6" x="-4.2" y="-1.2" dx="1.5" dy="0.5" layer="1"/>
<smd name="5" x="-4.2" y="-0.4" dx="1.5" dy="0.5" layer="1"/>
<smd name="3" x="-4.2" y="1.2" dx="1.5" dy="0.5" layer="1"/>
<smd name="2" x="-4.2" y="2" dx="1.5" dy="0.5" layer="1"/>
<smd name="1" x="-4.2" y="2.8" dx="1.5" dy="0.5" layer="1"/>
<smd name="32" x="-2.8" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="31" x="-2" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="30" x="-1.2" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="29" x="-0.4" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="28" x="0.4" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="27" x="1.2" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="26" x="2" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="15" x="2" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="14" x="1.2" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="13" x="0.4" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="12" x="-0.4" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="11" x="-1.2" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="10" x="-2" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="9" x="-2.8" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="8" x="-4.2" y="-2.8" dx="1.5" dy="0.5" layer="1"/>
<smd name="16" x="2.8" y="-4.2" dx="1.5" dy="0.5" layer="1" rot="R90"/>
<smd name="17" x="4.2" y="-2.8" dx="1.5" dy="0.5" layer="1" rot="R180"/>
<smd name="25" x="2.8" y="4.2" dx="1.5" dy="0.5" layer="1" rot="R270"/>
<text x="-2.4" y="1.8" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.4" y="1.2" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-3.8" y="2.7" size="0.254" layer="51">1</text>
<text x="-3.8" y="1.9" size="0.254" layer="51">2</text>
<text x="-3.8" y="1.1" size="0.254" layer="51">3</text>
<text x="-3.8" y="0.3" size="0.254" layer="51">4</text>
<text x="-3.8" y="-0.5" size="0.254" layer="51">5</text>
<text x="-3.8" y="-1.3" size="0.254" layer="51">6</text>
<text x="-3.8" y="-2.1" size="0.254" layer="51">7</text>
<text x="-3.8" y="-2.9" size="0.254" layer="51">8</text>
<text x="-2.7" y="-3.8" size="0.254" layer="51" rot="R90">9</text>
<text x="-1.9" y="-4" size="0.254" layer="51" rot="R90">10</text>
<text x="-1.1" y="-4" size="0.254" layer="51" rot="R90">11</text>
<text x="-0.3" y="-4" size="0.254" layer="51" rot="R90">12</text>
<text x="0.5" y="-4" size="0.254" layer="51" rot="R90">13</text>
<text x="1.3" y="-4" size="0.254" layer="51" rot="R90">14</text>
<text x="2.1" y="-4" size="0.254" layer="51" rot="R90">15</text>
<text x="2.9" y="-4" size="0.254" layer="51" rot="R90">16</text>
<text x="3.6" y="-2.9" size="0.254" layer="51">17</text>
<text x="3.6" y="-2.1" size="0.254" layer="51">18</text>
<text x="3.6" y="-1.3" size="0.254" layer="51">19</text>
<text x="3.6" y="-0.5" size="0.254" layer="51">20</text>
<text x="3.6" y="0.3" size="0.254" layer="51">21</text>
<text x="3.6" y="1.1" size="0.254" layer="51">22</text>
<text x="3.6" y="1.9" size="0.254" layer="51">23</text>
<text x="3.6" y="2.7" size="0.254" layer="51">24</text>
<text x="2.9" y="3.6" size="0.254" layer="51" rot="R90">25</text>
<text x="2.1" y="3.6" size="0.254" layer="51" rot="R90">26</text>
<text x="1.3" y="3.6" size="0.254" layer="51" rot="R90">27</text>
<text x="0.5" y="3.6" size="0.254" layer="51" rot="R90">28</text>
<text x="-0.3" y="3.6" size="0.254" layer="51" rot="R90">29</text>
<text x="-1.1" y="3.6" size="0.254" layer="51" rot="R90">30</text>
<text x="-1.9" y="3.6" size="0.254" layer="51" rot="R90">31</text>
<text x="-2.7" y="3.6" size="0.254" layer="51" rot="R90">32</text>
<polygon width="0.127" layer="39">
<vertex x="-3.6" y="3.6"/>
<vertex x="3.6" y="3.6"/>
<vertex x="3.6" y="-3.6"/>
<vertex x="-3.6" y="-3.6"/>
</polygon>
</package>
<package name="LED_HSMX-C150">
<description>HSMx-C150&lt;p&gt;

&lt;b&gt;GaP:&lt;p&gt;&lt;/b&gt;
Green:HSMG-C150&lt;p&gt;
HER:HSMS-C150&lt;p&gt;
Orange:HSMD-C150&lt;p&gt;
Yellow:HSMY-C150&lt;p&gt;

&lt;b&gt;AlGaAs:&lt;p&gt;&lt;/b&gt;
Red:HSMH-C150&lt;p&gt;

&lt;i&gt;http://www.avagotech.com/docs/AV02-0551EN&lt;/i&gt;</description>
<circle x="1" y="0.5" radius="0.1" width="0.127" layer="21"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="0.8" x2="-0.7" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.8" x2="0.7" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.7" y1="0.8" x2="1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.8" x2="0.7" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.7" y1="-0.8" x2="-0.7" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.7" y1="-0.8" x2="-1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="0.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.4" x2="-1.2" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.2" y1="0" x2="-1.6" y2="-0.4" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.6" y1="-0.4" x2="-1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.4" x2="1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="-0.4" width="0.127" layer="21"/>
<wire x1="1.2" y1="0" x2="1.6" y2="0.4" width="0.127" layer="21" curve="-90"/>
<wire x1="1.6" y1="-0.4" x2="1.2" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.7" y1="0.8" x2="-0.7" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.7" y1="0.8" x2="0.7" y2="-0.8" width="0.127" layer="21"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="-0.5" y1="-0.8" x2="0.5" y2="0.8" layer="21"/>
<smd name="A" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="C" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-2.5" y="1.1" size="0.6096" layer="25">&gt;NAME</text>
<text x="-2.57" y="-1.74" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="Ø4/2MM">
<description>&lt;b&gt;Plated hole Ø4/ø2mm&lt;/b&gt;&lt;p&gt;
Do not mount</description>
<pad name="1" x="0" y="0" drill="2.1" diameter="4" thermals="no"/>
<polygon width="0.2" layer="39">
<vertex x="-2" y="0" curve="-90"/>
<vertex x="0" y="2" curve="-90"/>
<vertex x="2" y="0" curve="-90"/>
<vertex x="0" y="-2" curve="-90"/>
</polygon>
</package>
<package name="Ø6/3MM">
<description>&lt;b&gt;Plated hole Ø6/ø3mm&lt;/b&gt;&lt;p&gt;
Modified 3.1mm drill, 7mm wide (for bracket tab) 10.April 2014</description>
<circle x="0" y="0" radius="3.25" width="0.1" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.8128" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.8128" layer="21"/>
<pad name="1" x="0" y="0" drill="3.1" diameter="7" thermals="no"/>
<polygon width="0.127" layer="39">
<vertex x="-3.3" y="0" curve="-90"/>
<vertex x="0" y="3.3" curve="-90"/>
<vertex x="3.3" y="0" curve="-90"/>
<vertex x="0" y="-3.3" curve="-90"/>
</polygon>
</package>
<package name="Ø8/4MM">
<description>&lt;b&gt;Plated hole Ø8/ø4mm&lt;/b&gt;&lt;p&gt;
Do not mount</description>
<circle x="0" y="0" radius="4" width="0.1" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.8128" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.8128" layer="21"/>
<pad name="1" x="0" y="0" drill="4.1" diameter="8" thermals="no"/>
<text x="-2.3" y="2.2" size="0.6096" layer="25">&gt;NAME</text>
<text x="-2.5" y="-2.9" size="0.6096" layer="27">&gt;VALUE</text>
<polygon width="0.2" layer="39">
<vertex x="-4" y="0" curve="-90"/>
<vertex x="0" y="4" curve="-90"/>
<vertex x="4" y="0" curve="-90"/>
<vertex x="0" y="-4" curve="-90"/>
</polygon>
</package>
<package name="FIDUCIAL">
<description>&lt;b&gt;Fiducial mark&lt;/b&gt;
Do not mount, but needed in pick and place list</description>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.55" width="0" layer="29"/>
<circle x="0" y="0" radius="0.4472" width="0.127" layer="21"/>
<text x="-0.51" y="0.49" size="0.254" layer="25">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="0" y="0"/>
<vertex x="-0.4" y="0" curve="-90"/>
<vertex x="0" y="0.4"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="0" y="0"/>
<vertex x="0" y="-0.4" curve="90"/>
<vertex x="0.4" y="0"/>
</polygon>
</package>
<package name="CREATOR_LOGO_20X20">
<description>Creator.no logo 20x20mm</description>
<wire x1="-7.9507875" y1="-8.074053125" x2="4.8999" y2="7.241996875" width="0.254" layer="1"/>
<wire x1="4.8999" y1="7.241996875" x2="0.955325" y2="7.919971875" width="0.254" layer="1"/>
<wire x1="0.955325" y1="7.919971875" x2="-1.602484375" y2="4.89990625" width="0.254" layer="1"/>
<wire x1="-0.2465375" y1="1.109415625" x2="-4.1911125" y2="1.81820625" width="0.254" layer="1"/>
<wire x1="-4.1911125" y1="1.81820625" x2="-6.748925" y2="-1.26349375" width="0.254" layer="1"/>
<wire x1="-6.748925" y1="-1.26349375" x2="-9.306734375" y2="-4.34519375" width="0.254" layer="1"/>
<wire x1="-9.306734375" y1="-4.34519375" x2="-7.9507875" y2="-8.074053125" width="0.254" layer="1"/>
<wire x1="-9.306734375" y1="-4.34519375" x2="-10.662684375" y2="-0.554703125" width="0.254" layer="1"/>
<wire x1="-10.662684375" y1="-0.554703125" x2="8.9985625" y2="-4.037025" width="0.254" layer="1"/>
<wire x1="8.9985625" y1="-4.037025" x2="3.852125" y2="-10.169609375" width="0.254" layer="1"/>
<wire x1="3.852125" y1="-10.169609375" x2="-2.958434375" y2="8.6287625" width="0.254" layer="1"/>
<wire x1="-2.958434375" y1="8.6287625" x2="0.955325" y2="7.919971875" width="0.254" layer="1"/>
<wire x1="0.955325" y1="7.919971875" x2="3.667221875" y2="0.431440625" width="0.254" layer="1"/>
<wire x1="3.667221875" y1="0.431440625" x2="-1.4484" y2="-5.701140625" width="0.254" layer="1"/>
<wire x1="-1.4484" y1="-5.701140625" x2="6.409934375" y2="-7.118725" width="0.254" layer="1"/>
<wire x1="6.409934375" y1="-7.118725" x2="5.084803125" y2="-3.35905" width="0.254" layer="1"/>
<wire x1="5.084803125" y1="-3.35905" x2="2.496175" y2="-6.409934375" width="0.254" layer="1"/>
<wire x1="-1.4484" y1="-5.70114375" x2="-4.1911125" y2="1.81820625" width="0.254" layer="1"/>
<wire x1="-6.748925" y1="-1.26349375" x2="-5.392978125" y2="-5.02316875" width="0.254" layer="1"/>
<wire x1="-5.392978125" y1="-5.02316875" x2="-9.306734375" y2="-4.34519375" width="0.254" layer="1"/>
<wire x1="0.955325" y1="7.919971875" x2="3.5131375" y2="11.001671875" width="0.254" layer="1"/>
<wire x1="3.5131375" y1="11.001671875" x2="4.8999" y2="7.241996875" width="0.254" layer="1"/>
<wire x1="-1.602484375" y1="4.89990625" x2="2.311271875" y2="4.160296875" width="0.254" layer="1"/>
<wire x1="-0.2465375" y1="1.109415625" x2="3.667221875" y2="0.431440625" width="0.254" layer="1"/>
<wire x1="0.955325" y1="7.919971875" x2="0.64715625" y2="8.813665625" width="0.16" layer="1"/>
<wire x1="0.64715625" y1="8.813665625" x2="2.61944375" y2="11.15575625" width="0.16" layer="1"/>
<wire x1="2.61944375" y1="11.15575625" x2="3.5131375" y2="11.001671875" width="0.16" layer="1"/>
<wire x1="-2.958434375" y1="8.6287625" x2="-3.574775" y2="7.9507875" width="0.16" layer="1"/>
<wire x1="-3.574775" y1="7.9507875" x2="-2.526996875" y2="5.023171875" width="0.16" layer="1"/>
<wire x1="-2.526996875" y1="5.023171875" x2="-1.602484375" y2="4.89990625" width="0.16" layer="1"/>
<wire x1="4.8999" y1="7.241996875" x2="5.2388875" y2="6.379121875" width="0.16" layer="1"/>
<wire x1="5.2388875" y1="6.379121875" x2="3.2666" y2="4.037028125" width="0.16" layer="1"/>
<wire x1="3.2666" y1="4.037028125" x2="2.311271875" y2="4.160296875" width="0.16" layer="1"/>
<wire x1="3.667221875" y1="0.431440625" x2="4.037025" y2="-0.462253125" width="0.16" layer="1"/>
<wire x1="4.037025" y1="-0.462253125" x2="2.033921875" y2="-2.80434375" width="0.16" layer="1"/>
<wire x1="5.084803125" y1="-3.35905" x2="5.639509375" y2="-2.650259375" width="0.16" layer="1"/>
<wire x1="5.639509375" y1="-2.650259375" x2="8.69039375" y2="-3.17415" width="0.16" layer="1"/>
<wire x1="8.69039375" y1="-3.17415" x2="8.9985625" y2="-4.037025" width="0.16" layer="1"/>
<wire x1="6.409934375" y1="-7.118725" x2="6.748921875" y2="-7.9816" width="0.16" layer="1"/>
<wire x1="6.748921875" y1="-7.9816" x2="4.776634375" y2="-10.32369375" width="0.16" layer="1"/>
<wire x1="4.776634375" y1="-10.32369375" x2="3.852121875" y2="-10.16960625" width="0.16" layer="1"/>
<wire x1="2.496175" y1="-6.409934375" x2="1.910653125" y2="-7.118725" width="0.16" layer="1"/>
<wire x1="1.910653125" y1="-7.118725" x2="-1.1094125" y2="-6.594834375" width="0.16" layer="1"/>
<wire x1="-1.1094125" y1="-6.594834375" x2="-1.4484" y2="-5.70114375" width="0.16" layer="1"/>
<wire x1="-5.392978125" y1="-5.02316875" x2="-4.468465625" y2="-5.177253125" width="0.16" layer="1"/>
<wire x1="-4.468465625" y1="-5.177253125" x2="-2.496178125" y2="-2.8351625" width="0.16" layer="1"/>
<wire x1="-7.9507875" y1="-8.074053125" x2="-8.875296875" y2="-7.919965625" width="0.16" layer="1"/>
<wire x1="-8.875296875" y1="-7.919965625" x2="-9.923075" y2="-5.02316875" width="0.16" layer="1"/>
<wire x1="-9.923075" y1="-5.02316875" x2="-9.306734375" y2="-4.34519375" width="0.16" layer="1"/>
<wire x1="-10.662684375" y1="-0.554703125" x2="-10.107978125" y2="0.1540875" width="0.16" layer="1"/>
<wire x1="-10.107978125" y1="0.1540875" x2="-7.0879125" y2="-0.369803125" width="0.16" layer="1"/>
<wire x1="-7.0879125" y1="-0.369803125" x2="-6.748925" y2="-1.26349375" width="0.16" layer="1"/>
<wire x1="-4.1911125" y1="1.81820625" x2="-3.574775" y2="2.526996875" width="0.16" layer="1"/>
<wire x1="-3.574775" y1="2.526996875" x2="-0.55470625" y2="1.972290625" width="0.16" layer="1"/>
</package>
<package name="SMD-IPC_C1210">
<description>&lt;b&gt;CAPACITOR EIA1210&lt;/b&gt;</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="21"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="21"/>
<smd name="-" x="-1.5" y="0" dx="1.15" dy="2.7" layer="1" roundness="50"/>
<smd name="+" x="1.5" y="0" dx="1.15" dy="2.7" layer="1" roundness="50"/>
<text x="-0.762" y="0.762" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.016" size="0.3048" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2955" x2="-0.9516" y2="1.3045" layer="21"/>
<rectangle x1="0.9516" y1="-1.3045" x2="1.7018" y2="1.2955" layer="21"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.2" y2="0.4002" layer="35"/>
<polygon width="0.127" layer="39">
<vertex x="-1.9" y="-1.3"/>
<vertex x="1.9" y="-1.3"/>
<vertex x="1.9" y="1.3"/>
<vertex x="-1.9" y="1.3"/>
</polygon>
<text x="0.4826" y="-0.3048" size="0.6096" layer="21">+</text>
</package>
<package name="SMD-IPC_C1812">
<description>&lt;b&gt;CAPACITOR EIA1812&lt;/b&gt;</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="21"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="21"/>
<smd name="-" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1" roundness="50"/>
<smd name="+" x="1.95" y="0" dx="1.9" dy="3.4" layer="1" roundness="50"/>
<text x="-1.4288" y="0.7938" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.27" y="-1.1113" size="0.3048" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4375" y2="1.6491" layer="21"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3979" y2="1.6491" layer="21"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3001" y2="0.4002" layer="35"/>
<polygon width="0.127" layer="39">
<vertex x="-2.7" y="1.7"/>
<vertex x="2.8" y="1.7"/>
<vertex x="2.8" y="-1.7"/>
<vertex x="-2.7" y="-1.7"/>
</polygon>
<text x="0.4318" y="-0.635" size="1.27" layer="21">+</text>
</package>
<package name="SO-08">
<description>&lt;B&gt;Small Outline Narrow Plastic Gull Wing&lt;/B&gt;&lt;p&gt;
150-mil body, package type SN&lt;p&gt;
Edited:141028 ML (shorted pads, after microchip doc DS00000049BS page 170)</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="21"/>
<smd name="2" x="-0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="-1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.905" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<text x="-2.032" y="0.762" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.032" y="0.254" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.1501" y1="-3.1001" x2="-1.6599" y2="-2" layer="21"/>
<rectangle x1="-0.8801" y1="-3.1001" x2="-0.3899" y2="-2" layer="21"/>
<rectangle x1="0.3899" y1="-3.1001" x2="0.8801" y2="-2" layer="21"/>
<rectangle x1="1.6599" y1="-3.1001" x2="2.1501" y2="-2" layer="21"/>
<rectangle x1="1.6599" y1="2" x2="2.1501" y2="3.1001" layer="21"/>
<rectangle x1="0.3899" y1="2" x2="0.8801" y2="3.1001" layer="21"/>
<rectangle x1="-0.8801" y1="2" x2="-0.3899" y2="3.1001" layer="21"/>
<rectangle x1="-2.1501" y1="2" x2="-1.6599" y2="3.1001" layer="21"/>
<polygon width="0.127" layer="39">
<vertex x="-2.5" y="3.1"/>
<vertex x="2.5" y="3.1"/>
<vertex x="2.5" y="-3.1"/>
<vertex x="-2.5" y="-3.1"/>
</polygon>
<circle x="-2" y="-1" radius="0.2" width="0.127" layer="21"/>
</package>
<package name="SOIC127P600X175-8N">
<wire x1="-2.0066" y1="1.651" x2="-2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.381" x2="-2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.889" x2="-2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.159" x2="-2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.651" x2="2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.381" x2="2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.889" x2="2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.159" x2="2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="51" curve="-180"/>
<wire x1="-1.3716" y1="-2.4892" x2="1.3716" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.3716" y1="2.4892" x2="0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-1.3716" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="21" curve="-180"/>
<smd name="1" x="-2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="2" x="-2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="3" x="-2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="4" x="-2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="5" x="2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="6" x="2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="7" x="2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="8" x="2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<text x="-3.302" y="2.3368" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.302" y="2.3368" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-4.6736" y="3.3782" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.7404" y="-5.2324" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT23-TO236AB-R">
<description>&lt;b&gt;Reflow soldering footprint SOT23 (TO-236AB)&lt;/b&gt;&lt;p&gt;

Drawn from &lt;a href=http://www.nxp.com/documents/data_sheet/BC847_SER.pdf&gt;ON BC847 series datasheet 20 August 2012&lt;/a&gt;</description>
<wire x1="1.5" y1="0.85" x2="1.5" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-0.85" x2="1.15" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="1.15" y1="-0.85" x2="1.15" y2="-1.2" width="0.1524" layer="21"/>
<wire x1="1.15" y1="-1.2" x2="0.75" y2="-1.2" width="0.1524" layer="21"/>
<wire x1="0.75" y1="-1.2" x2="0.75" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="0.75" y1="-0.85" x2="-0.75" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="-0.75" y1="-0.85" x2="-0.75" y2="-1.2" width="0.1524" layer="21"/>
<wire x1="-0.75" y1="-1.2" x2="-1.15" y2="-1.2" width="0.1524" layer="21"/>
<wire x1="-1.15" y1="-1.2" x2="-1.15" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="-1.15" y1="-0.85" x2="-1.5" y2="-0.85" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-0.85" x2="-1.5" y2="0.85" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.85" x2="-0.2" y2="0.85" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.85" x2="-0.2" y2="1.2" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="1.2" x2="0.2" y2="1.2" width="0.1524" layer="21"/>
<wire x1="0.2" y1="1.2" x2="0.2" y2="0.85" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.85" x2="1.5" y2="0.85" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1" dx="0.6" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.95" y="-1" dx="0.6" dy="0.6" layer="1" roundness="30"/>
<smd name="1" x="-0.95" y="-1" dx="0.6" dy="0.6" layer="1" roundness="30"/>
<text x="-1.143" y="0.127" size="0.254" layer="25">&gt;NAME</text>
<text x="-1.143" y="-0.381" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.1" layer="39">
<vertex x="-1.5" y="-0.85"/>
<vertex x="-1.5" y="0.85"/>
<vertex x="-0.5" y="0.85"/>
<vertex x="-0.5" y="1.5"/>
<vertex x="0.5" y="1.5"/>
<vertex x="0.5" y="0.85"/>
<vertex x="1.5" y="0.85"/>
<vertex x="1.5" y="-0.85"/>
<vertex x="1.45" y="-0.85"/>
<vertex x="1.45" y="-1.5"/>
<vertex x="-1.45" y="-1.5"/>
<vertex x="-1.45" y="-0.85"/>
</polygon>
</package>
<package name="SOT416-SC75_R">
<description>&lt;b&gt;Reflow soldering footprint SOT416 (SC-75)&lt;/b&gt;&lt;p&gt;

Drawn from &lt;a href=http://www.nxp.com/documents/data_sheet/BC847_SER.pdf&gt;ON BC847 series datasheet 20 August 2012&lt;/a&gt;
&lt;p&gt;
http://www.onsemi.com/pub/Collateral/SOLDERRM-D.PDF</description>
<wire x1="1" y1="0.4" x2="1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="1" y1="-0.4" x2="0.65" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.65" y1="-0.4" x2="0.65" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="0.65" y1="-0.9" x2="0.35" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="0.35" y1="-0.9" x2="0.35" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.35" y1="-0.4" x2="-0.35" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-0.35" y1="-0.4" x2="-0.35" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.65" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.65" y1="-0.9" x2="-0.65" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-0.65" y1="-0.4" x2="-1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-1" y1="-0.4" x2="-1" y2="0.4" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.4" x2="-0.2" y2="0.4" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.4" x2="-0.2" y2="0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="0.2" y2="0.9" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.9" x2="0.2" y2="0.4" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.4" x2="1" y2="0.4" width="0.1524" layer="21"/>
<smd name="3" x="0" y="0.6475" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.5" y="-0.6475" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<smd name="1" x="-0.5" y="-0.6475" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<text x="-0.8" y="0" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.8" y="-0.3" size="0.254" layer="27">&gt;VALUE</text>
<polygon width="0.2" layer="39">
<vertex x="-1" y="-1"/>
<vertex x="-1" y="0.65"/>
<vertex x="-0.425" y="0.65"/>
<vertex x="-0.425" y="1"/>
<vertex x="0.425" y="1"/>
<vertex x="0.425" y="0.65"/>
<vertex x="1" y="0.65"/>
<vertex x="1" y="-1"/>
</polygon>
</package>
<package name="SOT883-SC101_R">
<description>&lt;b&gt;Reflow soldering footprint SOT883 (SC-101)&lt;/b&gt;&lt;p&gt;

Drawn from &lt;a href=http://www.nxp.com/documents/data_sheet/BC847_SER.pdf&gt;ON BC847 series datasheet 20 August 2012&lt;/a&gt;</description>
<smd name="3" x="0" y="0.35" dx="0.6" dy="0.3" layer="1" roundness="30"/>
<smd name="2" x="0.175" y="-0.35" dx="0.25" dy="0.3" layer="1" roundness="30"/>
<smd name="1" x="-0.175" y="-0.35" dx="0.25" dy="0.3" layer="1" roundness="30"/>
<text x="-0.4" y="-0.4" size="0.127" layer="25" rot="R90">&gt;NAME</text>
<text x="0.5" y="-0.4" size="0.127" layer="27" rot="R90">&gt;VALUE</text>
<polygon width="0.1" layer="39">
<vertex x="-0.45" y="-0.65"/>
<vertex x="-0.45" y="0.65"/>
<vertex x="0.45" y="0.65"/>
<vertex x="0.45" y="-0.65"/>
</polygon>
</package>
<package name="CSTER12M">
<description>Source: http://search.murata.co.jp/Ceramy/CatalogshowpageAction.do?sDirnm=A07X&amp;sFilnm=81G07006&amp;sType=2&amp;sLang=en&amp;sNHinnm=CSTCR6M00G53Z-R0&amp;sCapt=Standard_Land_Pattern_Dimensions</description>
<wire x1="-2.2" y1="-0.95" x2="2.2" y2="-0.95" width="0.1016" layer="51"/>
<wire x1="2.2" y1="-0.95" x2="2.2" y2="0.95" width="0.1016" layer="21"/>
<wire x1="2.2" y1="0.95" x2="-2.2" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-2.2" y1="0.95" x2="-2.2" y2="-0.95" width="0.1016" layer="21"/>
<smd name="1" x="-1.2" y="0" dx="0.4" dy="2.6" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="0" dx="0.4" dy="2.6" layer="1" stop="no" cream="no"/>
<smd name="3" x="1.2" y="0" dx="0.4" dy="2.6" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-1.3" x2="0.2" y2="1.3" layer="31"/>
<rectangle x1="-1.5" y1="-1.4" x2="-0.9" y2="1.4" layer="29"/>
<rectangle x1="1" y1="-1.3" x2="1.4" y2="1.3" layer="31"/>
<rectangle x1="-1.4" y1="-1.3" x2="-1" y2="1.3" layer="31"/>
<rectangle x1="-0.3" y1="-1.4" x2="0.3" y2="1.4" layer="29"/>
<rectangle x1="0.9" y1="-1.4" x2="1.5" y2="1.4" layer="29"/>
</package>
<package name="CSTCE">
<description>&lt;p&gt;Small ceramic rezonator with capacitors

&lt;ul&gt;
&lt;li&gt;&lt;a href="http://search.murata.co.jp/Ceramy/CatalogAction.do?sHinnm=?&amp;nbsp&amp;sNHinnm=CSTCE8M00G52-R0&amp;sNhin_key=CSTCE8M00G52-R0&amp;sLang=en&amp;sParam=CSTCE"&gt;Datasheet&lt;/a&gt;
&lt;li&gt;&lt;a href="http://search.murata.co.jp/Ceramy/CatalogshowpageAction.do?sParam=CSTCE&amp;sKno=G002&amp;sTblid=A703&amp;sDirnm=A07X&amp;sFilnm=81G02002&amp;sType=2&amp;sLang=en&amp;sNHinnm=CSTCE8M00G52-R0&amp;sCapt=Dimensions"&gt;Dimensions&lt;/a&gt;
&lt;li&gt;&lt;a href="http://search.murata.co.jp/Ceramy/CatalogshowpageAction.do?sParam=CSTCE&amp;sKno=G006&amp;sTblid=A703&amp;sDirnm=A07X&amp;sFilnm=81G07013&amp;sType=2&amp;sLang=en&amp;sNHinnm=CSTCE8M00G52-R0&amp;sCapt=Standard_Land_Pattern"&gt;Land pattern&lt;/a&gt;
&lt;/ul&gt;</description>
<text x="-2.54" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-1.2" y="0" dx="0.4" dy="2.1" layer="1"/>
<smd name="2" x="0" y="0" dx="0.4" dy="2.1" layer="1"/>
<smd name="3" x="1.2" y="0" dx="0.4" dy="2.1" layer="1"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.1" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="1.6" y2="-0.7" width="0.1" layer="21"/>
<wire x1="1.6" y1="-0.7" x2="1.6" y2="0.7" width="0.1" layer="21"/>
<wire x1="1.6" y1="0.7" x2="-1.6" y2="0.7" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TVSDIODEX2">
<description>Transient Voltage Supressor, generic, based on Bourns CDSOT23-SM712</description>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<pin name="A1" x="-7.62" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="A2" x="-7.62" y="2.54" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="CC" x="5.08" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="-2.54" y="0" size="1.016" layer="95" rot="MR0" align="bottom-center">&gt;NAME</text>
<text x="-2.54" y="0" size="1.016" layer="96" rot="MR0" align="top-center">&gt;VALUE</text>
<wire x1="1.27" y1="-1.27" x2="1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="0.762" y2="-4.318" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="0.762" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.778" y2="4.318" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-3.81" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="3.81" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-6.35" y1="1.27" x2="-6.858" y2="0.762" width="0.254" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-5.842" y2="4.318" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-5.842" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="-6.858" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="SMD-IPC_CAP">
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="1.016" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="1.016" y="-2.032" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="-2.286" size="0.8128" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.286" y="-1.016" size="0.254" layer="94">&gt;SIZE</text>
</symbol>
<symbol name="B">
<wire x1="-2.54" y1="5.08" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="1.27" y1="1.27" x2="5.08" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="5.08" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="1.397" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="1.397" width="0.1524" layer="94"/>
<pin name="+" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="-" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="BAW56">
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<pin name="AA" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="C1" x="-7.62" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="C2" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-0.254" y="0" size="1.016" layer="95" rot="MR0">&gt;NAME</text>
<text x="-0.254" y="-1.016" size="1.016" layer="96" rot="MR0">&gt;VALUE</text>
</symbol>
<symbol name="SMD-IPC_CAPPOL">
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="+" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="-" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<text x="1.524" y="1.778" size="0.8128" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.286" size="0.8128" layer="96">&gt;VALUE</text>
<text x="2.54" y="0" size="1.778" layer="94">+</text>
</symbol>
<symbol name="PNP">
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<text x="1.524" y="3.048" size="1.016" layer="95" rot="MR0">&gt;NAME</text>
<text x="1.524" y="-4.064" size="1.016" layer="96" rot="MR0">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ATMEGA328P-AU-1">
<wire x1="-12.7" y1="27.94" x2="-12.7" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-33.02" x2="12.7" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-33.02" x2="12.7" y2="27.94" width="0.4064" layer="94"/>
<wire x1="12.7" y1="27.94" x2="-12.7" y2="27.94" width="0.4064" layer="94"/>
<pin name="PE2" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="PE3" x="17.78" y="-27.94" length="middle" rot="R180"/>
<pin name="AREF" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="AVCC" x="-17.78" y="20.32" length="middle" direction="pwr"/>
<pin name="GND" x="-17.78" y="-25.4" length="middle" direction="pas"/>
<pin name="PE0" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="GND_2" x="-17.78" y="-27.94" length="middle" direction="pas"/>
<pin name="PB0" x="-17.78" y="10.16" length="middle"/>
<pin name="PB1" x="-17.78" y="7.62" length="middle"/>
<pin name="PB2" x="-17.78" y="5.08" length="middle"/>
<pin name="PB3" x="-17.78" y="2.54" length="middle"/>
<pin name="PB4" x="-17.78" y="0" length="middle"/>
<pin name="PB5" x="-17.78" y="-2.54" length="middle"/>
<pin name="PB6" x="-17.78" y="-5.08" length="middle"/>
<pin name="PB7" x="-17.78" y="-7.62" length="middle"/>
<pin name="PC0" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="PC1" x="17.78" y="20.32" length="middle" rot="R180"/>
<pin name="PC2" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="PC3" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="PC4" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="PC5" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PC6" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PD0" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="PD1" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PD2" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="PD3" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PD5" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PD6" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="PD7" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="PE1" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="VCC" x="-17.78" y="22.86" length="middle" direction="pwr"/>
<text x="-5.4864" y="28.8544" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.8768" y="-35.941" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="LED_MONOCHROME">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-0.1" y1="1.58" x2="0.5" y2="3.08" width="0.254" layer="94"/>
<wire x1="-1.5" y1="1.58" x2="-1" y2="3.08" width="0.254" layer="94"/>
<wire x1="0.8" y1="2.58" x2="0.5" y2="3.08" width="0.254" layer="94"/>
<wire x1="0" y1="2.88" x2="0.5" y2="3.08" width="0.254" layer="94"/>
<wire x1="-0.7" y1="2.58" x2="-1" y2="3.08" width="0.254" layer="94"/>
<wire x1="-1" y1="3.08" x2="-1.5" y2="2.88" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="pad" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="pad" length="point" direction="pas" rot="R180"/>
<text x="-2.54" y="-3.175" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.175" size="1.27" layer="96">&gt;COLOR</text>
</symbol>
<symbol name="HOLE_PLET">
<description>Plated round hole</description>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<pin name="1" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="-1.27" y="0" size="0.6096" layer="95">&gt;NAME</text>
<text x="-1.27" y="-1.27" size="0.6096" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="FIDUCIAL_MARK">
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="94">fiduc</text>
<text x="-4.572" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<polygon width="0.254" layer="94">
<vertex x="-1.27" y="0"/>
<vertex x="0" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="0" y="-1.27"/>
</polygon>
</symbol>
<symbol name="CREATOR_LOGO">
<wire x1="-6.6807875" y1="-8.074053125" x2="6.1699" y2="7.241996875" width="0.254" layer="94"/>
<wire x1="6.1699" y1="7.241996875" x2="2.225325" y2="7.919971875" width="0.254" layer="94"/>
<wire x1="2.225325" y1="7.919971875" x2="-0.332484375" y2="4.89990625" width="0.254" layer="94"/>
<wire x1="1.0234625" y1="1.109415625" x2="-2.9211125" y2="1.81820625" width="0.254" layer="94"/>
<wire x1="-2.9211125" y1="1.81820625" x2="-5.478925" y2="-1.26349375" width="0.254" layer="94"/>
<wire x1="-5.478925" y1="-1.26349375" x2="-8.036734375" y2="-4.34519375" width="0.254" layer="94"/>
<wire x1="-8.036734375" y1="-4.34519375" x2="-6.6807875" y2="-8.074053125" width="0.254" layer="94"/>
<wire x1="-8.036734375" y1="-4.34519375" x2="-9.392684375" y2="-0.554703125" width="0.254" layer="94"/>
<wire x1="-9.392684375" y1="-0.554703125" x2="10.2685625" y2="-4.037025" width="0.254" layer="94"/>
<wire x1="10.2685625" y1="-4.037025" x2="5.122125" y2="-10.169609375" width="0.254" layer="94"/>
<wire x1="5.122125" y1="-10.169609375" x2="-1.688434375" y2="8.6287625" width="0.254" layer="94"/>
<wire x1="-1.688434375" y1="8.6287625" x2="2.225325" y2="7.919971875" width="0.254" layer="94"/>
<wire x1="2.225325" y1="7.919971875" x2="4.937221875" y2="0.431440625" width="0.254" layer="94"/>
<wire x1="4.937221875" y1="0.431440625" x2="-0.1784" y2="-5.701140625" width="0.254" layer="94"/>
<wire x1="-0.1784" y1="-5.701140625" x2="7.679934375" y2="-7.118725" width="0.254" layer="94"/>
<wire x1="7.679934375" y1="-7.118725" x2="6.354803125" y2="-3.35905" width="0.254" layer="94"/>
<wire x1="6.354803125" y1="-3.35905" x2="3.766175" y2="-6.409934375" width="0.254" layer="94"/>
<wire x1="-0.1784" y1="-5.70114375" x2="-2.9211125" y2="1.81820625" width="0.254" layer="94"/>
<wire x1="-5.478925" y1="-1.26349375" x2="-4.122978125" y2="-5.02316875" width="0.254" layer="94"/>
<wire x1="-4.122978125" y1="-5.02316875" x2="-8.036734375" y2="-4.34519375" width="0.254" layer="94"/>
<wire x1="2.225325" y1="7.919971875" x2="4.7831375" y2="11.001671875" width="0.254" layer="94"/>
<wire x1="4.7831375" y1="11.001671875" x2="6.1699" y2="7.241996875" width="0.254" layer="94"/>
<wire x1="-0.332484375" y1="4.89990625" x2="3.581271875" y2="4.160296875" width="0.254" layer="94"/>
<wire x1="1.0234625" y1="1.109415625" x2="4.937221875" y2="0.431440625" width="0.254" layer="94"/>
<wire x1="2.225325" y1="7.919971875" x2="1.91715625" y2="8.813665625" width="0.127" layer="94"/>
<wire x1="1.91715625" y1="8.813665625" x2="3.88944375" y2="11.15575625" width="0.127" layer="94"/>
<wire x1="3.88944375" y1="11.15575625" x2="4.7831375" y2="11.001671875" width="0.127" layer="94"/>
<wire x1="-1.688434375" y1="8.6287625" x2="-2.304775" y2="7.9507875" width="0.127" layer="94"/>
<wire x1="-2.304775" y1="7.9507875" x2="-1.256996875" y2="5.023171875" width="0.127" layer="94"/>
<wire x1="-1.256996875" y1="5.023171875" x2="-0.332484375" y2="4.89990625" width="0.127" layer="94"/>
<wire x1="6.1699" y1="7.241996875" x2="6.5088875" y2="6.379121875" width="0.127" layer="94"/>
<wire x1="6.5088875" y1="6.379121875" x2="4.5366" y2="4.037028125" width="0.127" layer="94"/>
<wire x1="4.5366" y1="4.037028125" x2="3.581271875" y2="4.160296875" width="0.127" layer="94"/>
<wire x1="4.937221875" y1="0.431440625" x2="5.307025" y2="-0.462253125" width="0.127" layer="94"/>
<wire x1="5.307025" y1="-0.462253125" x2="3.303921875" y2="-2.80434375" width="0.127" layer="94"/>
<wire x1="6.354803125" y1="-3.35905" x2="6.909509375" y2="-2.650259375" width="0.127" layer="94"/>
<wire x1="6.909509375" y1="-2.650259375" x2="9.96039375" y2="-3.17415" width="0.127" layer="94"/>
<wire x1="9.96039375" y1="-3.17415" x2="10.2685625" y2="-4.037025" width="0.127" layer="94"/>
<wire x1="7.679934375" y1="-7.118725" x2="8.018921875" y2="-7.9816" width="0.127" layer="94"/>
<wire x1="8.018921875" y1="-7.9816" x2="6.046634375" y2="-10.32369375" width="0.127" layer="94"/>
<wire x1="6.046634375" y1="-10.32369375" x2="5.122121875" y2="-10.16960625" width="0.127" layer="94"/>
<wire x1="3.766175" y1="-6.409934375" x2="3.180653125" y2="-7.118725" width="0.127" layer="94"/>
<wire x1="3.180653125" y1="-7.118725" x2="0.1605875" y2="-6.594834375" width="0.127" layer="94"/>
<wire x1="0.1605875" y1="-6.594834375" x2="-0.1784" y2="-5.70114375" width="0.127" layer="94"/>
<wire x1="-4.122978125" y1="-5.02316875" x2="-3.198465625" y2="-5.177253125" width="0.127" layer="94"/>
<wire x1="-3.198465625" y1="-5.177253125" x2="-1.226178125" y2="-2.8351625" width="0.127" layer="94"/>
<wire x1="-6.6807875" y1="-8.074053125" x2="-7.605296875" y2="-7.919965625" width="0.127" layer="94"/>
<wire x1="-7.605296875" y1="-7.919965625" x2="-8.653075" y2="-5.02316875" width="0.127" layer="94"/>
<wire x1="-8.653075" y1="-5.02316875" x2="-8.036734375" y2="-4.34519375" width="0.127" layer="94"/>
<wire x1="-9.392684375" y1="-0.554703125" x2="-8.837978125" y2="0.1540875" width="0.127" layer="94"/>
<wire x1="-8.837978125" y1="0.1540875" x2="-5.8179125" y2="-0.369803125" width="0.127" layer="94"/>
<wire x1="-5.8179125" y1="-0.369803125" x2="-5.478925" y2="-1.26349375" width="0.127" layer="94"/>
<wire x1="-2.9211125" y1="1.81820625" x2="-2.304775" y2="2.526996875" width="0.127" layer="94"/>
<wire x1="-2.304775" y1="2.526996875" x2="0.71529375" y2="1.972290625" width="0.127" layer="94"/>
</symbol>
<symbol name="FUSE_RES">
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-2.286" y1="-0.254" x2="-2.032" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.254" x2="-1.524" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-1.27" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="-0.508" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-0.254" x2="-0.254" y2="-0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="1.524" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.8128" layer="96">&gt;VALUE</text>
<text x="-1.524" y="-1.016" size="0.8128" layer="94">t</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="+24V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="A4L-LOC">
<wire x1="179.94066875" y1="3.80971875" x2="231.37566875" y2="3.80971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="3.80971875" x2="265.03066875" y2="3.80971875" width="0.1016" layer="94"/>
<wire x1="265.03066875" y1="3.80971875" x2="275.19066875" y2="3.80971875" width="0.1016" layer="94"/>
<wire x1="275.19066875" y1="3.80971875" x2="275.19066875" y2="8.88971875" width="0.1016" layer="94"/>
<wire x1="275.19066875" y1="8.88971875" x2="275.19066875" y2="13.96971875" width="0.1016" layer="94"/>
<wire x1="275.19066875" y1="13.96971875" x2="275.19066875" y2="19.04971875" width="0.1016" layer="94"/>
<wire x1="275.19066875" y1="19.04971875" x2="275.19066875" y2="24.12971875" width="0.1016" layer="94"/>
<wire x1="179.94066875" y1="3.80971875" x2="179.94066875" y2="24.12971875" width="0.1016" layer="94"/>
<wire x1="179.94066875" y1="24.12971875" x2="231.37566875" y2="24.12971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="24.12971875" x2="275.19066875" y2="24.12971875" width="0.1016" layer="94"/>
<wire x1="265.03066875" y1="3.80971875" x2="265.03066875" y2="8.88971875" width="0.1016" layer="94"/>
<wire x1="265.03066875" y1="8.88971875" x2="275.19066875" y2="8.88971875" width="0.1016" layer="94"/>
<wire x1="265.03066875" y1="8.88971875" x2="231.37566875" y2="8.88971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="8.88971875" x2="231.37566875" y2="3.80971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="8.88971875" x2="231.37566875" y2="13.96971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="13.96971875" x2="275.19066875" y2="13.96971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="13.96971875" x2="231.37566875" y2="19.04971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="19.04971875" x2="275.19066875" y2="19.04971875" width="0.1016" layer="94"/>
<wire x1="231.37566875" y1="19.04971875" x2="231.37566875" y2="24.12971875" width="0.1016" layer="94"/>
<wire x1="179.94066875" y1="24.12971875" x2="179.94066875" y2="27.93971875" width="0" layer="94"/>
<wire x1="179.94066875" y1="27.93971875" x2="275.19066875" y2="27.93971875" width="0" layer="94"/>
<text x="244.71066875" y="15.23971875" size="2.286" layer="94">&gt;DRAWING_NAME</text>
<text x="244.71066875" y="10.15971875" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="244.71066875" y="5.07971875" size="2.286" layer="94">&gt;SHEET</text>
<text x="233.02666875" y="4.95271875" size="2.54" layer="94">Sheet:</text>
<text x="181.21066875" y="15.23971875" size="3.81" layer="94">&gt;OWNER</text>
<text x="233.28066875" y="20.31971875" size="2.54" layer="94">Ver:</text>
<text x="244.71066875" y="20.31971875" size="2.286" layer="94">&gt;VERSION</text>
<text x="233.28066875" y="15.23971875" size="2.54" layer="94">Name:</text>
<text x="233.28066875" y="10.15971875" size="2.54" layer="94">Saved:</text>
<text x="181.21066875" y="20.31971875" size="2.54" layer="94">Designed by:</text>
<text x="181.21066875" y="25.39971875" size="1.778" layer="94">&gt;DESCRIPTION</text>
<frame x1="0" y1="0" x2="279" y2="192" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="SN75HVD08DR">
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="12.7" width="0.4064" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.4064" layer="94"/>
<pin name="A" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="B" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="D" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="DE" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-12.7" length="middle" direction="pas"/>
<pin name="R" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="VCC" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="~RE" x="-17.78" y="2.54" length="middle" direction="in"/>
<text x="-5.588" y="14.5796" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.8862" y="-20.7518" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="1.778" y="3.048" size="1.016" layer="95" rot="MR0">&gt;NAME</text>
<text x="1.778" y="-4.064" size="1.016" layer="96" rot="MR0">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="CST">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="-0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="-1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-2.286" x2="1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.286" x2="2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.286" x2="-1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.286" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.302" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<circle x="-3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="0" y="-3.302" radius="0.254" width="0" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TVSDIODEX2" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="TVSDIODEX2" x="2.54" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="CC" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="SUP-3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-IPC_CAP" prefix="C" uservalue="yes">
<description>Capacitor, non polarized</description>
<gates>
<gate name="G$1" symbol="SMD-IPC_CAP" x="0" y="0"/>
</gates>
<devices>
<device name="C0201" package="SMD-IPC_C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201X" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201_HAPRO" package="C0201_HAPRO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0306" package="SMD-IPC_C0306">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="SMD-IPC_C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="SMD-IPC_C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="SMD-IPC_C1206">
<connects>
<connect gate="G$1" pin="1" pad="-"/>
<connect gate="G$1" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="SMD-IPC_C1210">
<connects>
<connect gate="G$1" pin="1" pad="-"/>
<connect gate="G$1" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="SMD-IPC_C3225">
<connects>
<connect gate="G$1" pin="1" pad="-"/>
<connect gate="G$1" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C_EIA0402" package="SMD-IPC_CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-IPC_RES" prefix="R" uservalue="yes">
<description>Resistor</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="0201"/>
</technology>
</technologies>
</device>
<device name="R0402" package="SMD-IPC_R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="0402"/>
</technology>
</technologies>
</device>
<device name="R0603" package="SMD-IPC_R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="0603"/>
</technology>
</technologies>
</device>
<device name="R0805" package="SMD-IPC_R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="0805"/>
</technology>
</technologies>
</device>
<device name="R0805W" package="SMD-IPC_R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="0805(wave)"/>
</technology>
</technologies>
</device>
<device name="R1206" package="SMD-IPC_R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="1206"/>
</technology>
</technologies>
</device>
<device name="R1206W" package="SMD-IPC_R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SIZE" value="1206(wave)"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUZZER_PASSIVE" prefix="S">
<description>Generic passive buzzer</description>
<gates>
<gate name="G$1" symbol="B" x="-5.08" y="5.08"/>
</gates>
<devices>
<device name="_BESTAR_BSP1212" package="BSP1212-03H03-06_LF">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAW56" prefix="D" uservalue="yes">
<description>Double diode, common anode</description>
<gates>
<gate name="G$1" symbol="BAW56" x="0" y="0"/>
</gates>
<devices>
<device name="1-2-3" package="SOT23">
<connects>
<connect gate="G$1" pin="AA" pad="3"/>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-IPC_CAPPOL" prefix="C" uservalue="yes">
<description>Polarized capacitor</description>
<gates>
<gate name="POS" symbol="SMD-IPC_CAPPOL" x="0" y="0"/>
</gates>
<devices>
<device name="C1206" package="SMD-IPC_C1206">
<connects>
<connect gate="POS" pin="+" pad="-"/>
<connect gate="POS" pin="-" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="SMD-IPC_C1210">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="SMD-IPC_C1310">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="SMD-IPC_C1608">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="SMD-IPC_C1812">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="SMD-IPC_C1825">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="SMD-IPC_C2012">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="SMD-IPC_C3216">
<connects>
<connect gate="POS" pin="+" pad="P"/>
<connect gate="POS" pin="-" pad="N"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="SMD-IPC_C3225">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3528" package="SMD-IPC_CT3528">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="SMD-IPC_C4532">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="SMD-IPC_C4564">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C6032" package="SMD-IPC_CT6032">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C7343" package="SMD-IPC_CT7343">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PXE_F" package="PXE-F">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PXE_H" package="PXE-H">
<connects>
<connect gate="POS" pin="+" pad="+"/>
<connect gate="POS" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR_PNP" prefix="Q" uservalue="yes">
<description>&lt;b&gt;PNP Transistor&lt;/b&gt;</description>
<gates>
<gate name="Q" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="_ON-SOT23" package="SOT23-TO236AB-R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT416" package="SOT416-SC75_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT883" package="SOT883-SC101_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON_SOT323" package="SOT323-SC70_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SOT23" package="SOT23">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328PB-AU" prefix="U">
<description>8-bit Microcontroller with In-System Programmable Flash</description>
<gates>
<gate name="A" symbol="ATMEGA328P-AU-1" x="0" y="0"/>
</gates>
<devices>
<device name="32N" package="QFP80P900X900X120-32N">
<connects>
<connect gate="A" pin="AREF" pad="20"/>
<connect gate="A" pin="AVCC" pad="18"/>
<connect gate="A" pin="GND" pad="21"/>
<connect gate="A" pin="GND_2" pad="5"/>
<connect gate="A" pin="PB0" pad="12"/>
<connect gate="A" pin="PB1" pad="13"/>
<connect gate="A" pin="PB2" pad="14"/>
<connect gate="A" pin="PB3" pad="15"/>
<connect gate="A" pin="PB4" pad="16"/>
<connect gate="A" pin="PB5" pad="17"/>
<connect gate="A" pin="PB6" pad="7"/>
<connect gate="A" pin="PB7" pad="8"/>
<connect gate="A" pin="PC0" pad="23"/>
<connect gate="A" pin="PC1" pad="24"/>
<connect gate="A" pin="PC2" pad="25"/>
<connect gate="A" pin="PC3" pad="26"/>
<connect gate="A" pin="PC4" pad="27"/>
<connect gate="A" pin="PC5" pad="28"/>
<connect gate="A" pin="PC6" pad="29"/>
<connect gate="A" pin="PD0" pad="30"/>
<connect gate="A" pin="PD1" pad="31"/>
<connect gate="A" pin="PD2" pad="32"/>
<connect gate="A" pin="PD3" pad="1"/>
<connect gate="A" pin="PD4" pad="2"/>
<connect gate="A" pin="PD5" pad="9"/>
<connect gate="A" pin="PD6" pad="10"/>
<connect gate="A" pin="PD7" pad="11"/>
<connect gate="A" pin="PE0" pad="3"/>
<connect gate="A" pin="PE1" pad="6"/>
<connect gate="A" pin="PE2" pad="19"/>
<connect gate="A" pin="PE3" pad="22"/>
<connect gate="A" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="ATMEGA328P-AU" constant="no"/>
<attribute name="OC_FARNELL" value="1715486" constant="no"/>
<attribute name="OC_NEWARK" value="14R4631" constant="no"/>
<attribute name="PACKAGE" value="TQFP-32" constant="no"/>
<attribute name="SUPPLIER" value="Atmel" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="QFP32">
<connects>
<connect gate="A" pin="AREF" pad="20"/>
<connect gate="A" pin="AVCC" pad="18"/>
<connect gate="A" pin="GND" pad="21"/>
<connect gate="A" pin="GND_2" pad="5"/>
<connect gate="A" pin="PB0" pad="12"/>
<connect gate="A" pin="PB1" pad="13"/>
<connect gate="A" pin="PB2" pad="14"/>
<connect gate="A" pin="PB3" pad="15"/>
<connect gate="A" pin="PB4" pad="16"/>
<connect gate="A" pin="PB5" pad="17"/>
<connect gate="A" pin="PB6" pad="7"/>
<connect gate="A" pin="PB7" pad="8"/>
<connect gate="A" pin="PC0" pad="23"/>
<connect gate="A" pin="PC1" pad="24"/>
<connect gate="A" pin="PC2" pad="25"/>
<connect gate="A" pin="PC3" pad="26"/>
<connect gate="A" pin="PC4" pad="27"/>
<connect gate="A" pin="PC5" pad="28"/>
<connect gate="A" pin="PC6" pad="29"/>
<connect gate="A" pin="PD0" pad="30"/>
<connect gate="A" pin="PD1" pad="31"/>
<connect gate="A" pin="PD2" pad="32"/>
<connect gate="A" pin="PD3" pad="1"/>
<connect gate="A" pin="PD4" pad="2"/>
<connect gate="A" pin="PD5" pad="9"/>
<connect gate="A" pin="PD6" pad="10"/>
<connect gate="A" pin="PD7" pad="11"/>
<connect gate="A" pin="PE0" pad="3"/>
<connect gate="A" pin="PE1" pad="6"/>
<connect gate="A" pin="PE2" pad="19"/>
<connect gate="A" pin="PE3" pad="22"/>
<connect gate="A" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_MONOCHROME" prefix="D" uservalue="yes">
<description>General monochrome LED</description>
<gates>
<gate name="G$1" symbol="LED_MONOCHROME" x="0" y="0"/>
</gates>
<devices>
<device name="_AVAGO-HSMX-C150" package="LED_HSMX-C150">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="COLOR" value="(unset)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HOLE_PLET" prefix="H">
<description>Plated hole</description>
<gates>
<gate name="G$1" symbol="HOLE_PLET" x="0" y="5.08"/>
</gates>
<devices>
<device name="2MM" package="Ø4/2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="NOTINBOM" value="1"/>
</technology>
</technologies>
</device>
<device name="3MM" package="Ø6/3MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="NOTINBOM" value="1"/>
</technology>
</technologies>
</device>
<device name="4MM" package="Ø8/4MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="NOTINBOM" value="1"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL_MARK" prefix="FID">
<description>Fiducial mark for pick&amp;place</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL_MARK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL">
<technologies>
<technology name="">
<attribute name="NOTINBOM" value="1"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CREATOR_LOGO" prefix="LOGO">
<description>Creator.no logo</description>
<gates>
<gate name="G$1" symbol="CREATOR_LOGO" x="2.54" y="0"/>
</gates>
<devices>
<device name="20X20" package="CREATOR_LOGO_20X20">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINISMDC014F" prefix="F">
<gates>
<gate name="G$1" symbol="FUSE_RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD-IPC_C1812">
<connects>
<connect gate="G$1" pin="1" pad="-"/>
<connect gate="G$1" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="SUP-24V">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A4L-LOC" prefix="FRAME">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="(set DESCRIPTION attribute)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN75HVD08DR" prefix="U">
<description>WIDE SUPPLY RANGE RS-485 TRANSCEIVER</description>
<gates>
<gate name="A" symbol="SN75HVD08DR" x="0" y="0"/>
</gates>
<devices>
<device name="8N" package="SOIC127P600X175-8N">
<connects>
<connect gate="A" pin="A" pad="6"/>
<connect gate="A" pin="B" pad="7"/>
<connect gate="A" pin="D" pad="4"/>
<connect gate="A" pin="DE" pad="3"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="R" pad="1"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="~RE" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="SN75HVD08DR" constant="no"/>
<attribute name="OC_FARNELL" value="1470440" constant="no"/>
<attribute name="OC_NEWARK" value="32H6457" constant="no"/>
<attribute name="PACKAGE" value="SOIC-8" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="SO-08">
<connects>
<connect gate="A" pin="A" pad="6"/>
<connect gate="A" pin="B" pad="7"/>
<connect gate="A" pin="D" pad="4"/>
<connect gate="A" pin="DE" pad="3"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="R" pad="1"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="~RE" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR_NPN" prefix="Q" uservalue="yes">
<description>&lt;b&gt;NPN Transistor&lt;/b&gt;</description>
<gates>
<gate name="Q" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT23" package="SOT23">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT23" package="SOT23-TO236AB-R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT323" package="SOT323-SC70_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT416" package="SOT416-SC75_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ON-SOT883" package="SOT883-SC101_R">
<connects>
<connect gate="Q" pin="B" pad="1"/>
<connect gate="Q" pin="C" pad="3"/>
<connect gate="Q" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CSTC?" prefix="ZQ">
<description>&lt;h1&gt;Resonator&lt;/h1&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href="http://search.murata.co.jp/Ceramy/CatalogAction.do?sHinnm=?&amp;nbsp&amp;sNHinnm=CSTCR6M00G53-R0&amp;sNhin_key=CSTCR6M00G53-R0&amp;sLang=en&amp;sParam=CSTCR"&gt;CSTCR ­- 2.0 x 4.5 mm&lt;/a&gt;
&lt;li&gt;&lt;a href="http://search.murata.co.jp/Ceramy/CatalogAction.do?sHinnm=?&amp;nbsp&amp;sNHinnm=CSTCE8M00G52-R0&amp;sNhin_key=CSTCE8M00G52-R0&amp;sLang=en&amp;sParam=CSTCE"&gt;CSTCE ­- 1.3 x 3.2 mm&lt;/a&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CST" x="0" y="0"/>
</gates>
<devices>
<device name="R" package="CSTER12M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="CSTCE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="8M00G52-R0">
<attribute name="FREQUENCY" value="8 MHz"/>
<attribute name="OC_MOUSER" value="81-CSTCE8M00G52-R0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="optocoupler">
<description>&lt;b&gt;Opto Couplers&lt;/b&gt;&lt;p&gt;
Siemens, Hewlett-Packard, Texas Instuments, Sharp, Motorola&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMD4">
<description>&lt;b&gt;SMD 4&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/83673/sfh618.pdf</description>
<wire x1="2.315" y1="3.18" x2="2.315" y2="-3.18" width="0.1524" layer="21"/>
<wire x1="-2.315" y1="-1.085" x2="-2.315" y2="3.18" width="0.1524" layer="21"/>
<wire x1="-2.315" y1="3.18" x2="2.315" y2="3.18" width="0.1524" layer="51"/>
<wire x1="2.315" y1="-3.18" x2="-2.315" y2="-3.18" width="0.1524" layer="51"/>
<wire x1="-2.315" y1="-2.13" x2="-2.315" y2="-1.085" width="0.1524" layer="21" curve="180"/>
<wire x1="-2.315" y1="-3.18" x2="-2.315" y2="-2.13" width="0.1524" layer="21"/>
<wire x1="-2.315" y1="-2.13" x2="-2.315" y2="-1.085" width="0.1524" layer="21"/>
<smd name="1" x="-1.27" y="-3.5" dx="1.78" dy="1.55" layer="1"/>
<smd name="2" x="1.27" y="-3.5" dx="1.78" dy="1.55" layer="1"/>
<smd name="3" x="1.27" y="3.5" dx="1.78" dy="1.55" layer="1" rot="R180"/>
<smd name="4" x="-1.27" y="3.5" dx="1.78" dy="1.55" layer="1" rot="R180"/>
<text x="-2.54" y="-3.175" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.81" y="-3.175" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-3.925" x2="-0.889" y2="-3.2" layer="51"/>
<rectangle x1="0.889" y1="-3.925" x2="1.651" y2="-3.2" layer="51"/>
<rectangle x1="0.889" y1="3.2" x2="1.651" y2="3.925" layer="51" rot="R180"/>
<rectangle x1="-1.651" y1="3.2" x2="-0.889" y2="3.925" layer="51" rot="R180"/>
</package>
<package name="DIL04">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="2.54" y1="2.921" x2="-2.54" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="2.54" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-1.27" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="2" x="1.27" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="3" x="1.27" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<pad name="4" x="-1.27" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<text x="4.191" y="-2.921" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.921" y="-2.667" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="OK">
<wire x1="-2.413" y1="-1.143" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="-1.905" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-0.127" x2="-1.397" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="-0.635" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="1.397" x2="-2.032" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.016" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.143" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-5.715" y2="1.27" width="0.254" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-4.445" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.016" x2="2.286" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-2.286" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<text x="-6.985" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.985" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.381" y1="-2.54" x2="0.381" y2="2.54" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="EMIT" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="COL" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LTV816" prefix="OK">
<description>&lt;b&gt;OPTO COUPLER&lt;/b&gt;&lt;p&gt;
Source: LITEON, LTV816.pdf</description>
<gates>
<gate name="G$1" symbol="OK" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="SMD4">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="COL" pad="4"/>
<connect gate="G$1" pin="EMIT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="DIL04">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="COL" pad="4"/>
<connect gate="G$1" pin="EMIT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-phoenix-3.81">
<description>&lt;b&gt;Phoenix Connectors&lt;/b&gt; Grid 3.81 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1727036">
<description>&lt;b&gt;MKDS 1/ 4-3,81&lt;/b&gt;&lt;p&gt;
Source: http://eshop.phoenixcontact.de</description>
<wire x1="-7.5184" y1="-3.5484" x2="7.5184" y2="-3.5484" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="1.4367" x2="-7.5184" y2="-1.6163" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="1.9123" x2="-7.5184" y2="1.4367" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="-1.6163" x2="7.5184" y2="-1.6163" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="1.4367" x2="-7.5184" y2="1.4367" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="-3.35" x2="7.5184" y2="-3.35" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-0.6" x2="5.715" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="5.715" y1="-0.595" x2="5.71" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="6.921" y1="0.611" x2="6.265" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="5.715" y1="0.395" x2="5.8115" y2="0.4915" width="0.2032" layer="51"/>
<wire x1="5.715" y1="0.5" x2="5.715" y2="0.395" width="0.2032" layer="51"/>
<wire x1="6.265" y1="-0.045" x2="5.715" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="5.1735" y1="-0.1465" x2="5.715" y2="0.395" width="0.2032" layer="51"/>
<wire x1="3.111" y1="0.611" x2="2.455" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="5.71" y1="-0.6" x2="5.004" y2="-1.306" width="0.2032" layer="51"/>
<wire x1="1.905" y1="0.5" x2="1.905" y2="0.395" width="0.2032" layer="51"/>
<wire x1="1.905" y1="0.395" x2="2.0015" y2="0.4915" width="0.2032" layer="51"/>
<wire x1="2.455" y1="-0.045" x2="1.905" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-0.595" x2="1.905" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="4.509" y1="-0.811" x2="5.1735" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-0.595" x2="1.9" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.3635" y1="-0.1465" x2="1.905" y2="0.395" width="0.2032" layer="51"/>
<wire x1="2.0015" y1="0.4915" x2="2.616" y2="1.106" width="0.2032" layer="51"/>
<wire x1="5.8115" y1="0.4915" x2="6.426" y2="1.106" width="0.2032" layer="51"/>
<wire x1="0.699" y1="-0.811" x2="1.3635" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="1.9" y1="-0.6" x2="1.194" y2="-1.306" width="0.2032" layer="51"/>
<wire x1="-7.5184" y1="-3.35" x2="-7.5184" y2="-3.5484" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="-2.911" x2="-7.5184" y2="-3.35" width="0.2032" layer="21"/>
<wire x1="-1.91" y1="-0.6" x2="-2.616" y2="-1.306" width="0.2032" layer="51"/>
<wire x1="-7.5184" y1="-1.6163" x2="-7.5184" y2="-2.0993" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="1.9123" x2="-6.0171" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="-0.699" y1="0.611" x2="-1.355" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="0.395" x2="-1.8085" y2="0.4915" width="0.2032" layer="51"/>
<wire x1="-1.355" y1="-0.045" x2="-1.905" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="-6.921" y1="-0.811" x2="-6.2565" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="-0.6" x2="-5.715" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="0.395" x2="-5.715" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-6.2565" y1="-0.1465" x2="-5.715" y2="0.395" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="-0.595" x2="-5.72" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-5.165" y1="-0.045" x2="-5.715" y2="-0.595" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="0.395" x2="-5.6185" y2="0.4915" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-0.595" x2="-1.905" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="0.5" x2="-1.905" y2="0.395" width="0.2032" layer="51"/>
<wire x1="-2.4465" y1="-0.1465" x2="-1.905" y2="0.395" width="0.2032" layer="51"/>
<wire x1="-4.509" y1="0.611" x2="-5.165" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="-5.6185" y1="0.4915" x2="-5.004" y2="1.106" width="0.2032" layer="51"/>
<wire x1="-5.4129" y1="1.9123" x2="-2.2071" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="-3.111" y1="-0.811" x2="-2.4465" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-0.595" x2="-1.91" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-1.8085" y1="0.4915" x2="-1.194" y2="1.106" width="0.2032" layer="51"/>
<wire x1="-5.72" y1="-0.6" x2="-6.426" y2="-1.306" width="0.2032" layer="51"/>
<wire x1="-1.6029" y1="1.9123" x2="1.6029" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="-2.0993" x2="-7.5184" y2="-2.911" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="-2.911" x2="7.5184" y2="-2.911" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-2.0993" x2="-7.5184" y2="-2.0993" width="0.2032" layer="21"/>
<wire x1="2.2071" y1="1.9123" x2="5.4129" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="6.0171" y1="1.9123" x2="7.5184" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-3.35" x2="7.5184" y2="-3.5484" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-2.911" x2="7.5184" y2="-3.35" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-1.6163" x2="7.5184" y2="-2.0993" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-2.0993" x2="7.5184" y2="-2.911" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="-1.6163" x2="7.5184" y2="1.4367" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="1.9123" x2="7.5184" y2="1.4367" width="0.2032" layer="21"/>
<wire x1="7.5184" y1="1.9123" x2="7.5184" y2="3.5484" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="3.5484" x2="7.5184" y2="3.5484" width="0.2032" layer="21"/>
<wire x1="-7.5184" y1="3.5484" x2="-7.5184" y2="1.9123" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-0.6" x2="6.265" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="5.715" y1="0.5" x2="5.1735" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-0.6" x2="2.455" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="1.905" y1="0.5" x2="1.3635" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="-0.6" x2="-5.165" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="0.5" x2="-6.2565" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="0.5" x2="-2.4465" y2="-0.1465" width="0.2032" layer="51"/>
<wire x1="-1.905" y1="-0.6" x2="-1.355" y2="-0.045" width="0.2032" layer="51"/>
<wire x1="1.6026" y1="1.8953" x2="1.6053" y2="1.9378" width="0.2032" layer="21" curve="-7.7346"/>
<wire x1="1.6053" y1="1.9378" x2="1.612" y2="1.9722" width="0.2032" layer="21" curve="-6.253828"/>
<wire x1="1.6119" y1="1.9722" x2="1.6158" y2="1.9859" width="0.2032" layer="21"/>
<wire x1="1.6158" y1="1.9859" x2="1.6342" y2="2.0322" width="0.2032" layer="21" curve="-9.439343"/>
<wire x1="1.6342" y1="2.0322" x2="1.6462" y2="2.0541" width="0.2032" layer="21"/>
<wire x1="1.6462" y1="2.0541" x2="1.6752" y2="2.0942" width="0.2032" layer="21" curve="-9.372181"/>
<wire x1="1.6752" y1="2.0942" x2="1.6921" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="1.6921" y1="2.1124" x2="1.7297" y2="2.144" width="0.2032" layer="21" curve="-9.30957"/>
<wire x1="1.7297" y1="2.144" x2="1.7503" y2="2.1574" width="0.2032" layer="21"/>
<wire x1="1.7503" y1="2.1574" x2="1.7792" y2="2.1726" width="0.2032" layer="21" curve="-6.581881"/>
<wire x1="1.7792" y1="2.1726" x2="1.8171" y2="2.1869" width="0.2032" layer="21" curve="-8.019102"/>
<wire x1="1.8171" y1="2.187" x2="1.846" y2="2.1942" width="0.2032" layer="21"/>
<wire x1="1.846" y1="2.1942" x2="1.905" y2="2.2" width="0.2032" layer="21" curve="-11.23206"/>
<wire x1="-2.2074" y1="1.8953" x2="-2.2047" y2="1.9378" width="0.2032" layer="21" curve="-7.7346"/>
<wire x1="-2.2047" y1="1.9378" x2="-2.198" y2="1.9722" width="0.2032" layer="21" curve="-6.255811"/>
<wire x1="-2.1981" y1="1.9722" x2="-2.1942" y2="1.9859" width="0.2032" layer="21"/>
<wire x1="-2.1942" y1="1.9859" x2="-2.1713" y2="2.0409" width="0.2032" layer="21" curve="-11.272551"/>
<wire x1="-2.1713" y1="2.0409" x2="-2.1381" y2="2.0903" width="0.2032" layer="21" curve="-11.289508"/>
<wire x1="-2.1381" y1="2.0903" x2="-2.1179" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="-2.1179" y1="2.1124" x2="-2.0871" y2="2.1391" width="0.2032" layer="21" curve="-7.368918"/>
<wire x1="-2.0871" y1="2.1391" x2="-2.0597" y2="2.1575" width="0.2032" layer="21" curve="-5.850578"/>
<wire x1="-2.0597" y1="2.1574" x2="-2.0464" y2="2.1649" width="0.2032" layer="21"/>
<wire x1="-2.0464" y1="2.1649" x2="-2.0129" y2="2.1801" width="0.2032" layer="21" curve="-7.335576"/>
<wire x1="-2.0129" y1="2.1801" x2="-1.9929" y2="2.187" width="0.2032" layer="21"/>
<wire x1="-1.9929" y1="2.187" x2="-1.9527" y2="2.1963" width="0.2032" layer="21" curve="-7.493099"/>
<wire x1="-1.9527" y1="2.1962" x2="-1.9189" y2="2.1997" width="0.2032" layer="21" curve="-6.071817"/>
<wire x1="-1.9189" y1="2.1997" x2="-1.905" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-6.0174" y1="1.8953" x2="-6.0161" y2="1.926" width="0.2032" layer="21" curve="-6.259059"/>
<wire x1="-6.0161" y1="1.926" x2="-6.01" y2="1.9644" width="0.2032" layer="21" curve="-7.728482"/>
<wire x1="-6.0099" y1="1.9644" x2="-6.0042" y2="1.9859" width="0.2032" layer="21"/>
<wire x1="-6.0042" y1="1.9859" x2="-5.9813" y2="2.0409" width="0.2032" layer="21" curve="-11.272551"/>
<wire x1="-5.9813" y1="2.0409" x2="-5.9481" y2="2.0903" width="0.2032" layer="21" curve="-11.289508"/>
<wire x1="-5.9481" y1="2.0903" x2="-5.9279" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="-5.9279" y1="2.1124" x2="-5.8903" y2="2.144" width="0.2032" layer="21" curve="-9.30957"/>
<wire x1="-5.8903" y1="2.144" x2="-5.8697" y2="2.1574" width="0.2032" layer="21"/>
<wire x1="-5.8697" y1="2.1574" x2="-5.8259" y2="2.1789" width="0.2032" layer="21" curve="-9.241969"/>
<wire x1="-5.8259" y1="2.1789" x2="-5.8029" y2="2.187" width="0.2032" layer="21"/>
<wire x1="-5.8029" y1="2.187" x2="-5.7627" y2="2.1963" width="0.2032" layer="21" curve="-7.493099"/>
<wire x1="-5.7627" y1="2.1962" x2="-5.7289" y2="2.1997" width="0.2032" layer="21" curve="-6.071817"/>
<wire x1="-5.7289" y1="2.1997" x2="-5.715" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-5.4126" y1="1.8953" x2="-5.4173" y2="1.8721" width="0.2032" layer="21" curve="-20.191506"/>
<wire x1="-5.4174" y1="1.8721" x2="-5.4203" y2="1.865" width="0.2032" layer="21"/>
<wire x1="-5.4203" y1="1.865" x2="-5.4312" y2="1.845" width="0.2032" layer="21" curve="-9.375625"/>
<wire x1="-5.4311" y1="1.845" x2="-5.4394" y2="1.833" width="0.2032" layer="21"/>
<wire x1="-5.4394" y1="1.833" x2="-5.449" y2="1.8208" width="0.2032" layer="21"/>
<wire x1="-5.449" y1="1.8208" x2="-5.4724" y2="1.7961" width="0.2032" layer="21" curve="-7.330264"/>
<wire x1="-5.4724" y1="1.7961" x2="-5.4945" y2="1.777" width="0.2032" layer="21"/>
<wire x1="-5.4945" y1="1.777" x2="-5.5197" y2="1.7588" width="0.2032" layer="21"/>
<wire x1="-5.5197" y1="1.7588" x2="-5.5605" y2="1.7355" width="0.2032" layer="21" curve="-6.850176"/>
<wire x1="-5.5605" y1="1.7355" x2="-5.5772" y2="1.7278" width="0.2032" layer="21"/>
<wire x1="-5.5772" y1="1.7278" x2="-5.644" y2="1.7067" width="0.2032" layer="21" curve="-11.318509"/>
<wire x1="-5.644" y1="1.7067" x2="-5.715" y2="1.6993" width="0.2032" layer="21" curve="-11.935451"/>
<wire x1="-5.715" y1="1.6993" x2="-5.7507" y2="1.7011" width="0.2032" layer="21"/>
<wire x1="-5.7507" y1="1.7011" x2="-5.786" y2="1.7067" width="0.2032" layer="21"/>
<wire x1="-5.786" y1="1.7067" x2="-5.8528" y2="1.7278" width="0.2032" layer="21" curve="-11.141162"/>
<wire x1="-5.8528" y1="1.7278" x2="-5.8748" y2="1.7382" width="0.2032" layer="21"/>
<wire x1="-5.8748" y1="1.7382" x2="-5.9102" y2="1.7588" width="0.2032" layer="21" curve="-5.972395"/>
<wire x1="-5.9102" y1="1.7588" x2="-5.9295" y2="1.7724" width="0.2032" layer="21"/>
<wire x1="-5.9295" y1="1.7724" x2="-5.9576" y2="1.7961" width="0.2032" layer="21" curve="-5.546471"/>
<wire x1="-5.9576" y1="1.7961" x2="-5.9709" y2="1.8095" width="0.2032" layer="21"/>
<wire x1="-5.9709" y1="1.8095" x2="-5.981" y2="1.8208" width="0.2032" layer="21"/>
<wire x1="-5.981" y1="1.8208" x2="-5.999" y2="1.8451" width="0.2032" layer="21" curve="-6.625705"/>
<wire x1="-5.999" y1="1.8451" x2="-6.006" y2="1.8573" width="0.2032" layer="21"/>
<wire x1="-6.006" y1="1.8573" x2="-6.0126" y2="1.8721" width="0.2032" layer="21"/>
<wire x1="-6.0126" y1="1.8721" x2="-6.0173" y2="1.8953" width="0.2032" layer="21" curve="-16.113355"/>
<wire x1="-5.715" y1="2.2" x2="-5.6739" y2="2.1972" width="0.2032" layer="21" curve="-7.486486"/>
<wire x1="-5.6739" y1="2.1972" x2="-5.6406" y2="2.1907" width="0.2032" layer="21" curve="-6.059992"/>
<wire x1="-5.6406" y1="2.1907" x2="-5.6271" y2="2.187" width="0.2032" layer="21"/>
<wire x1="-5.6271" y1="2.187" x2="-5.5964" y2="2.1758" width="0.2032" layer="21" curve="-6.588084"/>
<wire x1="-5.5964" y1="2.1758" x2="-5.5603" y2="2.1575" width="0.2032" layer="21" curve="-8.014763"/>
<wire x1="-5.5603" y1="2.1574" x2="-5.5397" y2="2.144" width="0.2032" layer="21"/>
<wire x1="-5.5397" y1="2.144" x2="-5.5021" y2="2.1124" width="0.2032" layer="21" curve="-9.313585"/>
<wire x1="-5.5021" y1="2.1124" x2="-5.4914" y2="2.1012" width="0.2032" layer="21"/>
<wire x1="-5.4914" y1="2.1012" x2="-5.468" y2="2.0721" width="0.2032" layer="21" curve="-7.456129"/>
<wire x1="-5.468" y1="2.0721" x2="-5.4562" y2="2.0541" width="0.2032" layer="21"/>
<wire x1="-5.4562" y1="2.0541" x2="-5.4372" y2="2.0172" width="0.2032" layer="21" curve="-7.505121"/>
<wire x1="-5.4372" y1="2.0172" x2="-5.4257" y2="1.9859" width="0.2032" layer="21" curve="-5.902723"/>
<wire x1="-5.4258" y1="1.9859" x2="-5.4219" y2="1.9722" width="0.2032" layer="21"/>
<wire x1="-5.4219" y1="1.9722" x2="-5.4152" y2="1.9378" width="0.2032" layer="21" curve="-7.02911"/>
<wire x1="-5.4153" y1="1.9378" x2="-5.4126" y2="1.8953" width="0.2032" layer="21" curve="-8.415066"/>
<wire x1="-1.6026" y1="1.8953" x2="-1.6112" y2="1.8628" width="0.2032" layer="21" curve="-25.573943"/>
<wire x1="-1.6112" y1="1.8628" x2="-1.6211" y2="1.845" width="0.2032" layer="21"/>
<wire x1="-1.6211" y1="1.845" x2="-1.639" y2="1.8208" width="0.2032" layer="21" curve="-8.620003"/>
<wire x1="-1.639" y1="1.8208" x2="-1.6501" y2="1.8084" width="0.2032" layer="21"/>
<wire x1="-1.6501" y1="1.8084" x2="-1.6624" y2="1.7961" width="0.2032" layer="21"/>
<wire x1="-1.6624" y1="1.7961" x2="-1.7091" y2="1.7592" width="0.2032" layer="21" curve="-10.050268"/>
<wire x1="-1.7091" y1="1.7592" x2="-1.7672" y2="1.7278" width="0.2032" layer="21" curve="-10.541408"/>
<wire x1="-1.7672" y1="1.7278" x2="-1.8" y2="1.7156" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.7156" x2="-1.8691" y2="1.7011" width="0.2032" layer="21" curve="-11.63017"/>
<wire x1="-1.8691" y1="1.7012" x2="-1.905" y2="1.6993" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.6993" x2="-1.955" y2="1.703" width="0.2032" layer="21" curve="-7.959317"/>
<wire x1="-1.955" y1="1.7029" x2="-1.976" y2="1.7067" width="0.2032" layer="21"/>
<wire x1="-1.976" y1="1.7067" x2="-2.0428" y2="1.7278" width="0.2032" layer="21" curve="-11.141162"/>
<wire x1="-2.0428" y1="1.7278" x2="-2.0732" y2="1.7425" width="0.2032" layer="21"/>
<wire x1="-2.0732" y1="1.7425" x2="-2.1258" y2="1.7772" width="0.2032" layer="21" curve="-9.672181"/>
<wire x1="-2.1258" y1="1.7772" x2="-2.1476" y2="1.7961" width="0.2032" layer="21"/>
<wire x1="-2.1476" y1="1.7961" x2="-2.171" y2="1.8208" width="0.2032" layer="21" curve="-7.127485"/>
<wire x1="-2.171" y1="1.8208" x2="-2.1816" y2="1.8343" width="0.2032" layer="21"/>
<wire x1="-2.1816" y1="1.8343" x2="-2.189" y2="1.8451" width="0.2032" layer="21"/>
<wire x1="-2.189" y1="1.8451" x2="-2.2051" y2="1.8798" width="0.2032" layer="21" curve="-15.974667"/>
<wire x1="-2.2051" y1="1.8798" x2="-2.2074" y2="1.8953" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.2" x2="-1.8751" y2="2.1985" width="0.2032" layer="21" curve="-6.089773"/>
<wire x1="-1.8751" y1="2.1985" x2="-1.8379" y2="2.1925" width="0.2032" layer="21" curve="-7.481104"/>
<wire x1="-1.8379" y1="2.1925" x2="-1.8171" y2="2.187" width="0.2032" layer="21"/>
<wire x1="-1.8171" y1="2.187" x2="-1.7628" y2="2.1645" width="0.2032" layer="21" curve="-11.106224"/>
<wire x1="-1.7628" y1="2.1645" x2="-1.714" y2="2.1321" width="0.2032" layer="21" curve="-11.104686"/>
<wire x1="-1.714" y1="2.1321" x2="-1.6921" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="-1.6921" y1="2.1124" x2="-1.6599" y2="2.0747" width="0.2032" layer="21" curve="-9.399061"/>
<wire x1="-1.6599" y1="2.0747" x2="-1.6462" y2="2.0541" width="0.2032" layer="21"/>
<wire x1="-1.6462" y1="2.0541" x2="-1.6306" y2="2.0247" width="0.2032" layer="21" curve="-6.730234"/>
<wire x1="-1.6306" y1="2.0247" x2="-1.6158" y2="1.9859" width="0.2032" layer="21" curve="-8.234687"/>
<wire x1="-1.6158" y1="1.9859" x2="-1.6101" y2="1.9644" width="0.2032" layer="21"/>
<wire x1="-1.6101" y1="1.9644" x2="-1.604" y2="1.926" width="0.2032" layer="21" curve="-7.010408"/>
<wire x1="-1.6039" y1="1.926" x2="-1.6026" y2="1.8953" width="0.2032" layer="21" curve="-5.405737"/>
<wire x1="1.905" y1="2.2" x2="1.9349" y2="2.1985" width="0.2032" layer="21" curve="-6.089773"/>
<wire x1="1.9349" y1="2.1985" x2="1.9721" y2="2.1924" width="0.2032" layer="21" curve="-7.474554"/>
<wire x1="1.9721" y1="2.1925" x2="1.9929" y2="2.187" width="0.2032" layer="21"/>
<wire x1="1.9929" y1="2.187" x2="2.0383" y2="2.1691" width="0.2032" layer="21" curve="-9.235346"/>
<wire x1="2.0383" y1="2.1691" x2="2.0597" y2="2.1574" width="0.2032" layer="21"/>
<wire x1="2.0597" y1="2.1574" x2="2.087" y2="2.139" width="0.2032" layer="21" curve="-6.640135"/>
<wire x1="2.0871" y1="2.1391" x2="2.1179" y2="2.1124" width="0.2032" layer="21" curve="-8.064982"/>
<wire x1="2.1179" y1="2.1124" x2="2.1348" y2="2.0942" width="0.2032" layer="21"/>
<wire x1="2.1348" y1="2.0942" x2="2.1638" y2="2.0541" width="0.2032" layer="21" curve="-9.379816"/>
<wire x1="2.1638" y1="2.0541" x2="2.1758" y2="2.0322" width="0.2032" layer="21"/>
<wire x1="2.1758" y1="2.0322" x2="2.1942" y2="1.9859" width="0.2032" layer="21" curve="-9.449407"/>
<wire x1="2.1942" y1="1.9859" x2="2.1999" y2="1.9644" width="0.2032" layer="21"/>
<wire x1="2.1999" y1="1.9644" x2="2.206" y2="1.926" width="0.2032" layer="21" curve="-7.01264"/>
<wire x1="2.2061" y1="1.926" x2="2.2074" y2="1.8953" width="0.2032" layer="21" curve="-5.407398"/>
<wire x1="6.0174" y1="1.8953" x2="6.0088" y2="1.8628" width="0.2032" layer="21" curve="-25.573943"/>
<wire x1="6.0088" y1="1.8628" x2="5.9989" y2="1.845" width="0.2032" layer="21"/>
<wire x1="5.9989" y1="1.845" x2="5.9737" y2="1.8125" width="0.2032" layer="21" curve="-10.232293"/>
<wire x1="5.9737" y1="1.8125" x2="5.9576" y2="1.7961" width="0.2032" layer="21"/>
<wire x1="5.9576" y1="1.7961" x2="5.9295" y2="1.7724" width="0.2032" layer="21" curve="-6.801102"/>
<wire x1="5.9295" y1="1.7724" x2="5.9103" y2="1.7588" width="0.2032" layer="21"/>
<wire x1="5.9103" y1="1.7588" x2="5.8695" y2="1.7355" width="0.2032" layer="21" curve="-6.850176"/>
<wire x1="5.8695" y1="1.7355" x2="5.8528" y2="1.7278" width="0.2032" layer="21"/>
<wire x1="5.8528" y1="1.7278" x2="5.786" y2="1.7067" width="0.2032" layer="21" curve="-11.328526"/>
<wire x1="5.786" y1="1.7067" x2="5.715" y2="1.6993" width="0.2032" layer="21" curve="-11.935451"/>
<wire x1="5.715" y1="1.6993" x2="5.6937" y2="1.7" width="0.2032" layer="21"/>
<wire x1="5.6937" y1="1.7" x2="5.644" y2="1.7067" width="0.2032" layer="21" curve="-8.711456"/>
<wire x1="5.644" y1="1.7067" x2="5.6097" y2="1.7157" width="0.2032" layer="21"/>
<wire x1="5.6097" y1="1.7157" x2="5.5772" y2="1.7278" width="0.2032" layer="21"/>
<wire x1="5.5772" y1="1.7278" x2="5.5191" y2="1.7592" width="0.2032" layer="21" curve="-10.09507"/>
<wire x1="5.5191" y1="1.7592" x2="5.4724" y2="1.7961" width="0.2032" layer="21" curve="-9.445076"/>
<wire x1="5.4724" y1="1.7961" x2="5.4601" y2="1.8084" width="0.2032" layer="21"/>
<wire x1="5.4601" y1="1.8084" x2="5.449" y2="1.8208" width="0.2032" layer="21"/>
<wire x1="5.449" y1="1.8208" x2="5.431" y2="1.8451" width="0.2032" layer="21" curve="-7.352598"/>
<wire x1="5.431" y1="1.8451" x2="5.4247" y2="1.856" width="0.2032" layer="21"/>
<wire x1="5.4247" y1="1.856" x2="5.4174" y2="1.8721" width="0.2032" layer="21" curve="-6.987625"/>
<wire x1="5.4174" y1="1.8721" x2="5.4139" y2="1.884" width="0.2032" layer="21"/>
<wire x1="5.4139" y1="1.884" x2="5.4126" y2="1.8953" width="0.2032" layer="21"/>
<wire x1="2.2074" y1="1.8953" x2="2.2027" y2="1.8721" width="0.2032" layer="21" curve="-20.191506"/>
<wire x1="2.2026" y1="1.8721" x2="2.1968" y2="1.8588" width="0.2032" layer="21"/>
<wire x1="2.1968" y1="1.8588" x2="2.1889" y2="1.845" width="0.2032" layer="21"/>
<wire x1="2.1889" y1="1.845" x2="2.171" y2="1.8208" width="0.2032" layer="21" curve="-6.908842"/>
<wire x1="2.171" y1="1.8208" x2="2.1599" y2="1.8084" width="0.2032" layer="21"/>
<wire x1="2.1599" y1="1.8084" x2="2.1476" y2="1.7961" width="0.2032" layer="21"/>
<wire x1="2.1476" y1="1.7961" x2="2.1009" y2="1.7592" width="0.2032" layer="21" curve="-10.050268"/>
<wire x1="2.1009" y1="1.7592" x2="2.0428" y2="1.7278" width="0.2032" layer="21" curve="-10.54807"/>
<wire x1="2.0428" y1="1.7278" x2="2.01" y2="1.7156" width="0.2032" layer="21"/>
<wire x1="2.01" y1="1.7156" x2="1.9409" y2="1.7011" width="0.2032" layer="21" curve="-11.63017"/>
<wire x1="1.9409" y1="1.7012" x2="1.905" y2="1.6993" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.6993" x2="1.8599" y2="1.7023" width="0.2032" layer="21" curve="-7.94548"/>
<wire x1="1.8599" y1="1.7023" x2="1.834" y2="1.7067" width="0.2032" layer="21"/>
<wire x1="1.834" y1="1.7067" x2="1.7672" y2="1.7278" width="0.2032" layer="21" curve="-11.141162"/>
<wire x1="1.7672" y1="1.7278" x2="1.7452" y2="1.7382" width="0.2032" layer="21"/>
<wire x1="1.7452" y1="1.7382" x2="1.7098" y2="1.7588" width="0.2032" layer="21" curve="-5.972395"/>
<wire x1="1.7098" y1="1.7588" x2="1.6963" y2="1.7681" width="0.2032" layer="21"/>
<wire x1="1.6963" y1="1.7681" x2="1.6624" y2="1.7961" width="0.2032" layer="21" curve="-7.835551"/>
<wire x1="1.6624" y1="1.7961" x2="1.6462" y2="1.8125" width="0.2032" layer="21"/>
<wire x1="1.6462" y1="1.8125" x2="1.621" y2="1.8451" width="0.2032" layer="21" curve="-9.426541"/>
<wire x1="1.621" y1="1.8451" x2="1.6112" y2="1.8629" width="0.2032" layer="21"/>
<wire x1="1.6112" y1="1.8629" x2="1.6026" y2="1.8953" width="0.2032" layer="21" curve="-22.192685"/>
<wire x1="5.4126" y1="1.8953" x2="5.4153" y2="1.9378" width="0.2032" layer="21" curve="-7.7346"/>
<wire x1="5.4153" y1="1.9378" x2="5.422" y2="1.9722" width="0.2032" layer="21" curve="-6.253828"/>
<wire x1="5.4219" y1="1.9722" x2="5.4258" y2="1.9859" width="0.2032" layer="21"/>
<wire x1="5.4258" y1="1.9859" x2="5.4406" y2="2.0247" width="0.2032" layer="21" curve="-7.514856"/>
<wire x1="5.4406" y1="2.0247" x2="5.4562" y2="2.0541" width="0.2032" layer="21" curve="-5.894955"/>
<wire x1="5.4562" y1="2.0541" x2="5.468" y2="2.0721" width="0.2032" layer="21"/>
<wire x1="5.468" y1="2.0721" x2="5.4914" y2="2.1012" width="0.2032" layer="21" curve="-6.696457"/>
<wire x1="5.4914" y1="2.1012" x2="5.5021" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="5.5021" y1="2.1124" x2="5.5397" y2="2.144" width="0.2032" layer="21" curve="-9.30957"/>
<wire x1="5.5397" y1="2.144" x2="5.5603" y2="2.1574" width="0.2032" layer="21"/>
<wire x1="5.5603" y1="2.1574" x2="5.5964" y2="2.1757" width="0.2032" layer="21" curve="-7.328568"/>
<wire x1="5.5964" y1="2.1758" x2="5.6271" y2="2.187" width="0.2032" layer="21" curve="-5.786911"/>
<wire x1="5.6271" y1="2.187" x2="5.656" y2="2.1942" width="0.2032" layer="21"/>
<wire x1="5.656" y1="2.1942" x2="5.715" y2="2.2" width="0.2032" layer="21" curve="-11.23206"/>
<wire x1="5.715" y1="2.2" x2="5.774" y2="2.1942" width="0.2032" layer="21" curve="-11.23206"/>
<wire x1="5.774" y1="2.1942" x2="5.8029" y2="2.187" width="0.2032" layer="21"/>
<wire x1="5.8029" y1="2.187" x2="5.8336" y2="2.1758" width="0.2032" layer="21" curve="-6.588084"/>
<wire x1="5.8336" y1="2.1758" x2="5.8697" y2="2.1575" width="0.2032" layer="21" curve="-8.014763"/>
<wire x1="5.8697" y1="2.1574" x2="5.8828" y2="2.1492" width="0.2032" layer="21"/>
<wire x1="5.8828" y1="2.1492" x2="5.9122" y2="2.1269" width="0.2032" layer="21" curve="-7.363435"/>
<wire x1="5.9122" y1="2.1268" x2="5.9279" y2="2.1124" width="0.2032" layer="21"/>
<wire x1="5.9279" y1="2.1124" x2="5.9551" y2="2.0815" width="0.2032" layer="21" curve="-7.443994"/>
<wire x1="5.9551" y1="2.0815" x2="5.9738" y2="2.0541" width="0.2032" layer="21" curve="-5.873542"/>
<wire x1="5.9738" y1="2.0541" x2="5.9815" y2="2.0406" width="0.2032" layer="21"/>
<wire x1="5.9815" y1="2.0406" x2="5.9972" y2="2.0064" width="0.2032" layer="21" curve="-7.515672"/>
<wire x1="5.9972" y1="2.0064" x2="6.0042" y2="1.9859" width="0.2032" layer="21"/>
<wire x1="6.0042" y1="1.9859" x2="6.0117" y2="1.9561" width="0.2032" layer="21" curve="-6.253781"/>
<wire x1="6.0117" y1="1.9561" x2="6.0167" y2="1.9175" width="0.2032" layer="21" curve="-7.737404"/>
<wire x1="6.0168" y1="1.9175" x2="6.0174" y2="1.8953" width="0.2032" layer="21"/>
<circle x="1.905" y="-0.1" radius="1.45" width="0.2032" layer="21"/>
<circle x="5.715" y="-0.1" radius="1.45" width="0.2032" layer="21"/>
<circle x="-1.905" y="-0.1" radius="1.45" width="0.2032" layer="21"/>
<circle x="-5.715" y="-0.1" radius="1.45" width="0.2032" layer="21"/>
<pad name="1" x="-5.715" y="0" drill="1.1" diameter="1.9304" rot="R90"/>
<pad name="2" x="-1.905" y="0" drill="1.1" diameter="1.9304" rot="R90"/>
<pad name="3" x="1.905" y="0" drill="1.1" diameter="1.9304" rot="R90"/>
<pad name="4" x="5.715" y="0" drill="1.1" diameter="1.9304" rot="R90"/>
<text x="-8.2328" y="-3.8188" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="9.3758" y="-3.8188" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="21">
<vertex x="-5.725" y="-2.25"/>
<vertex x="-5.975" y="-2.675"/>
<vertex x="-5.5" y="-2.675"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="SCHRAUBKLEMMEV">
<wire x1="-1.27" y1="0.635" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-0.635" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.27" x2="-2.286" y2="1.27" width="0.1524" layer="94"/>
<circle x="-0.889" y="0" radius="0.889" width="0.254" layer="94"/>
<text x="-3.175" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SCHRAUBKLEMME">
<wire x1="-1.27" y1="0.635" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-0.635" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.27" x2="-2.286" y2="1.27" width="0.1524" layer="94"/>
<circle x="-0.889" y="0" radius="0.889" width="0.254" layer="94"/>
<text x="-3.175" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1727036" prefix="X">
<description>&lt;b&gt;MKDS 1/4-3,81&lt;/b&gt;&lt;p&gt;
Source: http://eshop.phoenixcontact.de</description>
<gates>
<gate name="-1" symbol="SCHRAUBKLEMMEV" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="SCHRAUBKLEMME" x="0" y="2.54" addlevel="always"/>
<gate name="-3" symbol="SCHRAUBKLEMME" x="0" y="0" addlevel="always"/>
<gate name="-4" symbol="SCHRAUBKLEMME" x="0" y="-2.54" addlevel="always"/>
</gates>
<devices>
<device name="" package="1727036">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="FARNELL" constant="no"/>
<attribute name="MPN" value="1727036" constant="no"/>
<attribute name="OC_FARNELL" value="3704592" constant="no"/>
<attribute name="OC_NEWARK" value="71C4114" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="OWNER" value="ekarlso@creator.no"/>
<attribute name="VERSION" value="1.0"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D101" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R101" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="SUP-3V3101" library="cyviz" deviceset="+3V3" device=""/>
<part name="GND101" library="supply1" deviceset="GND" device=""/>
<part name="D102" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R105" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="D103" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R109" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="Q101" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="Q102" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="GND102" library="supply1" deviceset="GND" device=""/>
<part name="GND109" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3104" library="cyviz" deviceset="+3V3" device=""/>
<part name="SUP-3V3107" library="cyviz" deviceset="+3V3" device=""/>
<part name="R102" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="R108" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="D104" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R111" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="Q103" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="GND110" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3108" library="cyviz" deviceset="+3V3" device=""/>
<part name="R110" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="C103" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="22pF"/>
<part name="C104" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="22pF"/>
<part name="GND117" library="supply1" deviceset="GND" device=""/>
<part name="GND121" library="supply1" deviceset="GND" device=""/>
<part name="C106" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="0.1uF"/>
<part name="R124" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="C107" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="0.1uF"/>
<part name="GND119" library="supply1" deviceset="GND" device=""/>
<part name="GND118" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3112" library="cyviz" deviceset="+3V3" device=""/>
<part name="SUP-3V3113" library="cyviz" deviceset="+3V3" device=""/>
<part name="GND116" library="supply1" deviceset="GND" device=""/>
<part name="SV201" library="con-lstb" deviceset="MA03-2" device="" value="ICSP"/>
<part name="GND215" library="supply1" deviceset="GND" device=""/>
<part name="GND214" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3205" library="cyviz" deviceset="+3V3" device=""/>
<part name="FRAME201" library="creator" deviceset="A4L-LOC" device="">
<attribute name="DESCRIPTION" value="IO, power and mech"/>
</part>
<part name="FRAME101" library="creator" deviceset="A4L-LOC" device="">
<attribute name="DESCRIPTION" value="MCU, LEDs and RS485 ports"/>
</part>
<part name="U201" library="recom-intl" deviceset="R-78XX" device=""/>
<part name="GND204" library="supply1" deviceset="GND" device=""/>
<part name="C201" library="creator" deviceset="SMD-IPC_CAPPOL" device="C3216" value="10uF-35V"/>
<part name="SUP-24V201" library="creator" deviceset="+24V" device=""/>
<part name="U101" library="creator" deviceset="SN75HVD08DR" device=""/>
<part name="GND107" library="supply1" deviceset="GND" device=""/>
<part name="C101" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="0.1uF"/>
<part name="R103" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="R112" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10"/>
<part name="R113" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10"/>
<part name="GND111" library="supply1" deviceset="GND" device=""/>
<part name="R106" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="GND105" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3105" library="cyviz" deviceset="+3V3" device=""/>
<part name="SUP-3V3102" library="cyviz" deviceset="+3V3" device=""/>
<part name="GND103" library="supply1" deviceset="GND" device=""/>
<part name="S101" library="switch-dil" deviceset="DS01" device="" value="RS485-END"/>
<part name="R119" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="120"/>
<part name="U102" library="creator" deviceset="SN75HVD08DR" device=""/>
<part name="GND108" library="supply1" deviceset="GND" device=""/>
<part name="C102" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="0.1uF"/>
<part name="R104" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="R114" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10"/>
<part name="R115" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10"/>
<part name="GND112" library="supply1" deviceset="GND" device=""/>
<part name="R107" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="GND106" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3106" library="cyviz" deviceset="+3V3" device=""/>
<part name="SUP-3V3103" library="cyviz" deviceset="+3V3" device=""/>
<part name="GND104" library="supply1" deviceset="GND" device=""/>
<part name="S102" library="switch-dil" deviceset="DS01" device="" value="RS485-END"/>
<part name="R121" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="120"/>
<part name="K201" library="relay" deviceset="G5SB" device="" value="G5SB-14-24VDC"/>
<part name="Q201" library="creator" deviceset="TRANSISTOR_NPN" device="_SOT23"/>
<part name="GND203" library="supply1" deviceset="GND" device=""/>
<part name="R201" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="D201" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="GND201" library="supply1" deviceset="GND" device=""/>
<part name="J201" library="SparkFun-Connectors" deviceset="M03" device="SCREW" value="DOOR1-CON"/>
<part name="SUP-24V202" library="creator" deviceset="+24V" device=""/>
<part name="R202" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1K"/>
<part name="D106" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R117" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="D108" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R120" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="Q104" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="Q105" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="GND113" library="supply1" deviceset="GND" device=""/>
<part name="GND114" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3109" library="cyviz" deviceset="+3V3" device=""/>
<part name="SUP-3V3110" library="cyviz" deviceset="+3V3" device=""/>
<part name="R116" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="R118" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="D109" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R123" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="Q106" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="GND115" library="supply1" deviceset="GND" device=""/>
<part name="SUP-3V3111" library="cyviz" deviceset="+3V3" device=""/>
<part name="R122" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="D107" library="creator" deviceset="TVSDIODEX2" device="SOT23" value="CDSOT23-SM712"/>
<part name="D105" library="creator" deviceset="TVSDIODEX2" device="SOT23" value="CDSOT23-SM712"/>
<part name="K202" library="relay" deviceset="G5SB" device="" value="G5SB-14-24VDC"/>
<part name="Q203" library="creator" deviceset="TRANSISTOR_NPN" device="_SOT23"/>
<part name="GND207" library="supply1" deviceset="GND" device=""/>
<part name="R205" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="D204" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="GND206" library="supply1" deviceset="GND" device=""/>
<part name="SUP-24V203" library="creator" deviceset="+24V" device=""/>
<part name="R208" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1K"/>
<part name="SUP-24V204" library="creator" deviceset="+24V" device=""/>
<part name="SUP-3V3202" library="creator" deviceset="+3V3" device=""/>
<part name="S201" library="creator" deviceset="BUZZER_PASSIVE" device="_BESTAR_BSP1212" value="BESTAR_BSP1212"/>
<part name="GND205" library="creator" deviceset="GND" device=""/>
<part name="Q202" library="creator" deviceset="TRANSISTOR_PNP" device="_SOT23" value="BC857ASMD"/>
<part name="R203" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="SUP-3V3201" library="creator" deviceset="+3V3" device=""/>
<part name="D203" library="creator" deviceset="BAW56" device="1-2-3" value="NM"/>
<part name="C202" library="creator" deviceset="SMD-IPC_CAPPOL" device="C3216" value="10uF-16V"/>
<part name="GND202" library="creator" deviceset="GND" device=""/>
<part name="R204" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="U103" library="creator" deviceset="ATMEGA328PB-AU" device=""/>
<part name="OK202" library="optocoupler" deviceset="LTV816" device=""/>
<part name="GND209" library="supply1" deviceset="GND" device=""/>
<part name="OK201" library="optocoupler" deviceset="LTV816" device=""/>
<part name="R206" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="SUP-3V3203" library="creator" deviceset="+3V3" device=""/>
<part name="R209" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="GND208" library="supply1" deviceset="GND" device=""/>
<part name="R207" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="1k"/>
<part name="D206" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="SUP-3V3204" library="creator" deviceset="+3V3" device=""/>
<part name="R210" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="220"/>
<part name="D207" library="creator" deviceset="LED_MONOCHROME" device="_AVAGO-HSMX-C150" value="HSMS-C150">
<attribute name="COLOR" value="RED"/>
</part>
<part name="R211" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="NM"/>
<part name="H201" library="creator" deviceset="HOLE_PLET" device="3MM"/>
<part name="H202" library="creator" deviceset="HOLE_PLET" device="3MM"/>
<part name="H203" library="creator" deviceset="HOLE_PLET" device="3MM"/>
<part name="H204" library="creator" deviceset="HOLE_PLET" device="3MM"/>
<part name="X203" library="con-phoenix-3.81" deviceset="1727036" device=""/>
<part name="X202" library="con-phoenix-3.81" deviceset="1727036" device=""/>
<part name="GND210" library="supply1" deviceset="GND" device=""/>
<part name="GND211" library="supply1" deviceset="GND" device=""/>
<part name="GND212" library="supply1" deviceset="GND" device=""/>
<part name="GND213" library="supply1" deviceset="GND" device=""/>
<part name="D205" library="creator" deviceset="BAW56" device="1-2-3" value="BAW56"/>
<part name="D202" library="creator" deviceset="BAW56" device="1-2-3" value="BAW56"/>
<part name="GND216" library="supply1" deviceset="GND" device=""/>
<part name="SUP-24V205" library="creator" deviceset="+24V" device=""/>
<part name="X201" library="con-phoenix-3.81" deviceset="1727036" device=""/>
<part name="C105" library="creator" deviceset="SMD-IPC_CAP" device="C0805" value="10uF"/>
<part name="GND120" library="supply1" deviceset="GND" device=""/>
<part name="FID201" library="creator" deviceset="FIDUCIAL_MARK" device=""/>
<part name="FID202" library="creator" deviceset="FIDUCIAL_MARK" device=""/>
<part name="FID203" library="creator" deviceset="FIDUCIAL_MARK" device=""/>
<part name="FID204" library="creator" deviceset="FIDUCIAL_MARK" device=""/>
<part name="LOGO101" library="creator" deviceset="CREATOR_LOGO" device="20X20"/>
<part name="LOGO201" library="creator" deviceset="CREATOR_LOGO" device="20X20"/>
<part name="F201" library="creator" deviceset="MINISMDC014F" device=""/>
<part name="R1" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R2" library="creator" deviceset="SMD-IPC_RES" device="R0805" value="10K"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="ZQ1" library="creator" deviceset="CSTC?" device="E" technology="8M00G52-R0"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D101" gate="G$1" x="-30.48" y="-73.66" rot="R270"/>
<instance part="R101" gate="G$1" x="-30.48" y="-63.5" rot="R90"/>
<instance part="SUP-3V3101" gate="G$1" x="-30.48" y="-53.34"/>
<instance part="GND101" gate="1" x="-30.48" y="-83.82"/>
<instance part="D102" gate="G$1" x="-12.7" y="-73.66" rot="R270"/>
<instance part="R105" gate="G$1" x="-12.7" y="-63.5" rot="R90"/>
<instance part="D103" gate="G$1" x="5.08" y="-73.66" rot="R270"/>
<instance part="R109" gate="G$1" x="5.08" y="-63.5" rot="R90"/>
<instance part="Q101" gate="Q" x="-15.24" y="-50.8"/>
<instance part="Q102" gate="Q" x="2.54" y="-50.8"/>
<instance part="GND102" gate="1" x="-12.7" y="-83.82"/>
<instance part="GND109" gate="1" x="5.08" y="-83.82"/>
<instance part="SUP-3V3104" gate="G$1" x="-12.7" y="-38.1"/>
<instance part="SUP-3V3107" gate="G$1" x="5.08" y="-38.1"/>
<instance part="R102" gate="G$1" x="-20.32" y="-55.88" rot="R270"/>
<instance part="R108" gate="G$1" x="-2.54" y="-55.88" rot="R270"/>
<instance part="D104" gate="G$1" x="22.86" y="-73.66" rot="R270"/>
<instance part="R111" gate="G$1" x="22.86" y="-63.5" rot="R90"/>
<instance part="Q103" gate="Q" x="20.32" y="-50.8"/>
<instance part="GND110" gate="1" x="22.86" y="-83.82"/>
<instance part="SUP-3V3108" gate="G$1" x="22.86" y="-38.1"/>
<instance part="R110" gate="G$1" x="15.24" y="-55.88" rot="R270"/>
<instance part="C103" gate="G$1" x="129.54" y="17.78" rot="R90"/>
<instance part="C104" gate="G$1" x="149.86" y="17.78" rot="MR90"/>
<instance part="GND117" gate="1" x="142.24" y="2.54"/>
<instance part="GND121" gate="1" x="165.1" y="10.16" rot="R270"/>
<instance part="C106" gate="G$1" x="152.4" y="53.34" rot="R180"/>
<instance part="R124" gate="G$1" x="208.28" y="71.12" rot="R270"/>
<instance part="C107" gate="G$1" x="160.02" y="68.58" rot="R270"/>
<instance part="GND119" gate="1" x="152.4" y="60.96"/>
<instance part="GND118" gate="1" x="147.32" y="50.8"/>
<instance part="SUP-3V3112" gate="G$1" x="165.1" y="78.74" rot="MR0"/>
<instance part="SUP-3V3113" gate="G$1" x="208.28" y="83.82" rot="MR0"/>
<instance part="GND116" gate="1" x="139.7" y="15.24"/>
<instance part="FRAME101" gate="G$1" x="-43.18" y="-91.44"/>
<instance part="U101" gate="A" x="17.78" y="58.42"/>
<instance part="GND107" gate="1" x="-2.54" y="40.64"/>
<instance part="C101" gate="G$1" x="-7.62" y="76.2" rot="R90"/>
<instance part="R103" gate="G$1" x="-17.78" y="71.12" rot="R90"/>
<instance part="R112" gate="G$1" x="45.72" y="76.2" rot="R180"/>
<instance part="R113" gate="G$1" x="45.72" y="53.34" rot="R180"/>
<instance part="GND111" gate="1" x="43.18" y="66.04" rot="R270"/>
<instance part="R106" gate="G$1" x="-10.16" y="43.18" rot="R90"/>
<instance part="GND105" gate="1" x="-7.62" y="66.04"/>
<instance part="SUP-3V3105" gate="G$1" x="-2.54" y="86.36" rot="MR0"/>
<instance part="SUP-3V3102" gate="G$1" x="-17.78" y="81.28"/>
<instance part="GND103" gate="1" x="-10.16" y="33.02"/>
<instance part="S101" gate="G$1" x="73.66" y="58.42" rot="R90"/>
<instance part="R119" gate="G$1" x="71.12" y="68.58"/>
<instance part="U102" gate="A" x="17.78" y="5.08"/>
<instance part="GND108" gate="1" x="-2.54" y="-12.7"/>
<instance part="C102" gate="G$1" x="-7.62" y="22.86" rot="R90"/>
<instance part="R104" gate="G$1" x="-17.78" y="17.78" rot="R90"/>
<instance part="R114" gate="G$1" x="45.72" y="22.86" rot="R180"/>
<instance part="R115" gate="G$1" x="45.72" y="0" rot="R180"/>
<instance part="GND112" gate="1" x="43.18" y="12.7" rot="R270"/>
<instance part="R107" gate="G$1" x="-10.16" y="-10.16" rot="R90"/>
<instance part="GND106" gate="1" x="-7.62" y="12.7"/>
<instance part="SUP-3V3106" gate="G$1" x="-2.54" y="33.02" rot="MR0"/>
<instance part="SUP-3V3103" gate="G$1" x="-17.78" y="27.94"/>
<instance part="GND104" gate="1" x="-10.16" y="-20.32"/>
<instance part="S102" gate="G$1" x="78.74" y="5.08" rot="R90"/>
<instance part="R121" gate="G$1" x="76.2" y="17.78"/>
<instance part="D106" gate="G$1" x="53.34" y="-73.66" rot="R270"/>
<instance part="R117" gate="G$1" x="53.34" y="-63.5" rot="R90"/>
<instance part="D108" gate="G$1" x="71.12" y="-73.66" rot="R270"/>
<instance part="R120" gate="G$1" x="71.12" y="-63.5" rot="R90"/>
<instance part="Q104" gate="Q" x="50.8" y="-50.8"/>
<instance part="Q105" gate="Q" x="68.58" y="-50.8"/>
<instance part="GND113" gate="1" x="53.34" y="-83.82"/>
<instance part="GND114" gate="1" x="71.12" y="-83.82"/>
<instance part="SUP-3V3109" gate="G$1" x="53.34" y="-38.1"/>
<instance part="SUP-3V3110" gate="G$1" x="71.12" y="-38.1"/>
<instance part="R116" gate="G$1" x="45.72" y="-55.88" rot="R270"/>
<instance part="R118" gate="G$1" x="63.5" y="-55.88" rot="R270"/>
<instance part="D109" gate="G$1" x="88.9" y="-73.66" rot="R270"/>
<instance part="R123" gate="G$1" x="88.9" y="-63.5" rot="R90"/>
<instance part="Q106" gate="Q" x="86.36" y="-50.8"/>
<instance part="GND115" gate="1" x="88.9" y="-83.82"/>
<instance part="SUP-3V3111" gate="G$1" x="88.9" y="-38.1"/>
<instance part="R122" gate="G$1" x="81.28" y="-55.88" rot="R270"/>
<instance part="D107" gate="G$1" x="58.42" y="12.7" rot="MR0"/>
<instance part="D105" gate="G$1" x="53.34" y="66.04" rot="MR0"/>
<instance part="U103" gate="A" x="187.96" y="38.1"/>
<instance part="C105" gate="G$1" x="152.4" y="68.58" rot="R270"/>
<instance part="GND120" gate="1" x="160.02" y="60.96"/>
<instance part="LOGO101" gate="G$1" x="218.44" y="-50.8"/>
<instance part="R1" gate="G$1" x="-15.24" y="43.18" rot="R90"/>
<instance part="GND1" gate="1" x="-15.24" y="33.02"/>
<instance part="R2" gate="G$1" x="-15.24" y="-10.16" rot="R90"/>
<instance part="GND2" gate="1" x="-15.24" y="-20.32"/>
<instance part="ZQ1" gate="G$1" x="139.7" y="27.94"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND101" gate="1" pin="GND"/>
<pinref part="D101" gate="G$1" pin="C"/>
<wire x1="-30.48" y1="-81.28" x2="-30.48" y2="-76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D102" gate="G$1" pin="C"/>
<pinref part="GND102" gate="1" pin="GND"/>
<wire x1="-12.7" y1="-76.2" x2="-12.7" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D103" gate="G$1" pin="C"/>
<pinref part="GND109" gate="1" pin="GND"/>
<wire x1="5.08" y1="-76.2" x2="5.08" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D104" gate="G$1" pin="C"/>
<pinref part="GND110" gate="1" pin="GND"/>
<wire x1="22.86" y1="-76.2" x2="22.86" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C103" gate="G$1" pin="1"/>
<pinref part="GND117" gate="1" pin="GND"/>
<wire x1="129.54" y1="15.24" x2="129.54" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="149.86" y1="15.24" x2="149.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="10.16" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="10.16" x2="129.54" y2="10.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="5.08" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
<junction x="142.24" y="10.16"/>
</segment>
<segment>
<wire x1="170.18" y1="10.16" x2="170.18" y2="12.7" width="0.1524" layer="91"/>
<pinref part="GND121" gate="1" pin="GND"/>
<wire x1="170.18" y1="10.16" x2="167.64" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U103" gate="A" pin="GND"/>
<pinref part="U103" gate="A" pin="GND_2"/>
<junction x="170.18" y="10.16"/>
</segment>
<segment>
<pinref part="GND118" gate="1" pin="GND"/>
<pinref part="C106" gate="G$1" pin="2"/>
<wire x1="147.32" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND116" gate="1" pin="GND"/>
<wire x1="139.7" y1="17.78" x2="139.7" y2="20.32" width="0.1524" layer="91"/>
<pinref part="ZQ1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND103" gate="1" pin="GND"/>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="38.1" x2="-10.16" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND107" gate="1" pin="GND"/>
<pinref part="U101" gate="A" pin="GND"/>
<wire x1="-2.54" y1="43.18" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="45.72" x2="0" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND111" gate="1" pin="GND"/>
<wire x1="45.72" y1="66.04" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<pinref part="D105" gate="G$1" pin="CC"/>
</segment>
<segment>
<pinref part="C101" gate="G$1" pin="1"/>
<pinref part="GND105" gate="1" pin="GND"/>
<wire x1="-7.62" y1="68.58" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND104" gate="1" pin="GND"/>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="-17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND108" gate="1" pin="GND"/>
<pinref part="U102" gate="A" pin="GND"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND112" gate="1" pin="GND"/>
<wire x1="45.72" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D107" gate="G$1" pin="CC"/>
</segment>
<segment>
<pinref part="C102" gate="G$1" pin="1"/>
<pinref part="GND106" gate="1" pin="GND"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D106" gate="G$1" pin="C"/>
<pinref part="GND113" gate="1" pin="GND"/>
<wire x1="53.34" y1="-76.2" x2="53.34" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D108" gate="G$1" pin="C"/>
<pinref part="GND114" gate="1" pin="GND"/>
<wire x1="71.12" y1="-76.2" x2="71.12" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D109" gate="G$1" pin="C"/>
<pinref part="GND115" gate="1" pin="GND"/>
<wire x1="88.9" y1="-76.2" x2="88.9" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C105" gate="G$1" pin="2"/>
<pinref part="GND119" gate="1" pin="GND"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C107" gate="G$1" pin="2"/>
<pinref part="GND120" gate="1" pin="GND"/>
<wire x1="160.02" y1="66.04" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="38.1" x2="-15.24" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="-17.78" x2="-15.24" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="SUP-3V3101" gate="G$1" pin="+3V3"/>
<pinref part="R101" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-55.88" x2="-30.48" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q101" gate="Q" pin="E"/>
<pinref part="SUP-3V3104" gate="G$1" pin="+3V3"/>
<wire x1="-12.7" y1="-40.64" x2="-12.7" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q102" gate="Q" pin="E"/>
<pinref part="SUP-3V3107" gate="G$1" pin="+3V3"/>
<wire x1="5.08" y1="-40.64" x2="5.08" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q103" gate="Q" pin="E"/>
<pinref part="SUP-3V3108" gate="G$1" pin="+3V3"/>
<wire x1="22.86" y1="-40.64" x2="22.86" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="170.18" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="165.1" y1="58.42" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
<wire x1="165.1" y1="73.66" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="170.18" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<junction x="165.1" y="60.96"/>
<pinref part="SUP-3V3112" gate="G$1" pin="+3V3"/>
<pinref part="U103" gate="A" pin="AVCC"/>
<pinref part="U103" gate="A" pin="VCC"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="160.02" y1="71.12" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="73.66" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
<junction x="165.1" y="73.66"/>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="160.02" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="71.12" width="0.1524" layer="91"/>
<junction x="160.02" y="73.66"/>
</segment>
<segment>
<pinref part="R124" gate="G$1" pin="1"/>
<wire x1="208.28" y1="76.2" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SUP-3V3113" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="-2.54" y1="83.82" x2="-2.54" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U101" gate="A" pin="VCC"/>
<wire x1="-2.54" y1="81.28" x2="-2.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="66.04" x2="0" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="81.28" x2="-7.62" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="81.28" x2="-7.62" y2="78.74" width="0.1524" layer="91"/>
<junction x="-2.54" y="81.28"/>
<pinref part="C101" gate="G$1" pin="2"/>
<pinref part="SUP-3V3105" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="78.74" x2="-17.78" y2="76.2" width="0.1524" layer="91"/>
<pinref part="SUP-3V3102" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="-2.54" y1="30.48" x2="-2.54" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U102" gate="A" pin="VCC"/>
<wire x1="-2.54" y1="27.94" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="27.94" x2="-7.62" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="27.94" x2="-7.62" y2="25.4" width="0.1524" layer="91"/>
<junction x="-2.54" y="27.94"/>
<pinref part="C102" gate="G$1" pin="2"/>
<pinref part="SUP-3V3106" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R104" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="25.4" x2="-17.78" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SUP-3V3103" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="Q104" gate="Q" pin="E"/>
<pinref part="SUP-3V3109" gate="G$1" pin="+3V3"/>
<wire x1="53.34" y1="-40.64" x2="53.34" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q105" gate="Q" pin="E"/>
<pinref part="SUP-3V3110" gate="G$1" pin="+3V3"/>
<wire x1="71.12" y1="-40.64" x2="71.12" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q106" gate="Q" pin="E"/>
<pinref part="SUP-3V3111" gate="G$1" pin="+3V3"/>
<wire x1="88.9" y1="-40.64" x2="88.9" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="D101" gate="G$1" pin="A"/>
<wire x1="-30.48" y1="-68.58" x2="-30.48" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485-TX" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="-60.96" x2="-2.54" y2="-63.5" width="0.1524" layer="91"/>
<label x="-2.54" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="38.1" x2="210.82" y2="38.1" width="0.1524" layer="91"/>
<label x="210.82" y="38.1" size="1.27" layer="95" xref="yes"/>
<pinref part="U103" gate="A" pin="PD1"/>
</segment>
<segment>
<pinref part="U101" gate="A" pin="D"/>
<wire x1="0" y1="50.8" x2="-22.86" y2="50.8" width="0.1524" layer="91"/>
<label x="-22.86" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RS485-RX" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="-60.96" x2="-20.32" y2="-63.5" width="0.1524" layer="91"/>
<label x="-20.32" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="40.64" x2="210.82" y2="40.64" width="0.1524" layer="91"/>
<label x="210.82" y="40.64" size="1.27" layer="95" xref="yes"/>
<pinref part="U103" gate="A" pin="PD0"/>
</segment>
<segment>
<pinref part="U101" gate="A" pin="R"/>
<wire x1="0" y1="53.34" x2="-17.78" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="66.04" x2="-17.78" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="53.34" x2="-22.86" y2="53.34" width="0.1524" layer="91"/>
<junction x="-17.78" y="53.34"/>
<label x="-22.86" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="1"/>
<pinref part="D102" gate="G$1" pin="A"/>
<wire x1="-12.7" y1="-68.58" x2="-12.7" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R109" gate="G$1" pin="1"/>
<pinref part="D103" gate="G$1" pin="A"/>
<wire x1="5.08" y1="-68.58" x2="5.08" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="Q101" gate="Q" pin="C"/>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="-55.88" x2="-12.7" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q102" gate="Q" pin="C"/>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="5.08" y1="-55.88" x2="5.08" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<pinref part="Q101" gate="Q" pin="B"/>
<wire x1="-20.32" y1="-50.8" x2="-17.78" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<pinref part="Q102" gate="Q" pin="B"/>
<wire x1="-2.54" y1="-50.8" x2="0" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="1"/>
<pinref part="D104" gate="G$1" pin="A"/>
<wire x1="22.86" y1="-68.58" x2="22.86" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Q103" gate="Q" pin="C"/>
<pinref part="R111" gate="G$1" pin="2"/>
<wire x1="22.86" y1="-55.88" x2="22.86" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="1"/>
<pinref part="Q103" gate="Q" pin="B"/>
<wire x1="15.24" y1="-50.8" x2="17.78" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B-" class="0">
<segment>
<pinref part="R113" gate="G$1" pin="1"/>
<label x="83.82" y="53.34" size="1.27" layer="95" xref="yes"/>
<pinref part="S101" gate="G$1" pin="2"/>
<wire x1="63.5" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="66.04" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<wire x1="63.5" y1="58.42" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<junction x="63.5" y="53.34"/>
<pinref part="D105" gate="G$1" pin="A1"/>
<wire x1="60.96" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="63.5" y="58.42"/>
</segment>
</net>
<net name="A+" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="1"/>
<label x="83.82" y="76.2" size="1.27" layer="95" xref="yes"/>
<wire x1="63.5" y1="76.2" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="50.8" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="66.04" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="63.5" y1="68.58" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="63.5" y="76.2"/>
<pinref part="D105" gate="G$1" pin="A2"/>
<wire x1="60.96" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="63.5" y="68.58"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C103" gate="G$1" pin="2"/>
<wire x1="129.54" y1="33.02" x2="129.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="27.94" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="132.08" y1="27.94" x2="129.54" y2="27.94" width="0.1524" layer="91"/>
<junction x="129.54" y="27.94"/>
<pinref part="U103" gate="A" pin="PB6"/>
<wire x1="170.18" y1="33.02" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<pinref part="ZQ1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="149.86" y1="30.48" x2="149.86" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="2"/>
<wire x1="149.86" y1="27.94" x2="149.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="147.32" y1="27.94" x2="149.86" y2="27.94" width="0.1524" layer="91"/>
<junction x="149.86" y="27.94"/>
<pinref part="U103" gate="A" pin="PB7"/>
<wire x1="170.18" y1="30.48" x2="149.86" y2="30.48" width="0.1524" layer="91"/>
<pinref part="ZQ1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<wire x1="205.74" y1="45.72" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<label x="210.82" y="45.72" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="R124" gate="G$1" pin="2"/>
<wire x1="208.28" y1="45.72" x2="210.82" y2="45.72" width="0.1524" layer="91"/>
<wire x1="208.28" y1="66.04" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<junction x="208.28" y="45.72"/>
<pinref part="U103" gate="A" pin="PC6"/>
</segment>
</net>
<net name="A+R1" class="0">
<segment>
<pinref part="U101" gate="A" pin="A"/>
<wire x1="35.56" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
<wire x1="38.1" y1="66.04" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="38.1" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R112" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R119" gate="G$1" pin="2"/>
<wire x1="76.2" y1="68.58" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<wire x1="83.82" y1="68.58" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<pinref part="S101" gate="G$1" pin="1"/>
<wire x1="83.82" y1="58.42" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B-R1" class="0">
<segment>
<pinref part="U101" gate="A" pin="B"/>
<wire x1="35.56" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="38.1" y1="63.5" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="38.1" y1="53.34" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A+R2" class="0">
<segment>
<pinref part="U102" gate="A" pin="A"/>
<wire x1="35.56" y1="12.7" x2="38.1" y2="12.7" width="0.1524" layer="91"/>
<wire x1="38.1" y1="12.7" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R114" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R121" gate="G$1" pin="2"/>
<wire x1="81.28" y1="17.78" x2="88.9" y2="17.78" width="0.1524" layer="91"/>
<wire x1="88.9" y1="17.78" x2="88.9" y2="5.08" width="0.1524" layer="91"/>
<pinref part="S102" gate="G$1" pin="1"/>
<wire x1="88.9" y1="5.08" x2="86.36" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B-R2" class="0">
<segment>
<pinref part="U102" gate="A" pin="B"/>
<wire x1="35.56" y1="10.16" x2="38.1" y2="10.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="10.16" x2="38.1" y2="0" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="38.1" y1="0" x2="40.64" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="READER-A+" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="1"/>
<label x="86.36" y="22.86" size="1.27" layer="95" xref="yes"/>
<wire x1="66.04" y1="22.86" x2="68.58" y2="22.86" width="0.1524" layer="91"/>
<wire x1="68.58" y1="22.86" x2="86.36" y2="22.86" width="0.1524" layer="91"/>
<wire x1="50.8" y1="22.86" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<wire x1="66.04" y1="15.24" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<junction x="66.04" y="22.86"/>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="71.12" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="17.78" x2="68.58" y2="22.86" width="0.1524" layer="91"/>
<junction x="68.58" y="22.86"/>
<pinref part="D107" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="READER-B-" class="0">
<segment>
<wire x1="66.04" y1="10.16" x2="66.04" y2="0" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="66.04" y1="0" x2="50.8" y2="0" width="0.1524" layer="91"/>
<label x="86.36" y="0" size="1.27" layer="95" xref="yes"/>
<wire x1="66.04" y1="0" x2="68.58" y2="0" width="0.1524" layer="91"/>
<junction x="66.04" y="0"/>
<pinref part="S102" gate="G$1" pin="2"/>
<wire x1="68.58" y1="0" x2="86.36" y2="0" width="0.1524" layer="91"/>
<wire x1="71.12" y1="5.08" x2="68.58" y2="5.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="5.08" x2="68.58" y2="0" width="0.1524" layer="91"/>
<junction x="68.58" y="0"/>
<pinref part="D107" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="READER-RS485-RX" class="0">
<segment>
<pinref part="U102" gate="A" pin="R"/>
<wire x1="0" y1="0" x2="-17.78" y2="0" width="0.1524" layer="91"/>
<label x="-20.32" y="0" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="0" x2="-20.32" y2="0" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="12.7" x2="-17.78" y2="0" width="0.1524" layer="91"/>
<junction x="-17.78" y="0"/>
</segment>
<segment>
<pinref part="R116" gate="G$1" pin="2"/>
<wire x1="45.72" y1="-60.96" x2="45.72" y2="-63.5" width="0.1524" layer="91"/>
<label x="45.72" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PB4"/>
<wire x1="167.64" y1="38.1" x2="170.18" y2="38.1" width="0.1524" layer="91"/>
<label x="167.64" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="READER-RS485-TX" class="0">
<segment>
<pinref part="U102" gate="A" pin="D"/>
<wire x1="0" y1="-2.54" x2="-20.32" y2="-2.54" width="0.1524" layer="91"/>
<label x="-20.32" y="-2.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R118" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-60.96" x2="63.5" y2="-63.5" width="0.1524" layer="91"/>
<label x="63.5" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PB3"/>
<wire x1="170.18" y1="40.64" x2="167.64" y2="40.64" width="0.1524" layer="91"/>
<label x="167.64" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="READER-RS485-RXE" class="0">
<segment>
<wire x1="0" y1="7.62" x2="-10.16" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U102" gate="A" pin="~RE"/>
<wire x1="-10.16" y1="7.62" x2="-20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="7.62" width="0.1524" layer="91"/>
<junction x="-10.16" y="7.62"/>
<label x="-20.32" y="7.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PE2"/>
<wire x1="205.74" y1="12.7" x2="208.28" y2="12.7" width="0.1524" layer="91"/>
<label x="208.28" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R117" gate="G$1" pin="1"/>
<pinref part="D106" gate="G$1" pin="A"/>
<wire x1="53.34" y1="-68.58" x2="53.34" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R120" gate="G$1" pin="1"/>
<pinref part="D108" gate="G$1" pin="A"/>
<wire x1="71.12" y1="-68.58" x2="71.12" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="Q104" gate="Q" pin="C"/>
<pinref part="R117" gate="G$1" pin="2"/>
<wire x1="53.34" y1="-55.88" x2="53.34" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="Q105" gate="Q" pin="C"/>
<pinref part="R120" gate="G$1" pin="2"/>
<wire x1="71.12" y1="-55.88" x2="71.12" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R116" gate="G$1" pin="1"/>
<pinref part="Q104" gate="Q" pin="B"/>
<wire x1="45.72" y1="-50.8" x2="48.26" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R118" gate="G$1" pin="1"/>
<pinref part="Q105" gate="Q" pin="B"/>
<wire x1="63.5" y1="-50.8" x2="66.04" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R123" gate="G$1" pin="1"/>
<pinref part="D109" gate="G$1" pin="A"/>
<wire x1="88.9" y1="-68.58" x2="88.9" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="Q106" gate="Q" pin="C"/>
<pinref part="R123" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-55.88" x2="88.9" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R122" gate="G$1" pin="1"/>
<pinref part="Q106" gate="Q" pin="B"/>
<wire x1="81.28" y1="-50.8" x2="83.82" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<pinref part="U103" gate="A" pin="PB0"/>
<wire x1="170.18" y1="48.26" x2="167.64" y2="48.26" width="0.1524" layer="91"/>
<label x="167.64" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C106" gate="G$1" pin="1"/>
<pinref part="U103" gate="A" pin="AREF"/>
<wire x1="154.94" y1="53.34" x2="170.18" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALARM1" class="0">
<segment>
<pinref part="U103" gate="A" pin="PD3"/>
<wire x1="205.74" y1="33.02" x2="210.82" y2="33.02" width="0.1524" layer="91"/>
<label x="210.82" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ALARM2" class="0">
<segment>
<pinref part="U103" gate="A" pin="PD4"/>
<wire x1="205.74" y1="30.48" x2="210.82" y2="30.48" width="0.1524" layer="91"/>
<label x="210.82" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DOOR1-OPEN" class="0">
<segment>
<pinref part="U103" gate="A" pin="PE0"/>
<wire x1="205.74" y1="17.78" x2="208.28" y2="17.78" width="0.1524" layer="91"/>
<label x="208.28" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="READER-POWER" class="0">
<segment>
<pinref part="U103" gate="A" pin="PE1"/>
<wire x1="205.74" y1="15.24" x2="208.28" y2="15.24" width="0.1524" layer="91"/>
<label x="208.28" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RS485-RXE" class="0">
<segment>
<wire x1="0" y1="60.96" x2="-10.16" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U101" gate="A" pin="~RE"/>
<wire x1="-10.16" y1="60.96" x2="-22.86" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="48.26" x2="-10.16" y2="60.96" width="0.1524" layer="91"/>
<junction x="-10.16" y="60.96"/>
<label x="-22.86" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PC5"/>
<wire x1="205.74" y1="48.26" x2="210.82" y2="48.26" width="0.1524" layer="91"/>
<label x="210.82" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RS485-TXE" class="0">
<segment>
<pinref part="U101" gate="A" pin="DE"/>
<wire x1="0" y1="58.42" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="58.42" x2="-22.86" y2="58.42" width="0.1524" layer="91"/>
<junction x="-15.24" y="58.42"/>
<label x="-22.86" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PD2"/>
<wire x1="205.74" y1="35.56" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<label x="210.82" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="15.24" y1="-60.96" x2="15.24" y2="-63.5" width="0.1524" layer="91"/>
<label x="15.24" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="READER-RS485-TXE" class="0">
<segment>
<pinref part="U102" gate="A" pin="DE"/>
<wire x1="-20.32" y1="5.08" x2="-15.24" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-5.08" x2="-15.24" y2="5.08" width="0.1524" layer="91"/>
<junction x="-15.24" y="5.08"/>
<label x="-20.32" y="5.08" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U103" gate="A" pin="PB5"/>
<wire x1="170.18" y1="35.56" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<label x="167.64" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R122" gate="G$1" pin="2"/>
<wire x1="81.28" y1="-60.96" x2="81.28" y2="-63.5" width="0.1524" layer="91"/>
<label x="81.28" y="-63.5" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="254" y="71.12" size="1.778" layer="97" rot="R90">Power input from bus connector</text>
</plain>
<instances>
<instance part="SV201" gate="1" x="213.36" y="106.68"/>
<instance part="GND215" gate="1" x="231.14" y="68.58"/>
<instance part="GND214" gate="1" x="223.52" y="99.06"/>
<instance part="SUP-3V3205" gate="G$1" x="223.52" y="116.84" rot="MR0"/>
<instance part="FRAME201" gate="G$1" x="-7.62" y="-50.8"/>
<instance part="U201" gate="G$1" x="25.4" y="109.22"/>
<instance part="GND204" gate="1" x="25.4" y="88.9"/>
<instance part="C201" gate="POS" x="12.7" y="101.6" rot="R90"/>
<instance part="SUP-24V201" gate="G$1" x="7.62" y="114.3"/>
<instance part="K201" gate="1" x="22.86" y="5.08"/>
<instance part="K201" gate="U" x="43.18" y="7.62"/>
<instance part="Q201" gate="Q" x="20.32" y="-12.7"/>
<instance part="GND203" gate="1" x="22.86" y="-22.86"/>
<instance part="R201" gate="G$1" x="2.54" y="-30.48" rot="R90"/>
<instance part="D201" gate="G$1" x="2.54" y="-17.78" rot="R270"/>
<instance part="GND201" gate="1" x="2.54" y="-40.64"/>
<instance part="J201" gate="J$1" x="177.8" y="111.76" rot="R180"/>
<instance part="SUP-24V202" gate="G$1" x="22.86" y="20.32"/>
<instance part="R202" gate="G$1" x="10.16" y="-12.7" rot="R180"/>
<instance part="K202" gate="1" x="104.14" y="5.08"/>
<instance part="K202" gate="U" x="137.16" y="2.54" rot="MR90"/>
<instance part="Q203" gate="Q" x="101.6" y="-12.7"/>
<instance part="GND207" gate="1" x="104.14" y="-22.86"/>
<instance part="R205" gate="G$1" x="83.82" y="-30.48" rot="R90"/>
<instance part="D204" gate="G$1" x="83.82" y="-17.78" rot="R270"/>
<instance part="GND206" gate="1" x="83.82" y="-40.64"/>
<instance part="SUP-24V203" gate="G$1" x="104.14" y="20.32"/>
<instance part="R208" gate="G$1" x="91.44" y="-12.7" rot="R180"/>
<instance part="SUP-24V204" gate="G$1" x="124.46" y="25.4" rot="MR0"/>
<instance part="SUP-3V3202" gate="G$1" x="38.1" y="116.84"/>
<instance part="S201" gate="G$1" x="22.86" y="50.8" smashed="yes" rot="MR270">
<attribute name="NAME" x="16.51" y="53.34" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="22.86" y="43.18" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND205" gate="1" x="25.4" y="35.56" rot="MR0"/>
<instance part="Q202" gate="Q" x="27.94" y="68.58" rot="MR0"/>
<instance part="R203" gate="G$1" x="38.1" y="68.58" rot="R180"/>
<instance part="SUP-3V3201" gate="G$1" x="25.4" y="81.28"/>
<instance part="D203" gate="G$1" x="33.02" y="48.26" rot="R270"/>
<instance part="C202" gate="POS" x="15.24" y="68.58" rot="R90"/>
<instance part="GND202" gate="1" x="15.24" y="60.96"/>
<instance part="R204" gate="G$1" x="43.18" y="48.26" rot="R270"/>
<instance part="OK202" gate="G$1" x="106.68" y="68.58"/>
<instance part="GND209" gate="1" x="116.84" y="60.96"/>
<instance part="OK201" gate="G$1" x="106.68" y="96.52"/>
<instance part="R206" gate="G$1" x="88.9" y="99.06"/>
<instance part="SUP-3V3203" gate="G$1" x="96.52" y="114.3"/>
<instance part="R209" gate="G$1" x="111.76" y="109.22"/>
<instance part="GND208" gate="1" x="116.84" y="88.9"/>
<instance part="R207" gate="G$1" x="88.9" y="71.12"/>
<instance part="D206" gate="G$1" x="101.6" y="109.22"/>
<instance part="SUP-3V3204" gate="G$1" x="96.52" y="86.36"/>
<instance part="R210" gate="G$1" x="111.76" y="81.28"/>
<instance part="D207" gate="G$1" x="101.6" y="81.28"/>
<instance part="R211" gate="G$1" x="129.54" y="7.62" rot="MR270"/>
<instance part="X203" gate="-1" x="236.22" y="40.64" rot="R180"/>
<instance part="X203" gate="-2" x="236.22" y="45.72" rot="R180"/>
<instance part="X203" gate="-3" x="236.22" y="50.8" rot="R180"/>
<instance part="X203" gate="-4" x="236.22" y="55.88" rot="R180"/>
<instance part="X202" gate="-1" x="236.22" y="73.66" rot="R180"/>
<instance part="X202" gate="-2" x="236.22" y="78.74" rot="R180"/>
<instance part="X202" gate="-3" x="236.22" y="83.82" rot="R180"/>
<instance part="X202" gate="-4" x="236.22" y="88.9" rot="R180"/>
<instance part="D205" gate="G$1" x="93.98" y="5.08" rot="R270"/>
<instance part="D202" gate="G$1" x="12.7" y="5.08" rot="R270"/>
<instance part="GND216" gate="1" x="231.14" y="35.56"/>
<instance part="SUP-24V205" gate="G$1" x="231.14" y="93.98"/>
<instance part="X201" gate="-1" x="198.12" y="40.64" rot="R180"/>
<instance part="X201" gate="-2" x="198.12" y="45.72" rot="R180"/>
<instance part="X201" gate="-3" x="198.12" y="50.8" rot="R180"/>
<instance part="X201" gate="-4" x="198.12" y="55.88" rot="R180"/>
<instance part="LOGO201" gate="G$1" x="254" y="-10.16"/>
<instance part="F201" gate="G$1" x="129.54" y="-7.62" rot="MR270"/>
<instance part="H201" gate="G$1" x="198.12" y="10.16"/>
<instance part="H202" gate="G$1" x="205.74" y="10.16"/>
<instance part="H203" gate="G$1" x="213.36" y="10.16"/>
<instance part="H204" gate="G$1" x="220.98" y="10.16"/>
<instance part="GND210" gate="1" x="198.12" y="0"/>
<instance part="GND211" gate="1" x="205.74" y="0"/>
<instance part="GND212" gate="1" x="213.36" y="0"/>
<instance part="GND213" gate="1" x="220.98" y="0"/>
<instance part="FID201" gate="G$1" x="193.04" y="-12.7"/>
<instance part="FID202" gate="G$1" x="203.2" y="-12.7"/>
<instance part="FID203" gate="G$1" x="213.36" y="-12.7"/>
<instance part="FID204" gate="G$1" x="223.52" y="-12.7"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SV201" gate="1" pin="1"/>
<wire x1="220.98" y1="104.14" x2="223.52" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND214" gate="1" pin="GND"/>
<wire x1="223.52" y1="101.6" x2="223.52" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND204" gate="1" pin="GND"/>
<pinref part="U201" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="91.44" x2="25.4" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C201" gate="POS" pin="-"/>
<wire x1="25.4" y1="93.98" x2="25.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="25.4" y1="93.98" x2="12.7" y2="93.98" width="0.1524" layer="91"/>
<wire x1="12.7" y1="93.98" x2="12.7" y2="99.06" width="0.1524" layer="91"/>
<junction x="25.4" y="93.98"/>
</segment>
<segment>
<pinref part="Q201" gate="Q" pin="E"/>
<wire x1="22.86" y1="-17.78" x2="22.86" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="GND203" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R201" gate="G$1" pin="1"/>
<pinref part="GND201" gate="1" pin="GND"/>
<wire x1="2.54" y1="-35.56" x2="2.54" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q203" gate="Q" pin="E"/>
<wire x1="104.14" y1="-17.78" x2="104.14" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="GND207" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R205" gate="G$1" pin="1"/>
<pinref part="GND206" gate="1" pin="GND"/>
<wire x1="83.82" y1="-35.56" x2="83.82" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S201" gate="G$1" pin="-"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND205" gate="1" pin="GND"/>
<pinref part="D203" gate="G$1" pin="AA"/>
<wire x1="25.4" y1="40.64" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<wire x1="33.02" y1="40.64" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<junction x="25.4" y="40.64"/>
<pinref part="R204" gate="G$1" pin="2"/>
<wire x1="43.18" y1="43.18" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<wire x1="43.18" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<junction x="33.02" y="40.64"/>
</segment>
<segment>
<pinref part="C202" gate="POS" pin="-"/>
<pinref part="GND202" gate="1" pin="GND"/>
<wire x1="15.24" y1="63.5" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OK202" gate="G$1" pin="EMIT"/>
<wire x1="114.3" y1="66.04" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
<wire x1="116.84" y1="66.04" x2="116.84" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND209" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OK201" gate="G$1" pin="EMIT"/>
<wire x1="114.3" y1="93.98" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
<wire x1="116.84" y1="93.98" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND208" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X203" gate="-3" pin="1"/>
<wire x1="233.68" y1="50.8" x2="231.14" y2="50.8" width="0.1524" layer="91"/>
<wire x1="231.14" y1="50.8" x2="231.14" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GND216" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND215" gate="1" pin="GND"/>
<wire x1="231.14" y1="71.12" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<pinref part="X202" gate="-3" pin="1"/>
<wire x1="231.14" y1="83.82" x2="233.68" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H204" gate="G$1" pin="1"/>
<pinref part="GND213" gate="1" pin="GND"/>
<wire x1="220.98" y1="5.08" x2="220.98" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H203" gate="G$1" pin="1"/>
<pinref part="GND212" gate="1" pin="GND"/>
<wire x1="213.36" y1="5.08" x2="213.36" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H202" gate="G$1" pin="1"/>
<pinref part="GND211" gate="1" pin="GND"/>
<wire x1="205.74" y1="5.08" x2="205.74" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H201" gate="G$1" pin="1"/>
<pinref part="GND210" gate="1" pin="GND"/>
<wire x1="198.12" y1="5.08" x2="198.12" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<pinref part="SV201" gate="1" pin="2"/>
<wire x1="205.74" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<label x="203.2" y="104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="SV201" gate="1" pin="5"/>
<wire x1="220.98" y1="109.22" x2="223.52" y2="109.22" width="0.1524" layer="91"/>
<pinref part="SUP-3V3205" gate="G$1" pin="+3V3"/>
<wire x1="223.52" y1="109.22" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U201" gate="G$1" pin="OUT"/>
<wire x1="33.02" y1="109.22" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
<wire x1="38.1" y1="109.22" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUP-3V3202" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="SUP-3V3201" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="78.74" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C202" gate="POS" pin="+"/>
<wire x1="15.24" y1="71.12" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="15.24" y1="76.2" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Q202" gate="Q" pin="E"/>
<wire x1="25.4" y1="73.66" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<junction x="25.4" y="76.2"/>
</segment>
<segment>
<pinref part="SUP-3V3203" gate="G$1" pin="+3V3"/>
<pinref part="D206" gate="G$1" pin="A"/>
<wire x1="96.52" y1="111.76" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
<wire x1="96.52" y1="109.22" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP-3V3204" gate="G$1" pin="+3V3"/>
<pinref part="D207" gate="G$1" pin="A"/>
<wire x1="96.52" y1="83.82" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="U201" gate="G$1" pin="IN"/>
<wire x1="7.62" y1="111.76" x2="7.62" y2="109.22" width="0.1524" layer="91"/>
<wire x1="7.62" y1="109.22" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C201" gate="POS" pin="+"/>
<wire x1="12.7" y1="109.22" x2="17.78" y2="109.22" width="0.1524" layer="91"/>
<wire x1="12.7" y1="104.14" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<junction x="12.7" y="109.22"/>
<pinref part="SUP-24V201" gate="G$1" pin="+24V"/>
</segment>
<segment>
<pinref part="K201" gate="1" pin="1"/>
<wire x1="22.86" y1="10.16" x2="22.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="15.24" y1="15.24" x2="22.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="22.86" y1="15.24" x2="22.86" y2="17.78" width="0.1524" layer="91"/>
<junction x="22.86" y="15.24"/>
<pinref part="SUP-24V202" gate="G$1" pin="+24V"/>
<pinref part="D202" gate="G$1" pin="C1"/>
<pinref part="D202" gate="G$1" pin="C2"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<wire x1="15.24" y1="15.24" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="12.7" width="0.1524" layer="91"/>
<junction x="15.24" y="15.24"/>
</segment>
<segment>
<pinref part="K202" gate="1" pin="1"/>
<wire x1="104.14" y1="10.16" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="12.7" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<junction x="104.14" y="12.7"/>
<pinref part="SUP-24V203" gate="G$1" pin="+24V"/>
<pinref part="D205" gate="G$1" pin="C1"/>
<junction x="96.52" y="12.7"/>
<pinref part="D205" gate="G$1" pin="C2"/>
<wire x1="96.52" y1="12.7" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP-24V204" gate="G$1" pin="+24V"/>
<pinref part="R211" gate="G$1" pin="1"/>
<wire x1="124.46" y1="17.78" x2="124.46" y2="22.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="12.7" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="17.78" x2="124.46" y2="17.78" width="0.1524" layer="91"/>
<pinref part="K202" gate="U" pin="O"/>
<wire x1="142.24" y1="7.62" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<wire x1="142.24" y1="17.78" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<junction x="129.54" y="17.78"/>
</segment>
<segment>
<pinref part="X202" gate="-4" pin="1"/>
<wire x1="233.68" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="231.14" y1="88.9" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<pinref part="SUP-24V205" gate="G$1" pin="+24V"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="K201" gate="1" pin="2"/>
<wire x1="22.86" y1="0" x2="22.86" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-2.54" x2="22.86" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="Q201" gate="Q" pin="C"/>
<wire x1="22.86" y1="-2.54" x2="22.86" y2="-7.62" width="0.1524" layer="91"/>
<junction x="22.86" y="-2.54"/>
<pinref part="D202" gate="G$1" pin="AA"/>
</segment>
</net>
<net name="DOOR1-OPEN" class="0">
<segment>
<pinref part="R202" gate="G$1" pin="2"/>
<wire x1="5.08" y1="-12.7" x2="2.54" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="D201" gate="G$1" pin="A"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-12.7" x2="0" y2="-12.7" width="0.1524" layer="91"/>
<junction x="2.54" y="-12.7"/>
<label x="0" y="-12.7" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D201" gate="G$1" pin="C"/>
<pinref part="R201" gate="G$1" pin="2"/>
<wire x1="2.54" y1="-20.32" x2="2.54" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DOOR1-NO" class="0">
<segment>
<pinref part="K201" gate="U" pin="S"/>
<wire x1="38.1" y1="12.7" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<label x="38.1" y="15.24" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="167.64" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<label x="167.64" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J201" gate="J$1" pin="1"/>
</segment>
</net>
<net name="DOOR1-NC" class="0">
<segment>
<pinref part="K201" gate="U" pin="O"/>
<wire x1="48.26" y1="12.7" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<label x="48.26" y="15.24" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J201" gate="J$1" pin="3"/>
<wire x1="170.18" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<label x="167.64" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DOOR1-SUPPLY" class="0">
<segment>
<pinref part="K201" gate="U" pin="P"/>
<wire x1="43.18" y1="5.08" x2="43.18" y2="2.54" width="0.1524" layer="91"/>
<label x="43.18" y="2.54" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="170.18" y1="111.76" x2="167.64" y2="111.76" width="0.1524" layer="91"/>
<label x="167.64" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J201" gate="J$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R202" gate="G$1" pin="1"/>
<pinref part="Q201" gate="Q" pin="B"/>
<wire x1="15.24" y1="-12.7" x2="17.78" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="K202" gate="1" pin="2"/>
<wire x1="104.14" y1="0" x2="104.14" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="0" x2="93.98" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-2.54" x2="104.14" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="Q203" gate="Q" pin="C"/>
<wire x1="104.14" y1="-2.54" x2="104.14" y2="-7.62" width="0.1524" layer="91"/>
<junction x="104.14" y="-2.54"/>
<pinref part="D205" gate="G$1" pin="AA"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D204" gate="G$1" pin="C"/>
<pinref part="R205" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-20.32" x2="83.82" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R208" gate="G$1" pin="1"/>
<pinref part="Q203" gate="Q" pin="B"/>
<wire x1="96.52" y1="-12.7" x2="99.06" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="READER-POWER" class="0">
<segment>
<pinref part="R208" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-12.7" x2="83.82" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="D204" gate="G$1" pin="A"/>
<wire x1="83.82" y1="-15.24" x2="83.82" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-12.7" x2="81.28" y2="-12.7" width="0.1524" layer="91"/>
<junction x="83.82" y="-12.7"/>
<label x="81.28" y="-12.7" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="READER+24V" class="0">
<segment>
<wire x1="129.54" y1="-12.7" x2="129.54" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-15.24" x2="134.62" y2="-15.24" width="0.1524" layer="91"/>
<label x="134.62" y="-15.24" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="F201" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="X203" gate="-4" pin="1"/>
<wire x1="233.68" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<label x="231.14" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="READER-A+" class="0">
<segment>
<pinref part="X203" gate="-1" pin="1"/>
<wire x1="233.68" y1="40.64" x2="228.6" y2="40.64" width="0.1524" layer="91"/>
<label x="228.6" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<label x="45.72" y="68.58" size="1.27" layer="95" rot="MR180" xref="yes"/>
<wire x1="43.18" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R203" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="Q202" gate="Q" pin="B"/>
<pinref part="R203" gate="G$1" pin="2"/>
<wire x1="30.48" y1="68.58" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="S201" gate="G$1" pin="+"/>
<wire x1="25.4" y1="53.34" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<pinref part="Q202" gate="Q" pin="C"/>
<wire x1="25.4" y1="58.42" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="25.4" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<junction x="25.4" y="58.42"/>
<pinref part="D203" gate="G$1" pin="C2"/>
<wire x1="30.48" y1="58.42" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<wire x1="30.48" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<junction x="30.48" y="58.42"/>
<pinref part="D203" gate="G$1" pin="C1"/>
<wire x1="35.56" y1="58.42" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<wire x1="35.56" y1="58.42" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
<junction x="35.56" y="58.42"/>
<pinref part="R204" gate="G$1" pin="1"/>
<wire x1="43.18" y1="58.42" x2="43.18" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="OK202" gate="G$1" pin="A"/>
<wire x1="96.52" y1="71.12" x2="93.98" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R207" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ALARM1-A" class="0">
<segment>
<wire x1="83.82" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="81.28" y1="71.12" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<label x="81.28" y="73.66" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R207" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="X201" gate="-1" pin="1"/>
<wire x1="193.04" y1="40.64" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<label x="193.04" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ALARM1-C" class="0">
<segment>
<pinref part="OK202" gate="G$1" pin="C"/>
<wire x1="96.52" y1="66.04" x2="93.98" y2="66.04" width="0.1524" layer="91"/>
<label x="93.98" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X201" gate="-2" pin="1"/>
<wire x1="195.58" y1="45.72" x2="193.04" y2="45.72" width="0.1524" layer="91"/>
<label x="193.04" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ALARM1" class="0">
<segment>
<pinref part="OK202" gate="G$1" pin="COL"/>
<wire x1="114.3" y1="71.12" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="71.12" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="116.84" y1="76.2" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<label x="121.92" y="76.2" size="1.27" layer="95" xref="yes"/>
<wire x1="116.84" y1="81.28" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R210" gate="G$1" pin="2"/>
<junction x="116.84" y="76.2"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="OK201" gate="G$1" pin="A"/>
<pinref part="R206" gate="G$1" pin="2"/>
<wire x1="96.52" y1="99.06" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALARM2" class="0">
<segment>
<pinref part="OK201" gate="G$1" pin="COL"/>
<wire x1="114.3" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<wire x1="116.84" y1="99.06" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R209" gate="G$1" pin="2"/>
<label x="121.92" y="101.6" size="1.27" layer="95" xref="yes"/>
<wire x1="116.84" y1="101.6" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
<wire x1="121.92" y1="101.6" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
<junction x="116.84" y="101.6"/>
</segment>
</net>
<net name="ALARM2-A" class="0">
<segment>
<pinref part="R206" gate="G$1" pin="1"/>
<wire x1="83.82" y1="99.06" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="81.28" y1="99.06" x2="81.28" y2="101.6" width="0.1524" layer="91"/>
<label x="81.28" y="101.6" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X201" gate="-3" pin="1"/>
<wire x1="193.04" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<label x="193.04" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ALARM2-C" class="0">
<segment>
<pinref part="OK201" gate="G$1" pin="C"/>
<wire x1="96.52" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<label x="93.98" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X201" gate="-4" pin="1"/>
<wire x1="195.58" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<label x="193.04" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="D206" gate="G$1" pin="C"/>
<pinref part="R209" gate="G$1" pin="1"/>
<wire x1="104.14" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="D207" gate="G$1" pin="C"/>
<pinref part="R210" gate="G$1" pin="1"/>
<wire x1="104.14" y1="81.28" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="READER-B-" class="0">
<segment>
<pinref part="X203" gate="-2" pin="1"/>
<wire x1="233.68" y1="45.72" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
<label x="228.6" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A+" class="0">
<segment>
<pinref part="X202" gate="-1" pin="1"/>
<wire x1="233.68" y1="73.66" x2="228.6" y2="73.66" width="0.1524" layer="91"/>
<label x="228.6" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B-" class="0">
<segment>
<pinref part="X202" gate="-2" pin="1"/>
<wire x1="228.6" y1="78.74" x2="233.68" y2="78.74" width="0.1524" layer="91"/>
<label x="228.6" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="F201" gate="G$1" pin="1"/>
<wire x1="129.54" y1="-2.54" x2="129.54" y2="2.54" width="0.1524" layer="91"/>
<pinref part="K202" gate="U" pin="P"/>
<wire x1="129.54" y1="2.54" x2="134.62" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R211" gate="G$1" pin="2"/>
<junction x="129.54" y="2.54"/>
</segment>
</net>
<net name="READER-RS485-TXE" class="0">
<segment>
<pinref part="SV201" gate="1" pin="4"/>
<wire x1="205.74" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<label x="203.2" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="READER-RS485-RX" class="0">
<segment>
<pinref part="SV201" gate="1" pin="6"/>
<wire x1="205.74" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<label x="203.2" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="READER-RS485-TX" class="0">
<segment>
<pinref part="SV201" gate="1" pin="3"/>
<wire x1="220.98" y1="106.68" x2="223.52" y2="106.68" width="0.1524" layer="91"/>
<label x="223.52" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,0,66.04,U101,VCC,+3V3,,,"/>
<approved hash="104,1,0,12.7,U102,VCC,+3V3,,,"/>
<approved hash="104,1,170.18,58.42,U103,AVCC,+3V3,,,"/>
<approved hash="104,1,170.18,60.96,U103,VCC,+3V3,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
